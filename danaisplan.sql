-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 10, 2016 at 09:39 AM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `danaisplan`
--

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id` int(11) NOT NULL,
  `id_program` int(11) NOT NULL,
  `code_kegiatan` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`id`, `id_program`, `code_kegiatan`, `name`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, '001.001.003.004', 'Pengembangan museum afandi', 'xx', NULL, '2015-10-09 19:54:05', '2015-10-14 14:26:03'),
(2, 2, '001332', 'pembangunan fisik sekolah', 'll', NULL, '2015-10-14 14:13:05', '2015-10-14 14:13:05'),
(3, 3, '046', 'Pendampingan Masyarakat Pecinta Seni dan Budaya', 'Pagu tetap', NULL, '2015-10-23 04:54:08', '2015-10-23 06:07:48'),
(4, 3, '047', 'Pemberian Penghargaan Bagi Pelestari dan Penggiat Budaya', 'Pagu tetap', NULL, '2015-10-23 06:07:15', '2015-10-23 06:07:15'),
(5, 3, '049', 'Pengembangan Bahasa dan Sastra', 'Pagu tidak berubah', NULL, '2015-10-23 06:08:55', '2015-10-23 06:09:07'),
(6, 3, '050', 'Pelestarian, Pengembangan dan Aplikasi Nilai-nilai Budaya Luhur di Masyarakat', 'pagu tidak ada perubahan', NULL, '2015-10-23 06:10:10', '2015-10-23 06:11:07'),
(7, 3, '054', 'Pelestarian Kepercayaan dan Tradisi', 'pagu tidak ada perubahan', NULL, '2015-10-23 06:10:57', '2015-10-23 06:10:57'),
(8, 3, '056', 'Gelar Warisan Budaya Tak benda', 'pagu perubahan', NULL, '2015-10-23 06:12:32', '2015-10-23 06:21:50'),
(9, 4, '109', 'Pelestarian Warisan Budaya dan Cagar Budaya', 'Pagu awal', NULL, '2015-10-23 06:15:25', '2015-10-23 06:15:25'),
(10, 4, '112', 'Pembinaan dan Pengembangan Kesejarahan', 'Pagu awal', NULL, '2015-10-23 06:16:02', '2015-10-23 06:16:02'),
(11, 4, '113', 'Penguatan Lembaga Pengelola dan Pelestari Warisan Budaya', 'pagu awal', NULL, '2015-10-23 06:16:34', '2015-10-23 06:16:34'),
(12, 4, '117', 'Pembinaan dan Pengembangan Museum', 'pagu awal', NULL, '2015-10-23 06:17:04', '2015-10-23 06:17:04'),
(13, 4, '125', 'Warisan Budaya Nasional yang Dinominasikan', 'pagu awal', NULL, '2015-10-23 14:45:32', '2015-10-23 14:45:32'),
(14, 5, '0046', 'Pembinaan dan Pengembangan Seni Budaya Daerah', 'Pagu Awal', NULL, '2015-10-23 14:51:38', '2015-10-23 14:51:38'),
(15, 5, '0054', 'Aktualisasi Kesenian Tradisional dan Budaya Kontemporer', 'pagu awal', NULL, '2015-10-23 14:53:06', '2015-10-23 14:53:06'),
(16, 5, '055', 'Promosi dan Publikasi Seni Budaya', 'pagu awal', NULL, '2015-10-23 14:53:27', '2015-10-23 14:53:56'),
(17, 5, '064', 'Penyelenggaraan Event Lembaga Penggiat Seni dan Budaya', 'pagu awal', NULL, '2015-10-23 14:54:50', '2015-10-23 14:54:50'),
(18, 5, '065', 'Gelar Budaya Jogja', 'pagu awal', NULL, '2015-10-23 14:55:20', '2015-10-23 14:55:20'),
(19, 5, '066', 'Pengembangan Taman Budaya', 'pagu perubahan tahap 1', NULL, '2015-10-23 14:55:55', '2015-10-23 19:18:13'),
(20, 6, '011', 'Misi Kebudayaan dalam dan Luar Negeri dalam rangka diplomasi Budaya', 'Pagu awal', NULL, '2015-10-23 14:56:59', '2015-10-23 14:56:59'),
(21, 6, '013', 'Gelar Pelangi Budaya', 'pagu awal', NULL, '2015-10-23 14:57:36', '2015-10-23 14:57:36'),
(22, 6, '015', 'Membangun Kemitraan dengan Instansi', 'pagu awal', NULL, '2015-10-23 14:58:07', '2015-10-23 14:58:07'),
(23, 6, '016', 'Membangun Kemitraan Bilateral', 'pagu awal', NULL, '2015-10-23 14:58:43', '2015-10-23 14:59:06'),
(24, 6, '017', 'Membangun Kemitraan dengan Lembaga Pelestari Budaya', 'pagu awal', NULL, '2015-10-23 14:59:51', '2015-10-23 14:59:51'),
(25, 7, '006', 'Pengembangan kompleks Taman Budaya Yogyakarta', 'pagu awal', NULL, '2015-10-23 15:01:55', '2015-10-23 15:03:03'),
(26, 7, '012', 'Pengembangan Pusat Konservasi dan Pengembangan Budaya', 'Pagu perubahan ', NULL, '2015-10-23 15:02:48', '2015-10-23 19:17:20'),
(27, 7, '0013', 'Pengadaan Sarana Kesenian ke Sekolah', 'pagu awal', NULL, '2015-10-25 17:12:13', '2015-10-25 17:12:13'),
(28, 7, '014', 'Pengadaan Sarana Kesenian ke Masyarakat', 'Pagu awal', NULL, '2015-10-25 17:13:17', '2015-10-25 17:13:17'),
(29, 7, '0017', 'Pengembangan Rumah Budaya', 'pagu awal', NULL, '2015-10-25 17:14:22', '2015-10-25 17:14:22'),
(30, 7, '018', 'Pembangunan Laboratorium Budaya di Satuan Pendidikan', 'pagu awal', NULL, '2015-10-25 17:14:49', '2015-10-25 17:14:49'),
(31, 21, 'xxx', 'Penguatan Sarana dan Prasarana Lembaga Pelaksana Urusan Keistimewaan', 'pagu perubahan', NULL, '2015-10-25 17:17:24', '2015-10-25 17:17:24'),
(32, 4, '116', 'Pengelolaan dan Pengembangan Museum Sonobudoyo', 'pagu awal', NULL, '2015-10-25 17:20:29', '2015-10-25 17:20:29'),
(33, 3, '052', 'Pengembangan Desa dan Kantong Budaya', 'pagu awal', NULL, '2015-10-25 17:38:44', '2015-10-25 17:38:44'),
(34, 3, '053', 'Pengelolaan dan Penguatan Desa Budaya', 'pagu awal', NULL, '2015-10-25 17:39:37', '2015-10-25 17:39:37'),
(35, 5, '0053', 'Peningkatan dan Pengembangan Kapasitas SDM di Bidang Kebudayaan', 'pagu awal', NULL, '2015-10-25 17:51:30', '2015-10-25 17:51:30'),
(36, 8, '001', 'Peningkatan Kapasitas Pendidikan Dasar dalam Pengembangan dan Pelestarian Budaya Daerah', 'pagu awal', NULL, '2015-10-25 18:02:30', '2015-10-25 18:02:55'),
(37, 9, '0001', 'Peningkatan Kapasitas Pendidikan Menengah dalam Pengembangan dan Pelestarian Budaya Daerah', 'pagu awal', NULL, '2015-10-25 18:03:52', '2015-10-25 18:03:52'),
(38, 10, '003', 'Pengembangan Sekolah Model Pendidikan Berbasis Budaya', 'pagu awal', NULL, '2015-10-25 18:07:05', '2015-10-25 18:07:57'),
(39, 10, '004', 'Pengembangan Pedoman, Bahan Ajar  dan Media Pembelajaran Pendidikan Karakter', 'pagu awal', NULL, '2015-10-25 18:08:23', '2015-10-25 18:08:23'),
(40, 11, '00001', 'Peningkatan Kapasitas Pendidikan PAUD dalam Pengembangan dan Pelestarian Budaya Daerah', 'pagu awal', NULL, '2015-10-25 18:09:30', '2015-10-25 18:09:30'),
(41, 12, '0000001', 'Peningkatan Kapasitas Pendidikan Keaksaraan dan Kesetaraan dalam Pengembangan dan Pelestarian  Budaya Daerah', 'pagu awal', NULL, '2015-10-25 18:26:42', '2015-10-25 18:26:42'),
(42, 13, '00000001', 'Pengembangan dan Pemanfaatan Obat Tradisional dan Pelayanan Kesehatan Tradisional', 'pagu awal', NULL, '2015-10-25 18:27:42', '2015-10-25 18:27:42'),
(43, 14, '000000001', 'Pembangunan Ekosistem Kultural DIY Berbasis Digital', 'pagu awal', NULL, '2015-10-25 18:28:33', '2015-10-25 18:28:33'),
(44, 15, '0002', 'Pengembangan Kearifan Lokal Lingkungan Hidup dalam Mendukung Pembangunan Berkelanjutan', 'pagu awal', NULL, '2015-10-25 18:29:18', '2015-10-25 18:29:18'),
(45, 16, '000001', 'Revitalisasi Budaya Gotong-Royong', 'pagu awal', NULL, '2015-10-25 18:31:22', '2015-10-25 18:31:44'),
(46, 17, '00000000001', 'Pembinaan dan Pengembangan Produk Budaya Khas Yogyakarta', 'pagu awal', NULL, '2015-10-25 18:33:12', '2015-10-25 18:33:12'),
(47, 18, '00000000000001', 'Koordinasi, Monev Aksi Penanganan Perubahan Sosial dan Potensi Konflik', 'pagu awal', NULL, '2015-10-25 18:34:31', '2015-10-25 18:34:52'),
(48, 19, '00002', 'Legislasi Rancangan Peraturan Daerah Istimewaan', 'pagu awal', NULL, '2015-10-25 20:02:48', '2015-10-25 20:02:48'),
(49, 19, '0003', 'Penyebarluasan Produk Hukum Keistimewaan', 'pagu awal', NULL, '2015-10-25 20:03:24', '2015-10-25 20:03:24'),
(50, 20, '000002', 'Konservasi dan Pengembangan Kawasan Geoheritage', 'pagu awal', NULL, '2015-10-25 20:04:42', '2015-10-25 20:04:42'),
(51, 21, '0000000000001', 'Pemeliharaan Rutin/Berkala Perlengkapan Gedung Kantor', 'pagu awal', NULL, '2015-10-25 20:05:48', '2015-10-25 20:05:48');

-- --------------------------------------------------------

--
-- Table structure for table `perdais`
--

CREATE TABLE `perdais` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `perdais`
--

INSERT INTO `perdais` (`id`, `name`, `note`, `created_at`, `updated_at`) VALUES
(2, 'perdais', 'hello', '2016-06-06 20:45:12', '2016-06-06 20:45:12');

-- --------------------------------------------------------

--
-- Table structure for table `prioritas`
--

CREATE TABLE `prioritas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `prioritas`
--

INSERT INTO `prioritas` (`id`, `name`, `note`, `created_at`, `updated_at`) VALUES
(1, 'penting', 'penting sekali', '2016-05-19 07:33:25', '2016-05-19 07:33:25');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(11) NOT NULL,
  `code_program` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `years` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `code_program`, `name`, `years`, `deleted_at`, `created_at`, `updated_at`) VALUES
(3, '15', 'PROGRAM PENGEMBANGAN NILAI BUDAYA', '2015', NULL, '2015-10-23 04:54:08', '2015-10-23 04:54:08'),
(4, '16', 'PROGRAM PENGELOLAAN KEKAYAAN BUDAYA', '2015', NULL, '2015-10-23 05:46:54', '2015-10-23 05:46:54'),
(5, '17', 'PROGRAM PENGELOLAAN KERAGAMAN BUDAYA', '2015', NULL, '2015-10-23 05:49:48', '2015-10-23 05:49:48'),
(6, '18', 'PROGRAM PENGEMBANGAN KERJASAMA PENGELOLAAN KEKAYAAN BUDAYA', '2015', NULL, '2015-10-23 05:50:12', '2015-10-23 05:50:12'),
(7, '19', 'PROGRAM PENINGKATAN SARANA DAN PRASARANA KEBUDAYAAN', '2015', NULL, '2015-10-23 05:50:43', '2015-10-23 05:50:43'),
(8, '20', 'PROGRAM PENDIDIKAN DASAR', '2015', NULL, '2015-10-23 05:52:14', '2015-10-23 05:52:14'),
(9, '21', 'PROGRAM PENDIDIKAN MENENGAH', '2015', NULL, '2015-10-23 05:52:58', '2015-10-23 05:52:58'),
(10, '22', 'PROGRAM PENDIDIKAN KARAKTER BERBASIS BUDAYA', '2015', NULL, '2015-10-23 05:53:17', '2015-10-23 05:53:17'),
(11, '41', 'PROGRAM PENDIDIKAN ANAK USIA DINI', '2015', NULL, '2015-10-23 05:53:48', '2015-10-23 05:53:48'),
(12, '42', 'PROGRAM PENDIDIKAN NON FORMAL DAN INFORMAL', '2015', NULL, '2015-10-23 05:54:09', '2015-10-23 05:54:09'),
(13, '23', 'PROGRAM PROMOSI KESEHATAN DAN PEMBERDAYAAN MASYARAKAT', '2015', NULL, '2015-10-23 05:54:33', '2015-10-23 05:54:33'),
(14, '24', 'PROGRAM PENGEMBANGAN KOMUNIKASI, INFORMASI DAN MEDIA MASSA', '2015', NULL, '2015-10-23 05:54:55', '2015-10-23 05:54:55'),
(15, '25', 'PROGRAM PENGELOLAAN LINGKUNGAN BERBASIS BUDAYA', '2015', NULL, '2015-10-23 05:55:19', '2015-10-23 05:55:19'),
(16, '26', 'PROGRAM PENINGKATAN KEBERDAYAAN MASYARAKAT PERDESAAN', '2015', NULL, '2015-10-23 05:55:55', '2015-10-23 05:55:55'),
(17, '27', 'PROGRAM PENGEMBANGAN IKM', '2015', NULL, '2015-10-23 05:56:17', '2015-10-23 05:56:17'),
(18, '47', 'PROGRAM KEWASPADAAN DINI DAN PEMBINAAN MASYARAKAT', '2015', NULL, '2015-10-23 05:57:02', '2015-10-23 05:57:02'),
(19, '29', 'PROGRAM PENATAAN PERATURAN PERUNDANG-UNDANGAN', '2015', NULL, '2015-10-23 05:57:39', '2015-10-23 05:57:39'),
(20, '30', 'PROGRAM ANALISIS KEBIJAKAN PEMBANGUNAN', '2015', NULL, '2015-10-23 05:57:58', '2015-10-23 05:57:58'),
(21, '45', 'PROGRAM PENINGKATAN SARANA DAN PRASARANA APARATUR', '2015', NULL, '2015-10-23 05:58:21', '2015-10-23 05:58:21'),
(22, '46', 'PROGRAM KERJASAMA INFORMASI PENYELENGGARAAN KEISTIMEWAAN DIY', '2015', NULL, '2015-10-23 05:58:37', '2015-10-23 05:58:37'),
(23, '50', 'PROGRAM KERJASAMA INFORMASI DAN MASS MEDIA', '2015', NULL, '2015-10-23 05:58:55', '2015-10-23 05:58:55'),
(24, '32', 'PROGRAM PENINGKATAN PENDIDIKAN KEDINASAN', '2015', NULL, '2015-10-23 05:59:28', '2015-10-23 05:59:28'),
(25, '33', 'PROGRAM PENYELAMATAN DAN PELESTARIAN DOKUMAN/ARSIP DAERAH', '2015', NULL, '2015-10-23 05:59:46', '2015-10-23 05:59:46'),
(26, '44', 'PROGRAM PENINGKATAN KUALITAS PELAYANAN INFORMASI', '2015', NULL, '2015-10-23 06:00:04', '2015-10-23 06:00:04'),
(27, '36', 'PROGRAM PENGEMBANGAN KEMITRAAN', '2015', NULL, '2015-10-23 06:00:21', '2015-10-23 06:00:21'),
(28, '48', 'PROGRAM PERENCANAAN DAN PENGENDALIAN PELAKSANAAN KEGIATAN KEISTIMEWAAN', '2015', NULL, '2015-10-23 06:00:54', '2015-10-23 06:00:54'),
(29, '40', 'PROGRAM PENGEMBANGAN PERIKANAN TANGKAP', '2015', NULL, '2015-10-23 06:01:24', '2015-10-23 06:01:24');

-- --------------------------------------------------------

--
-- Table structure for table `skpd`
--

CREATE TABLE `skpd` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instansi_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email_staff` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personil_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `is_complete` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `activation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `skpd`
--

INSERT INTO `skpd` (`id`, `code`, `instansi_name`, `email`, `email_staff`, `username`, `phone`, `address`, `personil_name`, `password`, `is_active`, `is_complete`, `activation`, `avatar`, `deleted_at`, `created_at`, `updated_at`) VALUES
('1', '001', 'PT Global Network', 'giarsyani.nuli@gmail.com', '', 'giarsyani.nuli', '087787778777', 'jl jatijajar', 'Yono Murtopo', NULL, '0', '0', NULL, NULL, NULL, '2015-10-08 17:55:09', '2016-06-03 02:02:30'),
('10', '1.17.01.07.', 'DINAS PENDIDIKAN, PEMUDA DAN OLAH RAGA', 'juj@yahoo.com', '', 'juj', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:28:53', '2016-06-03 02:02:30'),
('11', '1.17.01.08.', 'DINAS KESEHATAN', 'usu@yahoo.com', '', 'usu', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:29:33', '2016-06-03 02:02:30'),
('12', '1.17.01.09.', 'DINAS PERHUBUNGAN, KOMUNIKASI DAN INFORMATIKA', 'yayaz@yahii.com', '', 'yayaz', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:30:51', '2016-06-03 02:02:30'),
('13', '1.17.01.10.', 'BADAN LINGKUNGAN HIDUP', 'jaja@yy.com', '', 'jaja', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:31:22', '2016-06-03 02:02:30'),
('14', '1.17.01.11.', 'BADAN PEMBERDAYAAN PEREMPUAN DAN MASYARAKAT', 'ajaj@yy.com', '', 'ajaj', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:32:23', '2016-06-03 02:02:30'),
('15', '1.17.01.12.', 'DINAS PERINDUSTRIAN, PERDAGANGAN, KOPERASI DAN U K M', 'ja@ya.com', '', 'ja', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:32:55', '2016-06-03 02:02:30'),
('16', '1.17.01.13.', 'KANTOR PERWAKILAN DAERAH', 'hah@yay.com', '', 'hah', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:33:26', '2016-06-03 02:02:30'),
('17', '1.17.01.14.', 'BADAN KESATUAN BANGSA DAN PERLINDUNGAN MASYARAKAT', 'hh@tt.com', '', 'hh', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:34:21', '2016-06-03 02:02:31'),
('18', '1.17.01.15.', 'BIRO HUKUM', 'aja@rr.com', '', 'aja', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:34:52', '2016-06-03 02:02:31'),
('19', '1.17.01.16.', 'BIRO ADMINISTRASI PEMBANGUNAN', 'ya@ya.com', '', 'ya', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:35:25', '2016-06-03 02:02:31'),
('1h4kFEkAQylzD8QKOn8Fx9rcz1YNYkrw', 'YRPr', 'PT Nuli', 'skpd@gmail.com', NULL, 'skpd', '123', 'Jl. Kaliurang KM 14', 'Nuli', NULL, '1', '2', NULL, NULL, NULL, '2016-06-03 00:48:11', '2016-06-07 20:02:56'),
('2', '005', 'Dispora', 'keren@mailinator.com', 'keren@mailinator.com', 'keren', '081915804771', 'Alamat palsu', 'Nuli', NULL, '0', '0', NULL, NULL, NULL, '2015-10-14 21:03:51', '2016-06-03 02:02:31'),
('20', '1.17.01.17.', 'BIRO UMUM, HUMAS DAN PROTOKOL', 'aja@ya.com', '', 'aja', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:36:03', '2016-06-03 02:02:31'),
('21', '1.17.01.18.', 'BADAN PENDIDIKAN DAN PELATIHAN', 'yay@tt.com', '', 'yay', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:36:39', '2016-06-03 02:02:31'),
('22', '1.17.01.19.', 'BADAN PERPUSTAKAAN DAN ARSIP DAERAH', 'gagga@gg.com', '', 'gagga', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:38:23', '2016-06-03 02:02:31'),
('23', '1.17.01.20.', 'DINAS PARIWISATA', 'kka@hh.com', '', 'kka', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:38:58', '2016-06-03 02:02:31'),
('24', '1.17.01.21.', 'BADAN PERENCANAAN PEMBANGUNAN DAERAH', 'tt@yy.com', '', 'tt', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:39:24', '2016-06-03 02:02:31'),
('25', '1.17.01.23.', 'DINAS KELAUTAN DAN PERIKANAN', 'ja@jj.com', '', 'ja', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:40:17', '2016-06-03 02:02:31'),
('26', '1.17.01.24.', 'BAGIAN TATA PEMERINTAHAN SETDA KOTA YOGYAKARTA', 'kk@yy.com', '', 'kk', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:40:48', '2016-06-03 02:02:31'),
('27', '1.17.01.25.', 'DINAS KIMPRASWIL KOTA YOGYAKARTA', 'yay@ww.com', '', 'yay', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:41:20', '2016-06-03 02:02:31'),
('28', '1.17.01.26.', 'DINAS BANGUNAN GEDUNG DAN ASET DAERAH KOTA YOGYAKARTA', 'oao@tt.com', '', 'oao', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:41:53', '2016-06-03 02:02:31'),
('29', '1.17.01.27.', 'DINAS PU KABUPATEN KULONPROGO', 'ka@wa.com', '', 'ka', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:42:22', '2016-06-03 02:02:31'),
('3', '1.17.01.00', 'DINAS KEBUDAYAAN', 'dinas@yahoo.co', '', 'dinas', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 11:55:37', '2016-06-03 02:02:31'),
('30', '1.17.01.28.', 'DINAS PEKERJAAN UMUM, PERUMAHAN DAN ENERGI SUMBER DAYA MINERAL', 'ppal@yahh.com', '', 'ppal', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:42:57', '2016-06-03 02:02:31'),
('31', '01.17.01.68', 'PT. Firman', 'firman@geekgarden.id', 'firman@geekgarden.id', 'firman', '087880388158', 'jalan kaliurang km.14', 'Firmansyah', NULL, '0', '0', NULL, NULL, NULL, '2015-11-03 05:45:20', '2016-06-03 02:02:31'),
('32', '1.1.1.1', 'Mas Nur', 'nrahmanto@yahoo.com', 'nrahmanto@yahoo.com', 'nrahmanto', '08137772772', 'Jalan kepatihan', 'Nur Rahmanto', NULL, '0', '0', NULL, NULL, NULL, '2015-11-03 06:08:40', '2016-06-03 02:02:31'),
('4', '1.17.01.01', 'MUSEUM NEGERI SONOBUDOYO', 'ssasalanao@yahoo.co', '', 'ssasalanao', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:24:15', '2016-06-03 02:02:31'),
('5', '1.17.01.02.', 'DINAS KEBUDAYAAN, PARIWISATA, PEMUDA DAN OLAH RAGA KABUPATEN KULONPROGO', 'ds@yahoo.com', '', 'ds', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:25:09', '2016-06-03 02:02:31'),
('6', '1.17.01.03.', 'DINAS KEBUDAYAAN DAN PARIWISATA KABUPATEN BANTUL', 'HH@YAHOO.COM', '', 'HH', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:26:11', '2016-06-03 02:02:32'),
('7', '1.17.01.04.', 'DINAS KEBUDAYAAN DAN KEPARIWISATAAN KABUPATEN GUNUNGKIDUL', 'ka@yahoo.com', '', 'ka', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:26:51', '2016-06-03 02:02:32'),
('8', '1.17.01.05.', 'DINAS KEBUDAYAAN DAN PARIWISATA KABUPATEN SLEMAN', 'kuu@yahoo.com', '', 'kuu', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:27:40', '2016-06-03 02:02:32'),
('9', '1.17.01.06.', 'DINAS PARIWISATA DAN KEBUDAYAAN KOTA YOGYAKARTA', 'lal@yahoo.com', '', 'lal', '', '', '', NULL, '0', '0', NULL, NULL, NULL, '2015-10-23 12:28:08', '2016-06-03 02:02:32');

-- --------------------------------------------------------

--
-- Table structure for table `wilayah_desa`
--

CREATE TABLE `wilayah_desa` (
  `id` varchar(10) NOT NULL,
  `kecamatan_id` varchar(7) DEFAULT NULL,
  `nama` varchar(40) DEFAULT NULL,
  `latitude` varchar(20) DEFAULT NULL,
  `longitude` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wilayah_desa`
--

INSERT INTO `wilayah_desa` (`id`, `kecamatan_id`, `nama`, `latitude`, `longitude`) VALUES
('3401010001', '3401010', 'Jangkaran', '-7.896026', '110.037155'),
('3401010002', '3401010', 'Sindutan', '-7.880042', '110.048083'),
('3401010003', '3401010', 'Palihan', '-7.892757', '110.054747'),
('3401010004', '3401010', 'Glagah', '-7.903997', '110.070056'),
('3401010005', '3401010', 'Kali Dengen', '-7.893955', '110.085350'),
('3401010006', '3401010', 'Plumbon', '-7.901041', '110.100729'),
('3401010007', '3401010', 'Kedundang', '-7.878827', '110.098566'),
('3401010008', '3401010', 'Demen', '-7.887662', '110.096003'),
('3401010009', '3401010', 'Kulur', '-7.873611', '110.101545'),
('3401010010', '3401010', 'Kaligintung', '-7.875418', '110.091122'),
('3401010011', '3401010', 'Temon Wetan', '-7.875488', '110.083008'),
('3401010012', '3401010', 'Temon Kulon', '-7.886478', '110.076233'),
('3401010013', '3401010', 'Kebonrejo', '-7.884341', '110.067299'),
('3401010014', '3401010', 'Janten', '-7.876748', '110.064322'),
('3401010015', '3401010', 'Karang Wuluh', '-7.874611', '110.055388'),
('3401020001', '3401020', 'Karang Wuni', '-7.917626', '110.098567'),
('3401020002', '3401020', 'Sogan', '-7.898818', '110.113457'),
('3401020003', '3401020', 'Kulwaru', '-7.902168', '110.123880'),
('3401020004', '3401020', 'Ngestiharjo', '-7.903087', '110.131325'),
('3401020005', '3401020', 'Triharjo', '-7.882151', '110.137281'),
('3401020006', '3401020', 'Bendungan', '-7.891875', '110.149194'),
('3401020007', '3401020', 'Giri Peni', '-7.875509', '110.167063'),
('3401020008', '3401020', 'Wates', '-7.870649', '110.161106'),
('3401030001', '3401030', 'Garongan', '-7.891031', '110.129386'),
('3401030002', '3401030', 'Pleret', '-7.820815', '110.393887'),
('3401030003', '3401030', 'Bugel', NULL, NULL),
('3401030004', '3401030', 'Kanoman', NULL, NULL),
('3401030005', '3401030', 'Depok', NULL, NULL),
('3401030006', '3401030', 'Bojong', NULL, NULL),
('3401030007', '3401030', 'Tayuban', NULL, NULL),
('3401030008', '3401030', 'Gotakan', NULL, NULL),
('3401030009', '3401030', 'Panjatan', NULL, NULL),
('3401030010', '3401030', 'Cerme', NULL, NULL),
('3401030011', '3401030', 'Krembangan', NULL, NULL),
('3401040001', '3401040', 'Karang Sewu', NULL, NULL),
('3401040002', '3401040', 'Banaran', NULL, NULL),
('3401040003', '3401040', 'Kranggan', NULL, NULL),
('3401040004', '3401040', 'Nomporejo', NULL, NULL),
('3401040005', '3401040', 'Brosot', NULL, NULL),
('3401040006', '3401040', 'Pandowan', NULL, NULL),
('3401040007', '3401040', 'Tirta Rahayu', NULL, NULL),
('3401050001', '3401050', 'Wahyuharjo', NULL, NULL),
('3401050002', '3401050', 'Bumirejo', NULL, NULL),
('3401050003', '3401050', 'Jatirejo', NULL, NULL),
('3401050004', '3401050', 'Sidorejo', NULL, NULL),
('3401050005', '3401050', 'Gulurejo', NULL, NULL),
('3401050006', '3401050', 'Ngentakrejo', NULL, NULL),
('3401060001', '3401060', 'Demangrejo', NULL, NULL),
('3401060002', '3401060', 'Srikayangan', NULL, NULL),
('3401060003', '3401060', 'Tuksono', NULL, NULL),
('3401060004', '3401060', 'Salamrejo', NULL, NULL),
('3401060005', '3401060', 'Sukoreno', NULL, NULL),
('3401060006', '3401060', 'Kaliagung', NULL, NULL),
('3401060007', '3401060', 'Sentolo', NULL, NULL),
('3401060008', '3401060', 'Banguncipto', NULL, NULL),
('3401070001', '3401070', 'Tawangsari', NULL, NULL),
('3401070002', '3401070', 'Karangsari', NULL, NULL),
('3401070003', '3401070', 'Kedungsari', NULL, NULL),
('3401070004', '3401070', 'Margosari', NULL, NULL),
('3401070005', '3401070', 'Pengasih', NULL, NULL),
('3401070006', '3401070', 'Sendangsari', NULL, NULL),
('3401070007', '3401070', 'Sidomulyo', NULL, NULL),
('3401080001', '3401080', 'Hargomulyo', NULL, NULL),
('3401080002', '3401080', 'Hargorejo', NULL, NULL),
('3401080003', '3401080', 'Hargowilis', NULL, NULL),
('3401080004', '3401080', 'Kalirejo', NULL, NULL),
('3401080005', '3401080', 'Hargotirto', NULL, NULL),
('3401090001', '3401090', 'Jatimulyo', NULL, NULL),
('3401090002', '3401090', 'Giripurwo', NULL, NULL),
('3401090003', '3401090', 'Pendoworejo', NULL, NULL),
('3401090004', '3401090', 'Purwosari', NULL, NULL),
('3401100001', '3401100', 'Banyuroto', NULL, NULL),
('3401100002', '3401100', 'Donomulyo', NULL, NULL),
('3401100003', '3401100', 'Wijimulyo', NULL, NULL),
('3401100004', '3401100', 'Tanjungharjo', NULL, NULL),
('3401100005', '3401100', 'Jati Sarono', NULL, NULL),
('3401100006', '3401100', 'Kembang', NULL, NULL),
('3401110001', '3401110', 'Banjararum', NULL, NULL),
('3401110002', '3401110', 'Banjarasri', NULL, NULL),
('3401110003', '3401110', 'Banjarharjo', NULL, NULL),
('3401110004', '3401110', 'Banjaroyo', NULL, NULL),
('3401120001', '3401120', 'Kebon Harjo', '-7.679099', '110.165921'),
('3401120002', '3401120', 'Banjarsari', NULL, NULL),
('3401120003', '3401120', 'Purwoharjo', NULL, NULL),
('3401120004', '3401120', 'Sidoharjo', NULL, NULL),
('3401120005', '3401120', 'Gerbosari', NULL, NULL),
('3401120006', '3401120', 'Ngargosari', NULL, NULL),
('3401120007', '3401120', 'Pagerharjo', NULL, NULL),
('3402010001', '3402010', 'Poncosari', NULL, NULL),
('3402010002', '3402010', 'Trimurti', NULL, NULL),
('3402020001', '3402020', 'Gadingsari', NULL, NULL),
('3402020002', '3402020', 'Gadingharjo', NULL, NULL),
('3402020003', '3402020', 'Srigading', NULL, NULL),
('3402020004', '3402020', 'Murtigading', NULL, NULL),
('3402030001', '3402030', 'Tirtohargo', NULL, NULL),
('3402030002', '3402030', 'Parangtritis', NULL, NULL),
('3402030003', '3402030', 'Donotirto', NULL, NULL),
('3402030004', '3402030', 'Tirtosari', NULL, NULL),
('3402030005', '3402030', 'Tirtomulyo', NULL, NULL),
('3402040001', '3402040', 'Seloharjo', NULL, NULL),
('3402040002', '3402040', 'Panjangrejo', NULL, NULL),
('3402040003', '3402040', 'Srihardono', NULL, NULL),
('3402050001', '3402050', 'Sidomulyo', NULL, NULL),
('3402050002', '3402050', 'Mulyodadi', NULL, NULL),
('3402050003', '3402050', 'Sumbermulyo', NULL, NULL),
('3402060001', '3402060', 'Caturharjo', NULL, NULL),
('3402060002', '3402060', 'Triharjo', NULL, NULL),
('3402060003', '3402060', 'Gilangharjo', NULL, NULL),
('3402060004', '3402060', 'Wijirejo', NULL, NULL),
('3402070001', '3402070', 'Palbapang', NULL, NULL),
('3402070002', '3402070', 'Ringin Harjo', NULL, NULL),
('3402070003', '3402070', 'Bantul', NULL, NULL),
('3402070004', '3402070', 'Trirenggo', NULL, NULL),
('3402070005', '3402070', 'Sabdodadi', NULL, NULL),
('3402080001', '3402080', 'Patalan', NULL, NULL),
('3402080002', '3402080', 'Canden', NULL, NULL),
('3402080003', '3402080', 'Sumber Agung', NULL, NULL),
('3402080004', '3402080', 'Trimulyo', NULL, NULL),
('3402090001', '3402090', 'Selopamioro', NULL, NULL),
('3402090002', '3402090', 'Sriharjo', NULL, NULL),
('3402090003', '3402090', 'Kebon Agung', NULL, NULL),
('3402090004', '3402090', 'Karang Tengah', NULL, NULL),
('3402090005', '3402090', 'Girirejo', NULL, NULL),
('3402090006', '3402090', 'Karangtalun', NULL, NULL),
('3402090007', '3402090', 'Imogiri', NULL, NULL),
('3402090008', '3402090', 'Wukirsari', NULL, NULL),
('3402100001', '3402100', 'Mangunan', NULL, NULL),
('3402100002', '3402100', 'Muntuk', NULL, NULL),
('3402100003', '3402100', 'Dlingo', NULL, NULL),
('3402100004', '3402100', 'Temuwuh', NULL, NULL),
('3402100005', '3402100', 'Jatimulyo', NULL, NULL),
('3402100006', '3402100', 'Terong', NULL, NULL),
('3402110001', '3402110', 'Wonokromo', NULL, NULL),
('3402110002', '3402110', 'Pleret', NULL, NULL),
('3402110003', '3402110', 'Segoroyoso', NULL, NULL),
('3402110004', '3402110', 'Bawuran', NULL, NULL),
('3402110005', '3402110', 'Wonolelo', NULL, NULL),
('3402120001', '3402120', 'Sitimulyo', NULL, NULL),
('3402120002', '3402120', 'Srimulyo', NULL, NULL),
('3402120003', '3402120', 'Srimartani', NULL, NULL),
('3402130001', '3402130', 'Tamanan', NULL, NULL),
('3402130002', '3402130', 'Jagalan', NULL, NULL),
('3402130003', '3402130', 'Singosaren', NULL, NULL),
('3402130004', '3402130', 'Wirokerten', NULL, NULL),
('3402130005', '3402130', 'Jambidan', NULL, NULL),
('3402130006', '3402130', 'Potorono', NULL, NULL),
('3402130007', '3402130', 'Baturetno', NULL, NULL),
('3402130008', '3402130', 'Banguntapan', NULL, NULL),
('3402140001', '3402140', 'Pendowoharjo', NULL, NULL),
('3402140002', '3402140', 'Timbulharjo', NULL, NULL),
('3402140003', '3402140', 'Bangunharjo', NULL, NULL),
('3402140004', '3402140', 'Panggungharjo', NULL, NULL),
('3402150001', '3402150', 'Bangunjiwo', NULL, NULL),
('3402150002', '3402150', 'Tirtonirmolo', NULL, NULL),
('3402150003', '3402150', 'Tamantirto', NULL, NULL),
('3402150004', '3402150', 'Ngestiharjo', NULL, NULL),
('3402160001', '3402160', 'Triwidadi', NULL, NULL),
('3402160002', '3402160', 'Sendangsari', NULL, NULL),
('3402160003', '3402160', 'Guwosari', NULL, NULL),
('3402170001', '3402170', 'Argodadi', NULL, NULL),
('3402170002', '3402170', 'Argorejo', NULL, NULL),
('3402170003', '3402170', 'Argosari', NULL, NULL),
('3402170004', '3402170', 'Argomulyo', NULL, NULL),
('3403010006', '3403010', 'Giriharjo', NULL, NULL),
('3403010007', '3403010', 'Giriwungu', NULL, NULL),
('3403010008', '3403010', 'Girimulyo', NULL, NULL),
('3403010009', '3403010', 'Girikarto', NULL, NULL),
('3403010010', '3403010', 'Girisekar', NULL, NULL),
('3403010011', '3403010', 'Girisuko', NULL, NULL),
('3403011001', '3403011', 'Girijati', NULL, NULL),
('3403011002', '3403011', 'Giriasih', NULL, NULL),
('3403011003', '3403011', 'Giricahyo', NULL, NULL),
('3403011004', '3403011', 'Giripurwo', NULL, NULL),
('3403011005', '3403011', 'Giritirto', NULL, NULL),
('3403020001', '3403020', 'Karang Duwet', NULL, NULL),
('3403020002', '3403020', 'Karang Asem', NULL, NULL),
('3403020003', '3403020', 'Mulusan', NULL, NULL),
('3403020004', '3403020', 'Giring', NULL, NULL),
('3403020005', '3403020', 'Sodo', NULL, NULL),
('3403020006', '3403020', 'Pampang', NULL, NULL),
('3403020007', '3403020', 'Grogol', NULL, NULL),
('3403030001', '3403030', 'Krambil Sawit', NULL, NULL),
('3403030002', '3403030', 'Kanigoro', NULL, NULL),
('3403030003', '3403030', 'Planjan', NULL, NULL),
('3403030004', '3403030', 'Monggol', NULL, NULL),
('3403030005', '3403030', 'Kepek', NULL, NULL),
('3403030006', '3403030', 'Nglora', NULL, NULL),
('3403030007', '3403030', 'Jetis', NULL, NULL),
('3403040005', '3403040', 'Sidoharjo', NULL, NULL),
('3403040006', '3403040', 'Tepus', NULL, NULL),
('3403040007', '3403040', 'Purwodadi', NULL, NULL),
('3403040008', '3403040', 'Giripanggung', NULL, NULL),
('3403040009', '3403040', 'Sumber Wungu', NULL, NULL),
('3403041001', '3403041', 'Kemadang', NULL, NULL),
('3403041002', '3403041', 'Kemiri', NULL, NULL),
('3403041003', '3403041', 'Banjarejo', NULL, NULL),
('3403041004', '3403041', 'Ngestirejo', NULL, NULL),
('3403041005', '3403041', 'Hargosari', NULL, NULL),
('3403050007', '3403050', 'Melikan', NULL, NULL),
('3403050010', '3403050', 'Bohol', NULL, NULL),
('3403050011', '3403050', 'Pringombo', NULL, NULL),
('3403050012', '3403050', 'Botodayakan', NULL, NULL),
('3403050013', '3403050', 'Petir', NULL, NULL),
('3403050014', '3403050', 'Semugih', NULL, NULL),
('3403050015', '3403050', 'Karangwuni', NULL, NULL),
('3403050016', '3403050', 'Pucanganom', NULL, NULL),
('3403051001', '3403051', 'Balong', NULL, NULL),
('3403051002', '3403051', 'Jepitu', NULL, NULL),
('3403051003', '3403051', 'Karangawen', NULL, NULL),
('3403051004', '3403051', 'Tileng', NULL, NULL),
('3403051005', '3403051', 'Nglindur', NULL, NULL),
('3403051006', '3403051', 'Jerukwudel', NULL, NULL),
('3403051007', '3403051', 'Pucung', NULL, NULL),
('3403051008', '3403051', 'Songbanyu', NULL, NULL),
('3403060001', '3403060', 'Pacarejo', NULL, NULL),
('3403060002', '3403060', 'Candirejo', NULL, NULL),
('3403060003', '3403060', 'Dadapayu', NULL, NULL),
('3403060004', '3403060', 'Ngeposari', NULL, NULL),
('3403060005', '3403060', 'Semanu', NULL, NULL),
('3403070001', '3403070', 'Gombang', NULL, NULL),
('3403070002', '3403070', 'Sidorejo', NULL, NULL),
('3403070003', '3403070', 'Bedoyo', NULL, NULL),
('3403070004', '3403070', 'Karang Asem', NULL, NULL),
('3403070005', '3403070', 'Ponjong', NULL, NULL),
('3403070006', '3403070', 'Genjahan', NULL, NULL),
('3403070007', '3403070', 'Sumber Giri', NULL, NULL),
('3403070008', '3403070', 'Kenteng', NULL, NULL),
('3403070009', '3403070', 'Tambakromo', NULL, NULL),
('3403070010', '3403070', 'Sawahan', NULL, NULL),
('3403070011', '3403070', 'Umbul Rejo', NULL, NULL),
('3403080001', '3403080', 'Bendungan', NULL, NULL),
('3403080002', '3403080', 'Bejiharjo', NULL, NULL),
('3403080003', '3403080', 'Wiladeg', NULL, NULL),
('3403080004', '3403080', 'Kelor', NULL, NULL),
('3403080005', '3403080', 'Ngipak', NULL, NULL),
('3403080006', '3403080', 'Karangmojo', NULL, NULL),
('3403080007', '3403080', 'Gedang Rejo', NULL, NULL),
('3403080008', '3403080', 'Ngawis', NULL, NULL),
('3403080009', '3403080', 'Jati Ayu', NULL, NULL),
('3403090001', '3403090', 'Wunung', NULL, NULL),
('3403090002', '3403090', 'Mulo', NULL, NULL),
('3403090003', '3403090', 'Duwet', NULL, NULL),
('3403090004', '3403090', 'Wareng', NULL, NULL),
('3403090005', '3403090', 'Pulutan', NULL, NULL),
('3403090006', '3403090', 'Siraman', NULL, NULL),
('3403090007', '3403090', 'Karang Rejek', NULL, NULL),
('3403090008', '3403090', 'Baleharjo', NULL, NULL),
('3403090009', '3403090', 'Selang', NULL, NULL),
('3403090010', '3403090', 'Wonosari', NULL, NULL),
('3403090011', '3403090', 'Kepek', NULL, NULL),
('3403090012', '3403090', 'Piyaman', NULL, NULL),
('3403090013', '3403090', 'Karang Tengah', NULL, NULL),
('3403090014', '3403090', 'Gari', NULL, NULL),
('3403100001', '3403100', 'Banyusoco', NULL, NULL),
('3403100002', '3403100', 'Plembutan', NULL, NULL),
('3403100003', '3403100', 'Bleberan', NULL, NULL),
('3403100004', '3403100', 'Getas', NULL, NULL),
('3403100005', '3403100', 'Dengok', NULL, NULL),
('3403100006', '3403100', 'Ngunut', NULL, NULL),
('3403100007', '3403100', 'Playen', NULL, NULL),
('3403100008', '3403100', 'Ngawu', NULL, NULL),
('3403100009', '3403100', 'Bandung', NULL, NULL),
('3403100010', '3403100', 'Logandeng', NULL, NULL),
('3403100011', '3403100', 'Gading', NULL, NULL),
('3403100012', '3403100', 'Banaran', NULL, NULL),
('3403100013', '3403100', 'Ngleri', NULL, NULL),
('3403110001', '3403110', 'Semoyo', NULL, NULL),
('3403110002', '3403110', 'Pengkok', NULL, NULL),
('3403110003', '3403110', 'Beji', NULL, NULL),
('3403110004', '3403110', 'Bunder', NULL, NULL),
('3403110005', '3403110', 'Nglegi', NULL, NULL),
('3403110006', '3403110', 'Putat', NULL, NULL),
('3403110007', '3403110', 'Salam', NULL, NULL),
('3403110008', '3403110', 'Patuk', NULL, NULL),
('3403110009', '3403110', 'Ngoro Oro', NULL, NULL),
('3403110010', '3403110', 'Nglanggeran', NULL, NULL),
('3403110011', '3403110', 'Terbah', NULL, NULL),
('3403120001', '3403120', 'Ngalang', NULL, NULL),
('3403120002', '3403120', 'Hargo Mulyo', NULL, NULL),
('3403120003', '3403120', 'Mertelu', NULL, NULL),
('3403120004', '3403120', 'Tegalrejo', NULL, NULL),
('3403120005', '3403120', 'Watu Gajah', NULL, NULL),
('3403120006', '3403120', 'Sampang', NULL, NULL),
('3403120007', '3403120', 'Serut', NULL, NULL),
('3403130001', '3403130', 'Kedung Keris', NULL, NULL),
('3403130002', '3403130', 'Nglipar', NULL, NULL),
('3403130003', '3403130', 'Pengkol', NULL, NULL),
('3403130004', '3403130', 'Kedungpoh', NULL, NULL),
('3403130005', '3403130', 'Katongan', NULL, NULL),
('3403130006', '3403130', 'Pilang Rejo', NULL, NULL),
('3403130007', '3403130', 'Natah', NULL, NULL),
('3403140001', '3403140', 'Watu Sigar', NULL, NULL),
('3403140002', '3403140', 'Beji', NULL, NULL),
('3403140003', '3403140', 'Kampung', NULL, NULL),
('3403140004', '3403140', 'Jurang Jero', NULL, NULL),
('3403140005', '3403140', 'Sambirejo', NULL, NULL),
('3403140006', '3403140', 'Tancep', NULL, NULL),
('3403150001', '3403150', 'Kalitekuk', NULL, NULL),
('3403150002', '3403150', 'Kemejing', NULL, NULL),
('3403150003', '3403150', 'Semin', NULL, NULL),
('3403150004', '3403150', 'Pundung Sari', NULL, NULL),
('3403150005', '3403150', 'Karang Sari', NULL, NULL),
('3403150006', '3403150', 'Rejosari', NULL, NULL),
('3403150007', '3403150', 'Bulurejo', NULL, NULL),
('3403150008', '3403150', 'Bendung', NULL, NULL),
('3403150009', '3403150', 'Sumberrejo', NULL, NULL),
('3403150010', '3403150', 'Candi Rejo', NULL, NULL),
('3404010001', '3404010', 'Sumberrahayu', NULL, NULL),
('3404010002', '3404010', 'Sumbersari', NULL, NULL),
('3404010003', '3404010', 'Sumber Agung', NULL, NULL),
('3404010004', '3404010', 'Sumberarum', NULL, NULL),
('3404020001', '3404020', 'Sendang Mulyo', NULL, NULL),
('3404020002', '3404020', 'Sendang Arum', NULL, NULL),
('3404020003', '3404020', 'Sendang Rejo', NULL, NULL),
('3404020004', '3404020', 'Sendangsari', NULL, NULL),
('3404020005', '3404020', 'Sendangagung', NULL, NULL),
('3404030001', '3404030', 'Margoluwih', NULL, NULL),
('3404030002', '3404030', 'Margodadi', NULL, NULL),
('3404030003', '3404030', 'Margomulyo', NULL, NULL),
('3404030004', '3404030', 'Margoagung', NULL, NULL),
('3404030005', '3404030', 'Margokaton', NULL, NULL),
('3404040001', '3404040', 'Sidorejo', NULL, NULL),
('3404040002', '3404040', 'Sidoluhur', NULL, NULL),
('3404040003', '3404040', 'Sidomulyo', NULL, NULL),
('3404040004', '3404040', 'Sidoagung', NULL, NULL),
('3404040005', '3404040', 'Sidokarto', NULL, NULL),
('3404040006', '3404040', 'Sidoarum', NULL, NULL),
('3404040007', '3404040', 'Sidomoyo', NULL, NULL),
('3404050001', '3404050', 'Balecatur', NULL, NULL),
('3404050002', '3404050', 'Ambarketawang', NULL, NULL),
('3404050003', '3404050', 'Banyuraden', NULL, NULL),
('3404050004', '3404050', 'Nogotirto', NULL, NULL),
('3404050005', '3404050', 'Trihanggo', NULL, NULL),
('3404060001', '3404060', 'Tirtoadi', NULL, NULL),
('3404060002', '3404060', 'Sumberadi', NULL, NULL),
('3404060003', '3404060', 'Tlogoadi', NULL, NULL),
('3404060004', '3404060', 'Sendangadi', NULL, NULL),
('3404060005', '3404060', 'Sinduadi', NULL, NULL),
('3404070001', '3404070', 'Catur Tunggal', NULL, NULL),
('3404070002', '3404070', 'Maguwoharjo', NULL, NULL),
('3404070003', '3404070', 'Condong Catur', NULL, NULL),
('3404080001', '3404080', 'Sendang Tirto', NULL, NULL),
('3404080002', '3404080', 'Tegal Tirto', NULL, NULL),
('3404080003', '3404080', 'Jogo Tirto', NULL, NULL),
('3404080004', '3404080', 'Kali Tirto', NULL, NULL),
('3404090001', '3404090', 'Sumber Harjo', NULL, NULL),
('3404090002', '3404090', 'Wukir Harjo', NULL, NULL),
('3404090003', '3404090', 'Gayam Harjo', NULL, NULL),
('3404090004', '3404090', 'Sambi Rejo', NULL, NULL),
('3404090005', '3404090', 'Madu Rejo', NULL, NULL),
('3404090006', '3404090', 'Boko Harjo', NULL, NULL),
('3404100001', '3404100', 'Purwo Martani', NULL, NULL),
('3404100002', '3404100', 'Tirto Martani', NULL, NULL),
('3404100003', '3404100', 'Taman Martani', NULL, NULL),
('3404100004', '3404100', 'Selo Martani', NULL, NULL),
('3404110001', '3404110', 'Wedomartani', NULL, NULL),
('3404110002', '3404110', 'Umbulmartani', NULL, NULL),
('3404110003', '3404110', 'Widodo Martani', NULL, NULL),
('3404110004', '3404110', 'Bimo Martani', NULL, NULL),
('3404110005', '3404110', 'Sindumartani', NULL, NULL),
('3404120001', '3404120', 'Sari Harjo', NULL, NULL),
('3404120002', '3404120', 'Sinduharjo', NULL, NULL),
('3404120003', '3404120', 'Minomartani', NULL, NULL),
('3404120004', '3404120', 'Suko Harjo', NULL, NULL),
('3404120005', '3404120', 'Sardonoharjo', NULL, NULL),
('3404120006', '3404120', 'Donoharjo', NULL, NULL),
('3404130001', '3404130', 'Catur Harjo', NULL, NULL),
('3404130002', '3404130', 'Triharjo', NULL, NULL),
('3404130003', '3404130', 'Tridadi', NULL, NULL),
('3404130004', '3404130', 'Pandowo Harjo', NULL, NULL),
('3404130005', '3404130', 'Tri Mulyo', NULL, NULL),
('3404140001', '3404140', 'Banyu Rejo', NULL, NULL),
('3404140002', '3404140', 'Tambak Rejo', NULL, NULL),
('3404140003', '3404140', 'Sumber Rejo', NULL, NULL),
('3404140004', '3404140', 'Pondok Rejo', NULL, NULL),
('3404140005', '3404140', 'Moro Rejo', NULL, NULL),
('3404140006', '3404140', 'Margo Rejo', NULL, NULL),
('3404140007', '3404140', 'Lumbung Rejo', NULL, NULL),
('3404140008', '3404140', 'Merdiko Rejo', NULL, NULL),
('3404150001', '3404150', 'Bangun Kerto', NULL, NULL),
('3404150002', '3404150', 'Donokerto', NULL, NULL),
('3404150003', '3404150', 'Giri Kerto', NULL, NULL),
('3404150004', '3404150', 'Wono Kerto', NULL, NULL),
('3404160001', '3404160', 'Purwo Binangun', NULL, NULL),
('3404160002', '3404160', 'Candi Binangun', NULL, NULL),
('3404160003', '3404160', 'Harjo Binangun', NULL, NULL),
('3404160004', '3404160', 'Pakem Binangun', NULL, NULL),
('3404160005', '3404160', 'Hargo Binangun', NULL, NULL),
('3404170001', '3404170', 'Wukir Sari', NULL, NULL),
('3404170002', '3404170', 'Argo Mulyo', NULL, NULL),
('3404170003', '3404170', 'Glagah Harjo', NULL, NULL),
('3404170004', '3404170', 'Kepuh Harjo', NULL, NULL),
('3404170005', '3404170', 'Umbul Harjo', NULL, NULL),
('3471010001', '3471010', 'Gedongkiwo', NULL, NULL),
('3471010002', '3471010', 'Suryodiningratan', NULL, NULL),
('3471010003', '3471010', 'Mantrijeron', NULL, NULL),
('3471020001', '3471020', 'Patehan', '-7.811142', '110.359925'),
('3471020002', '3471020', 'Panembahan', '-7.807664', '110.365139'),
('3471020003', '3471020', 'Kadipaten', '-7.806069', '110.358436'),
('3471030001', '3471030', 'Brontokusuman', '-7.820234', '110.371096'),
('3471030002', '3471030', 'Keparakan', '-7.812513', '110.371096'),
('3471030003', '3471030', 'Wirogunan', '-7.807069', '110.377054'),
('3471040001', '3471040', 'Giwangan', '-7.829633', '110.388970'),
('3471040002', '3471040', 'Sorosutan', '-7.823572', '110.381523'),
('3471040003', '3471040', 'Pandeyan', '-7.816766', '110.388970'),
('3471040004', '3471040', 'Warungboto', '-7.809046', '110.388970'),
('3471040005', '3471040', 'Tahunan', '-7.806771', '110.383012'),
('3471040006', '3471040', 'Muja Muju', '-7.798606', '110.391949'),
('3471040007', '3471040', 'Semaki', '-7.799053', '110.383012'),
('3471050001', '3471050', 'Prenggan', '-7.821464', '110.397907'),
('3471050002', '3471050', 'Purbayan', '-7.827068', '110.401631'),
('3471050003', '3471050', 'Rejowinangun', '-7.811172', '110.397907'),
('3471060001', '3471060', 'Baciro', '-7.791186', '110.385991'),
('3471060002', '3471060', 'Demangan', '-7.786573', '110.388226'),
('3471060003', '3471060', 'Klitren', '-7.783618', '110.383012'),
('3471060004', '3471060', 'Kotabaru', '-7.786636', '110.374075'),
('3471060005', '3471060', 'Terban', '-7.778919', '110.374075'),
('3471070001', '3471070', 'Suryatmajan', '-7.794045', '110.367373'),
('3471070002', '3471070', 'Tegal Panggung', '-7.791929', '110.371096'),
('3471070003', '3471070', 'Bausasran', '-7.793599', '110.376310'),
('3471080001', '3471080', 'Purwo Kinanti', '-7.799499', '110.374075'),
('3471080002', '3471080', 'Gunung Ketur', '-7.801243', '110.377799'),
('3471090001', '3471090', 'Prawirodirjan', '-7.803725', '110.369443'),
('3471090002', '3471090', 'Ngupasan', '-7.799121', '110.365111'),
('3471100001', '3471100', 'Notoprajan', '-7.802964', '110.356202'),
('3471100002', '3471100', 'Ngampilan', '-7.797669', '110.359181'),
('3471110001', '3471110', 'Patangpuluhan', '-7.810982', '110.350244'),
('3471110002', '3471110', 'Wirobrajan', '-7.803262', '110.350244'),
('3471110003', '3471110', 'Pakuncen', '-7.798115', '110.350244'),
('3471120001', '3471120', 'Pringgokusuman', '-7.792524', '110.359181'),
('3471120002', '3471120', 'Sosromenduran', '-7.791621', '110.364394'),
('3471130001', '3471130', 'Bumijo', '-7.784806', '110.359181'),
('3471130002', '3471130', 'Gowongan', '-7.784509', '110.365139'),
('3471130003', '3471130', 'Cokrodiningratan', '-7.779216', '110.368117'),
('3471140001', '3471140', 'Tegalrejo', '-7.460321', '110.275775'),
('3471140002', '3471140', 'Bener', '-7.779957', '110.353223'),
('3471140003', '3471140', 'Kricak', '-7.774517', '110.359181'),
('3471140004', '3471140', 'Karangwaru', '-7.773614', '110.364394');

-- --------------------------------------------------------

--
-- Table structure for table `wilayah_kabupaten`
--

CREATE TABLE `wilayah_kabupaten` (
  `id` varchar(4) NOT NULL,
  `provinsi_id` varchar(2) NOT NULL DEFAULT '',
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wilayah_kabupaten`
--

INSERT INTO `wilayah_kabupaten` (`id`, `provinsi_id`, `nama`) VALUES
('3401', '34', 'Kab. Kulon Progo'),
('3402', '34', 'Kab. Bantul'),
('3403', '34', 'Kab. Gunung Kidul'),
('3404', '34', 'Kab. Sleman'),
('3471', '34', 'Kota Yogyakarta');

-- --------------------------------------------------------

--
-- Table structure for table `wilayah_kecamatan`
--

CREATE TABLE `wilayah_kecamatan` (
  `id` varchar(7) NOT NULL,
  `kabupaten_id` varchar(4) NOT NULL DEFAULT '',
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wilayah_kecamatan`
--

INSERT INTO `wilayah_kecamatan` (`id`, `kabupaten_id`, `nama`) VALUES
('3401010', '3401', ' Temon'),
('3401020', '3401', ' Wates'),
('3401030', '3401', ' Panjatan'),
('3401040', '3401', ' Galur'),
('3401050', '3401', ' Lendah'),
('3401060', '3401', ' Sentolo'),
('3401070', '3401', ' Pengasih'),
('3401080', '3401', ' Kokap'),
('3401090', '3401', ' Girimulyo'),
('3401100', '3401', ' Nanggulan'),
('3401110', '3401', ' Kalibawang'),
('3401120', '3401', ' Samigaluh'),
('3402010', '3402', ' Srandakan'),
('3402020', '3402', ' Sanden'),
('3402030', '3402', ' Kretek'),
('3402040', '3402', ' Pundong'),
('3402050', '3402', ' Bambang Lipuro'),
('3402060', '3402', ' Pandak'),
('3402070', '3402', ' Bantul'),
('3402080', '3402', ' Jetis'),
('3402090', '3402', ' Imogiri'),
('3402100', '3402', ' Dlingo'),
('3402110', '3402', ' Pleret'),
('3402120', '3402', ' Piyungan'),
('3402130', '3402', ' Banguntapan'),
('3402140', '3402', ' Sewon'),
('3402150', '3402', ' Kasihan'),
('3402160', '3402', ' Pajangan'),
('3402170', '3402', ' Sedayu'),
('3403010', '3403', ' Panggang'),
('3403011', '3403', ' Purwosari'),
('3403020', '3403', ' Paliyan'),
('3403030', '3403', ' Sapto Sari'),
('3403040', '3403', ' Tepus'),
('3403041', '3403', ' Tanjungsari'),
('3403050', '3403', ' Rongkop'),
('3403051', '3403', ' Girisubo'),
('3403060', '3403', ' Semanu'),
('3403070', '3403', ' Ponjong'),
('3403080', '3403', ' Karangmojo'),
('3403090', '3403', ' Wonosari'),
('3403100', '3403', ' Playen'),
('3403110', '3403', ' Patuk'),
('3403120', '3403', ' Gedang Sari'),
('3403130', '3403', ' Nglipar'),
('3403140', '3403', ' Ngawen'),
('3403150', '3403', ' Semin'),
('3404010', '3404', ' Moyudan'),
('3404020', '3404', ' Minggir'),
('3404030', '3404', ' Seyegan'),
('3404040', '3404', ' Godean'),
('3404050', '3404', ' Gamping'),
('3404060', '3404', ' Mlati'),
('3404070', '3404', ' Depok'),
('3404080', '3404', ' Berbah'),
('3404090', '3404', ' Prambanan'),
('3404100', '3404', ' Kalasan'),
('3404110', '3404', ' Ngemplak'),
('3404120', '3404', ' Ngaglik'),
('3404130', '3404', ' Sleman'),
('3404140', '3404', ' Tempel'),
('3404150', '3404', ' Turi'),
('3404160', '3404', ' Pakem'),
('3404170', '3404', ' Cangkringan'),
('3471010', '3471', ' Mantrijeron'),
('3471020', '3471', ' Kraton'),
('3471030', '3471', ' Mergangsan'),
('3471040', '3471', ' Umbulharjo'),
('3471050', '3471', ' Kotagede'),
('3471060', '3471', ' Gondokusuman'),
('3471070', '3471', ' Danurejan'),
('3471080', '3471', ' Pakualaman'),
('3471090', '3471', ' Gondomanan'),
('3471100', '3471', ' Ngampilan'),
('3471110', '3471', ' Wirobrajan'),
('3471120', '3471', ' Gedong Tengen'),
('3471130', '3471', ' Jetis'),
('3471140', '3471', ' Tegalrejo');

-- --------------------------------------------------------

--
-- Table structure for table `wilayah_provinsi`
--

CREATE TABLE `wilayah_provinsi` (
  `id` varchar(2) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wilayah_provinsi`
--

INSERT INTO `wilayah_provinsi` (`id`, `nama`) VALUES
('11', 'Aceh'),
('12', 'Sumatera Utara'),
('13', 'Sumatera Barat'),
('14', 'Riau'),
('15', 'Jambi'),
('16', 'Sumatera Selatan'),
('17', 'Bengkulu'),
('18', 'Lampung'),
('19', 'Kepulauan Bangka Belitung'),
('21', 'Kepulauan Riau'),
('31', 'Dki Jakarta'),
('32', 'Jawa Barat'),
('33', 'Jawa Tengah'),
('34', 'Di Yogyakarta'),
('35', 'Jawa Timur'),
('36', 'Banten'),
('51', 'Bali'),
('52', 'Nusa Tenggara Barat'),
('53', 'Nusa Tenggara Timur'),
('61', 'Kalimantan Barat'),
('62', 'Kalimantan Tengah'),
('63', 'Kalimantan Selatan'),
('64', 'Kalimantan Timur'),
('65', 'Kalimantan Utara'),
('71', 'Sulawesi Utara'),
('72', 'Sulawesi Tengah'),
('73', 'Sulawesi Selatan'),
('74', 'Sulawesi Tenggara'),
('75', 'Gorontalo'),
('76', 'Sulawesi Barat'),
('81', 'Maluku'),
('82', 'Maluku Utara'),
('91', 'Papua Barat'),
('94', 'Papua');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perdais`
--
ALTER TABLE `perdais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prioritas`
--
ALTER TABLE `prioritas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skpd`
--
ALTER TABLE `skpd`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `skpd_email_unique` (`email`);

--
-- Indexes for table `wilayah_desa`
--
ALTER TABLE `wilayah_desa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wilayah_kabupaten`
--
ALTER TABLE `wilayah_kabupaten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wilayah_kecamatan`
--
ALTER TABLE `wilayah_kecamatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wilayah_provinsi`
--
ALTER TABLE `wilayah_provinsi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `perdais`
--
ALTER TABLE `perdais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `prioritas`
--
ALTER TABLE `prioritas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
