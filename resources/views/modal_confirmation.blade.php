<script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
$.validate({
    form : '#request',
    onSuccess : function() {
        waiting();
    }
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        {{Form::open(array('url'=>$uri, 'method'=>'POST','id'=>'request'))}}
        <input type="hidden" name="type" value="{{$value}}">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button close-waiting" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{$confirmation}}</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                  <p>{{$msg}}</p>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">OK</button>
            </div>
        </div>
        {{Form::close()}}
    </div>
</div>