@extends($template)

@section('head-script')
  @parent
  <style type="text/css">
    #map {
      height: 500px;
    }
    .hr-map{
      margin-top: 0;
      margin-bottom: 0;
    }
  </style>
@endsection

@section('body-content')
@if(count($dateShow) > 0)
<div class="callout callout-warning">
    <h4>Pengumuman!</h4>
    <ul>
      @foreach($dateShow as $i => $row)
                <li>{{ $row['content'] }}</li>
      @endforeach
    </ul>
</div>
@endif

<div class="row">
<div class="col-md-12">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Lokasi Usulan</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <div class="btn-group">
          <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>
        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div><!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <p class="text-center">
            {{-- <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong> --}}
          </p>
          <div class="chart">
                <div id="map"></div>
          </div><!-- /.chart-responsive -->
        </div><!-- /.col -->
 
      </div><!-- /.row -->
    </div><!-- ./box-body -->
 
  </div><!-- /.box -->
</div><!-- /.col -->
</div><!-- /.row -->

<!-- Main row -->
<div class="row">
<div class="col-md-12">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Usulan</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div><!-- /.box-header -->
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th>#</th>
              <th>Program</th>
              <th>Kegiatan</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
         <?php
         $nomor = 1;
         ?>
          @foreach($index as $row)
          <tr>
            <td>{{$nomor++}}</td>
            <td>{!!Str::words($row->program->name,5,' ...')!!}</td>
            <td>{!!Str::words($row->kegiatan->name,5,' ...')!!}</td>
            <td>
               @if($row->is_approved==1)
                  <span class="label label-success">Approved</span>
               @elseif($row->is_approved==2)
                  <span class="label label-info">Revision</span>
               @elseif($row->is_approved==3)
                  <span class="label label-danger">Denied</span>
               @else
                  <span class="label label-warning">Pending</span>
               @endif
            </td>
            <td>
                <a href="{{url($uri.'/show/'.$row->id)}}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-info"></i> {{trans('button.det')}}</a>
            </td>
          </tr>
          @endforeach
          </tbody>
        </table>
      </div><!-- /.table-responsive -->
    </div><!-- ./box-body -->
    <div class="box-footer clearfix">
      <a href="{{url($uri)}}" class="btn btn-sm btn-default btn-flat pull-right">Lihat Semua</a>
    </div><!-- /.box-footer -->
  </div><!-- /.box -->
</div><!-- /.col -->
</div><!-- /.row -->
@endsection

@section('end-script')
  @parent
  <script>
  <?php $tot = count($usulanMap); ?>
  var usulanMap = [
  @foreach($usulanMap as $i => $row)
    @if(!empty($row['latitude'])&&!empty($row['longitude']))
      @if(($i+1)==$tot)
        [{{$row['latitude']}},{{$row['longitude']}}, "{{$row['kabupaten']}}, {{$row['kecamatan']}}, {{$row['desa']}}", "{{$row['program']}}", "{{$row['kegiatan']}}", "{{$row['tolak']}}", "Rp {{$row['anggaran']}}", "{{$row['status']}}"]
      @else
        [{{$row['latitude']}},{{$row['longitude']}}, "{{$row['kabupaten']}}, {{$row['kecamatan']}}, {{$row['desa']}}", "{{$row['program']}}", "{{$row['kegiatan']}}", "{{$row['tolak']}}", "Rp {{$row['anggaran']}}", "{{$row['status']}}"],
      @endif
    @endif
  @endforeach
  ];

      var map;
      function initMap() {
        var infowindow = new google.maps.InfoWindow();
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -7.800868, lng: 110.373299},
          // mapTypeControl: false,
          streetViewControl: false,
          scrollwheel: false,
          zoom: 11
        });

        for (var i = 0; i < usulanMap.length; i++) {
              var marker = new google.maps.Marker({
                  position: {lat: usulanMap[i][0], lng: usulanMap[i][1]},
                  map: map,
                  icon: imageskpd(usulanMap[i][7]),
                  lokasi: usulanMap[i][2],
                  program: usulanMap[i][3],
                  kegiatan: usulanMap[i][4],
                  tolak: usulanMap[i][5],
                  anggaran: usulanMap[i][6],
                  status: statusSkpd(usulanMap[i][7])
              });
          // google.maps.event.addListener(marker, 'click', function() {
          //       window.location.href = this.url;
          //   });
          map.addListener('center_changed', function() {
             // 3 seconds after the center of the map has changed, pan back to the
             // marker.
             window.setTimeout(function() {
               map.panTo(this.getPosition());
             }, 3000);
           });

           marker.addListener('click', function() {
             map.setZoom(19);
             map.setCenter(this.getPosition());
           });

          google.maps.event.addListener(marker, 'mouseover', function() {

            var content = "<div class='infowindow-content'><b>Lokasi</b> : "+this.lokasi+"<br><hr class='hr-map'> <b>Program</b> : "+this.program+"<br><hr class='hr-map'> <b>Kegiatan</b> : "+this.kegiatan+"<br><hr class='hr-map'> <b>Tolak Ukur</b> : "+this.tolak+"<br><hr class='hr-map'> <b>Anggaran</b> : "+this.anggaran+"<br><hr class='hr-map'> <b>Keterangan</b> : "+this.status+"</div>" ;

            infowindow.setContent(content);
            infowindow.open(map, this);
          });

          google.maps.event.addListener(marker, 'mouseout', function() {
            if(infowindow)
            {
              infowindow.close()
            }
          });

        }
      }

      
    function statusSkpd(status){
       if(status==1){
          return 'Approved';
       }else if(status==2){
          return 'Revision';
       }else if(status==3){
          return 'Denied';
       }else{
          return 'Pending';
       }
    }

    function imageskpd(status){
      if(status==0) {
        return '{{asset("marker/mm_20_red.png")}}';
      }else if(status==1) {
        return '{{asset("marker/mm_20_green.png")}}';
      }else if(status==2) {
        return '{{asset("marker/mm_20_blue.png")}}';
      }else if(status==3) {
        return '{{asset("marker/mm_20_gray.png")}}';
      }
    }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8HshEMMYjzaBhabEOoWmW2-Ix2lN8T8k&callback=initMap" async defer></script>

@endsection