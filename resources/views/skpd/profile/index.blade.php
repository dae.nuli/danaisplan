@extends($template)

@section('body-content')

<div class="callout callout-danger">
    <h4>Warning!</h4>
    <p>Silahkan lengkapi data profil anda, agar bisa menggunakan aplikasi ini.</p>
</div>
<div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Data Penanggung Jawab</h3>
    </div>
    <div class="box-body">
      {!! Form::open(array('url' => $uri,'method'=>'POST','id'=>'request')) !!}
          <div class="box-body lengkapi">
            <div class="form-group">
              <div class="row">
                <div class="col-xs-12">
                    <label>Nama</label>
                    <input type="text" name="nama" class="form-control" autocomplete="off" data-validation="required" data-validation-error-msg="This field is required.">
                </div>
            </div>
            </div>
            <div class="form-group">
              <div class="row">
                  <div class="col-xs-12">
                    <label>Alamat Email</label>
                    <input type="email" name="email" class="form-control" autocomplete="off" data-validation="email">
                  </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                  <div class="col-xs-12">
                    <label>Nomor Telefon</label>
                    <input type="text" name="phone" class="form-control" autocomplete="off" data-validation="number">
                  </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-xs-12">
                  <label>Alamat</label>
                  <input type="text" class="form-control" autocomplete="off" name="alamat" data-validation="required" data-validation-error-msg="This field is required.">
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-xs-12">
                  <label>Password Baru</label>
                  <input type="password" class="form-control" autocomplete="off" name="password" data-validation="length" data-validation-length="min6">
                </div>
              </div>
            </div>
          </div><!-- /.box-body -->

          <div class="box-footer lengkapi">
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>
        {!! Form::close() !!}
    </div><!-- /.box-body -->
</div><!-- /.box -->

@endsection

@section('end-script')
  @parent

<script src="{{asset('js/jquery.blockUI.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
$.validate({
    form : '#request',
    onSuccess : function() {
        waiting();
    }
});

@if(session()->has('success'))
var n = noty({
    layout:'topRight',
    theme:'relax',
    text: '{{session()->get("success")}}',
    animation: {
        open  : 'animated fadeIn',
        close : 'animated fadeOut',
        easing: 'swing', // easing
        speed: 500 // opening & closing animation speed
    },
    type: 'success'
});
n.setTimeout(3000);
@endif
</script>
@endsection