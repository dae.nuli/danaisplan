@extends($template)

@section('body-content')

<div class="callout callout-info">
    <h4>Info</h4>
    <p>Silahkan cek email anda untuk mengaktifkan akun.</p>
</div>
<div class="box box-default collapsed-box">
  <div class="box-header with-border">
    Apabila email aktivasi belum masuk, silahkan <a href="#" class="btn btn-primary btn-xs" data-widget="collapse">kirim ulang</a>
    {{-- Apabila email aktifasi belum masuk, silahkan <a href="{{URL::to('panel/profile')}}" class="btn btn-primary btn-xs" data-widget="collapse">kirim ulang</a> --}}
  </div>
  <div class="box-body" style="display:none">
    {!! Form::open(array('url' => $action,'method'=>'POST','id'=>'request')) !!}
        <div class="box-body lengkapi"> 
          <div class="form-group">
            <div class="row">
              <div class="col-xs-12">
                <label>Alamat Email</label>
                <input type="email" name="email" class="form-control" autocomplete="off" data-validation="email" data-validation-event="keyup">
              </div>
            </div>
          </div>
        </div><!-- /.box-body -->
        <div class="box-footer lengkapi">
          <button type="submit" class="btn btn-primary">Kirim</button>
        </div>
      {!! Form::close() !!}
  </div>
</div>

@endsection

@section('end-script')
  @parent

<script src="{{asset('js/jquery.blockUI.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
$.validate({
    form : '#request',
    onSuccess : function() {
        waiting();
    }
});

@if(session()->has('success'))
var n = noty({
    layout:'topRight',
    theme:'relax',
    text: '{{session()->get("success")}}',
    animation: {
        open  : 'animated fadeIn',
        close : 'animated fadeOut',
        easing: 'swing', // easing
        speed: 500 // opening & closing animation speed
    },
    type: 'success'
});
n.setTimeout(3000);
@endif
</script>
@endsection