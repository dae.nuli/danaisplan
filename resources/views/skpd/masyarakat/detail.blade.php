@extends('layout.content')


@section('body-content')
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {{-- <h4>    <i class="icon fa fa-check"></i> Alert!</h4> --}}
    {{session()->get('success')}} sdsds
    </div>
@endif
<div class="row">
<!-- left column -->
    <div class="col-md-12">
     <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header">
             {{-- <h3 class="box-title">Quick Example</h3> --}}
            </div><!-- /.box-header -->
            {!! Form::open(array('url' => $action,'method'=>'POST')) !!}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" disabled="" value="{{$detail->name}}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Nomor Telefon</label>
                            <input type="text" disabled="" value="{{$detail->phone}}" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" disabled="" value="{{$detail->address}}" class="form-control">
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Kabupaten</label>
                                    <input type="text" disabled="" value="{{$detail->kabupaten->nama}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Kecamatan</label>
                                    <input type="text" disabled="" value="{{$detail->kecamatan->nama}}" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Desa</label>
                                    <input type="text" disabled="" value="{{$detail->desa->nama}}" class="form-control">
                                </div>
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <label>Ringkasan Permasalahan</label>
                            <textarea disabled="" class="form-control">{{$detail->problems_summary}}</textarea>
                        </div> --}}
                        <div class="form-group">
                            <label>Usulan</label>
                            <textarea disabled="" class="form-control">{{$detail->proposal}}</textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Catatan</label>
                            <textarea name="catatan" disabled="" class="form-control" rows="15">{{$detail->note}}</textarea>
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
                <a href="{{url($uri)}}" class="btn btn-warning">Batal</a>
                <input type="submit" class="btn btn-primary text-uppercase" name="type" value="approve">
                <input type="submit" class="btn btn-danger text-uppercase" name="type" value="denied">
            </div>
            {!! Form::close() !!}
        </div><!-- /.box -->
    </div>
</div>
@endsection
