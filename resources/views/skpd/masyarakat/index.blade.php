@extends('layout.content')

@section('body-content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      {{-- <div class="box-header">
        <h3 class="box-title">Responsive Hover Table</h3>
        <div class="box-tools">
          <div class="input-group">
            <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
            <div class="input-group-btn">
              <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div> --}}<!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr class="text-uppercase">
            <th>#</th>
            <th>Kabupaten</th>
            {{-- <th>Ringkasan Permasalahan</th> --}}
            <th>Usulan</th>
            <th>Status</th>
            <th></th>
          </tr>
         <?php 
         if($pages){
             $page = $pages;
         }else{
             $page = 1;
         }
         $nomor = $page + ($page-1) * ($limit-1);
         ?>
          @foreach($index as $row)
          <tr>
            <td>{{$nomor++}}</td>
            <td>{{$row->kabupaten->nama}}</td>
            {{-- <td>{!!Str::words($row->problems_summary,6,' ...')!!}</td> --}}
            <td>{!!Str::words($row->proposal,6,' ...')!!}</td>
            <td>
               @if($row->status==1)
                  <span class="label label-success">Approved</span>
               @elseif($row->status==2)
                  <span class="label label-danger">Denied</span>
               @else
                  <span class="label label-info">New</span>
               @endif
            </td>
            <td>
                <a href="{{url($uri.'/detail/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-info"></i> Detail</a>

                {{-- <a href="{{url($uri.'/delete/'.$row->id)}}" class="btn btn-danger btn-xs confirm"><i class="fa fa-fw fa-trash-o"></i> Delete</a> --}}
            </td>
          </tr>
          @endforeach
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
         {!! $index->links() !!}
      </div>
    </div><!-- /.box -->
</div>
@endsection


@section('end-script')
  @parent
<script src="{{asset('js/confirm-bootstrap.js')}}"></script>
<script src="{{asset('js/confirm.js')}}"></script>
@endsection