@extends('layout.content')

@section('end-script')
    @parent

    <script src="{{asset('js/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
        form : '#request',
        onSuccess : function() {
            waiting();
        }
    });
    $(document).on('change','.program',function(){
        var CSRF_TOKEN = $('input[name="_token"]').attr('value');
        var program = $(this).val();
        $.ajax({
            type : 'POST',
            url  : '/skpd/program',
            data : {_token: CSRF_TOKEN, id_program : program},
            dataType: 'JSON',
            beforeSend : function() {
                $('.kegiatan').html("<option value=''>Loading...</option>");
            },
            success : function(data){
                $('.kegiatan').html(data);
            }
        }).done(function(){
            // $('.kecamatan').val($('.kecamatan').attr('data-selected'));
        });
    });
    </script>
@stop

@section('body-content')
@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
<!-- left column -->
    <div class="col-md-12">
     <!-- general form elements -->
        {!! Form::open(array('url' => $uri,'method'=>'POST','id'=>'request','files'=>true)) !!}
        <div class="box box-primary">
            <div class="box-header">
             {{-- <h3 class="box-title">Quick Example</h3> --}}
            </div><!-- /.box-header -->
       <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Program</label>
                                <select name="program" class="form-control program">
                                    <option>- Pilih Program -</option>
                                    @foreach($program as $pro)
                                        <option value="{{$pro->id}}">{{$pro->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Kegiatan</label>
                                <select name="kegiatan" class="form-control kegiatan">
                                    <option>- Pilih Kegiatan -</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="box box-primary no-border">
            <div class="box-header">
             <h3 class="box-title">Keluaran</h3>
            </div><!-- /.box-header -->
       <!-- form start -->
                <div class="box-body">
                    
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label>Keluaran</label>
                                <input type="text" name="keluaran" class="form-control" data-validation="required" data-validation-error-msg="This field is required.">
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label>Anggaran</label>
                                <input type="text" name="anggaran" class="form-control">
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label>Jumlah</label>
                                <input type="text" name="jumlah" class="form-control" data-validation="required" data-validation-error-msg="This field is required.">
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label>Satuan</label>
                                <input type="text" name="satuan" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label>Kabupaten</label>
                                <select name="kabupaten" class="form-control">
                                    <option>- Pilih Kabupaten -</option>
                                    @foreach($kabupaten as $kab)
                                        <option value="{{$kab->id}}">{{$kab->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label>Kecamatan</label>
                                <select name="kecamatan" class="form-control">
                                    <option>- Pilih Kecamatan -</option>
                                    @foreach($kecamatan as $kec)
                                        <option value="{{$kec->id}}">{{$kec->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="form-group">
                                <label>Desa</label>
                                <select name="desa" class="form-control">
                                    <option>- Pilih Desa -</option>
                                    @foreach($desa as $des)
                                        <option value="{{$des->id}}">{{$des->nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Catatan</label>
                                <textarea name="catatan" rows="5" class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>TOR</label>
                                <input type="file" name="tor">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>RAB</label>
                                <input type="file" name="rab">
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <a href="{{url($uri)}}" class="btn btn-warning">Batal</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
        </div><!-- /.box -->
        {!! Form::close() !!}
    </div>
</div>
@endsection
