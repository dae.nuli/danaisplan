@extends($template)

@section('header-content')

<div class="pull-right" style="margin-left:5px">
    <a href="{{url($uri.'/create')}}" class="btn btn-primary"> <i class="fa fa-plus"></i> Tambah</a>
</div>

@endsection

@section('body-content')
{{-- @if(session()->has('success')) --}}
{{--     <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> --}}
    {{-- <h4><i class="icon fa fa-check"></i></h4> --}}
{{--     {{session()->get('success')}}
    </div> --}}
{{-- @endif --}}
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      {{-- <div class="box-header">
        <h3 class="box-title">Responsive Hover Table</h3>
        <div class="box-tools">
          <div class="input-group">
            <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
            <div class="input-group-btn">
              <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div> --}}<!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr class="text-uppercase">
            <th>#</th>
            <th>Program</th>
            <th>Kegiatan</th>
            <th>Status</th>
            <th>Date</th>
            <th></th>
          </tr>
         <?php 
         if($pages){
             $page = $pages;
         }else{
             $page = 1;
         }
         $nomor = $page + ($page-1) * ($limit-1);
         ?>
          @foreach($index as $row)
          <tr>
            <td>{{$nomor++}}</td>
            <td>{!!Str::words($row->program->name,4,' ...')!!}</td>
            <td>{!!Str::words($row->kegiatan->name,4,' ...')!!}</td>
            <td>{{date('d F Y',strtotime($row->created_at))}}</td>
            <td>
               @if($row->is_approved==1)
                  <span class="label label-success">Approved</span>
               @elseif($row->is_approved==2)
                  <span class="label label-info">Revision</span>
               @elseif($row->is_approved==3)
                  <span class="label label-danger">Denied</span>
               @elseif($row->is_approved==4)
                  <span class="label label-warning">Pending Revision</span>
               @else
                  <span class="label label-warning">Pending</span>
               @endif
            </td>
            <td>
                <a href="{{url($uri.'/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> {{trans('button.ed')}}</a>
                <a href="{{url($uri.'/show/'.$row->id)}}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-info"></i> {{trans('button.det')}}</a>
                
{{--                 {!! Form::open(array('url' => $uri.'/destroy/'.$row->id,'method'=>'POST','class'=>'in-blo')) !!}
                <button type="submit" class="btn btn-danger btn-xs confirm">
                    <i class="fa fa-fw fa-trash-o"></i> {{trans('button.del')}}
                </button>
                {!! Form::close() !!} --}}
                
                <a href="{{url($uri.'/destroy/'.$row->id)}}" class="btn btn-danger btn-xs confirm-href"><i class="fa fa-fw fa-trash-o"></i> Delete</a>
            </td>
          </tr>
          @endforeach
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
         {!! $index->links() !!}
      </div>
    </div><!-- /.box -->
</div>
@endsection

@section('end-script')
  @parent
<script src="{{asset('js/confirm-bootstrap.js')}}"></script>
<script src="{{asset('js/confirm.js')}}"></script>
<script type="text/javascript">
@if(session()->has('success'))
var n = noty({
    layout:'topRight',
    theme:'relax',
    text: '{{session()->get("success")}}',
    animation: {
        open  : 'animated fadeIn',
        close : 'animated fadeOut',
        easing: 'swing', // easing
        speed: 500 // opening & closing animation speed
    },
    type: 'success'
});
n.setTimeout(3000);
@endif
</script>
@endsection