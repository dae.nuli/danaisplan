    <style type="text/css">
    #map {
        height: 400px;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
        z-index: 9999;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
        z-index: 9999;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }
    </style>

<script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
$.validate({
    form : '#request',
    onSuccess : function() {
        waiting();
    }
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        {{Form::open(array('url'=>'#', 'method'=>'POST','id'=>'request'))}}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button close-waiting" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Maps</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <input id="pac-input" class="controls" type="text" placeholder="Cari Jalan">
                                <div id="map"></div>
                            </div>
                        </div> 
                    </div>

                </div>
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default close-waiting pull-left keep-map assign" data-index="{{$index}}" data-latlng="{{(!empty($ll))?$ll:''}}" data-dismiss="modal">{{trans('button.cl')}}</button>
                {{-- <button type="submit" class="btn btn-primary">Simpan</button> --}}
            </div>
        </div>
        {{Form::close()}}
    </div>
</div>
<script>
      // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -7.800868, lng: 110.373299},
          zoom: 13,
          mapTypeControl: false,
          streetViewControl: false,
          scrollwheel: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

            google.maps.event.addListener(map, "click", function(event) {
                placeMarker(event);
                // get lat/lon of click
                // var clickLat = event.latLng.lat();
                // var clickLon = event.latLng.lng();

                // // show in input box
                // // document.getElementById("lat").value = clickLat.toFixed(5);
                // // document.getElementById("lon").value = clickLon.toFixed(5);
                // $('.lat').val(clickLat.toFixed(5));
                // $('.lon').val(clickLon.toFixed(5));
                //   var marker = new google.maps.Marker({
                //         position: new google.maps.LatLng(clickLat,clickLon),
                //         map: map
                //      });

            });
        function setAllMap(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
            markers = [];
        }
        function placeMarker(location) {
            setAllMap(null);
            var clickLat = location.latLng.lat();
            var clickLon = location.latLng.lng();

            // show in input box
            // document.getElementById("lat").value = clickLat.toFixed(5);
            // document.getElementById("lon").value = clickLon.toFixed(5);
            $('.assign').attr('data-latlng',clickLat.toFixed(5)+','+clickLon.toFixed(5));
            // $('.lon').val(clickLon.toFixed(5));
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(clickLat,clickLon),
                map: map
            });
            markers.push(marker);
        
        }
        @if(count($latlng)>0)
            // markers = [];
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng({{$latlng[0]}},{{$latlng[1]}}),
                map: map
            });
            // markers.push(marker);
        @endif
        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

    </script>
    <script class="js-map" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8HshEMMYjzaBhabEOoWmW2-Ix2lN8T8k&libraries=places&callback=initAutocomplete" async defer></script>