@extends($template)

@section('end-script')
    @parent

    <script src="{{asset('js/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
    <script src="{{asset('js/jquery.price_format.2.0.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
        form : '#request',
        onSuccess : function() {
            waiting();
        }
    });
    $('.anggaran').priceFormat({
        prefix:'',
        thousandsSeparator: '.',
        centsLimit: 0
    });
function formats(){

    $(this).priceFormat({
        prefix:'',
        thousandsSeparator: '.',
        centsLimit: 0
    });
}
    $(document).on('click','.map-modal',function (e){
        var index  = $(this).data('index');
        var latlon = $('a[data-index="'+index+'"').attr('data-lalo');
        if(latlon){
            $('#open-modal').load('{{url("mapmodal")}}/'+index+'/'+latlon);
        }else{
            $('#open-modal').load('{{url("mapmodal")}}/'+index);
        }
    });

    $(document).on('click','.keep-map',function(){
        var dataindex = $(this).data('index');
        var latlong   = $(this).data('latlng');
        var map       = $('a[data-index="'+dataindex+'"').next().val(latlong);
        $('a[data-index="'+dataindex+'"').attr('data-lalo', latlong);
    });

    $(document).on('change','.tahun',function(){
        var CSRF_TOKEN = $('input[name="_token"]').attr('value');
        var tahun = $(this).val();
        $.ajax({
            type : 'POST',
            url  : '/skpd/usulan/tahun',
            data : {_token: CSRF_TOKEN, tahun : tahun},
            dataType: 'JSON',
            beforeSend : function() {
                $('.program').html("<option value=''>Loading...</option>");
            },
            success : function(data){
                $('.program').html(data);
            }
        }).done(function(){
            // $('.kecamatan').val($('.kecamatan').attr('data-selected'));
        });
    });

    $(document).on('change','.program',function(){
        var CSRF_TOKEN = $('input[name="_token"]').attr('value');
        var program = $(this).val();
        $.ajax({
            type : 'POST',
            url  : '/skpd/usulan/program',
            data : {_token: CSRF_TOKEN, id_program : program},
            dataType: 'JSON',
            beforeSend : function() {
                $('.kegiatan').html("<option value=''>Loading...</option>");
            },
            success : function(data){
                $('.kegiatan').html(data);
            }
        }).done(function(){
            // $('.kecamatan').val($('.kecamatan').attr('data-selected'));
        });
    });
    $(document).on('change','.kabupaten',function(){
        var CSRF_TOKEN     = $('input[name="_token"]').attr('value');
        var kabupaten      = $(this).val();
        var kabupatenClass = $(this);
        kabupatenClass.parents('.col-xs-4').next().next().find('.desa').html('<option value="">- Pilih Desa -</option>');
        $.ajax({
            type : 'POST',
            url  : '/kecamatan',
            data : {_token: CSRF_TOKEN, id_kabupaten : kabupaten},
            dataType: 'JSON',
            beforeSend : function() {
                kabupatenClass.parents('.col-xs-4').next().find('.kecamatan').html("<option value=''>Loading...</option>");
            },
            success : function(data){
                kabupatenClass.parents('.col-xs-4').next().find('.kecamatan').html(data);
            }
        }).done(function(){
            // $('.kecamatan').val($('.kecamatan').attr('data-selected'));
        });
    });

    $(document).on('change','.kecamatan',function(){
        var CSRF_TOKEN     = $('input[name="_token"]').attr('value');
        var kecamatan      = $(this).val();
        var kecamatanClass = $(this);
        // if(kecamatan==""){
        //     console.log('done');
        //     $(this).parents('.col-xs-3').next().find('.desa').html("<option value=''>- Pilih Desa -</option>");
        // }
        $.ajax({
            type : 'POST',
            url  : '/desa',
            data : {_token: CSRF_TOKEN, id_kecamatan : kecamatan},
            cache:false,
            beforeSend : function() {
                // $('.desa').html("<option value=''>Loading...</option>");
                kecamatanClass.parents('.col-xs-3').next().find('.desa').html("<option value=''>Loading...</option>");
            },
            success : function(data){
                // $('.desa').html(data);
                kecamatanClass.parents('.col-xs-3').next().find('.desa').html(data);
            }
        }).done(function(){
            // $('.kecamatan').val($('.kecamatan').attr('data-selected'));
        });
    });
    var max_fields      = 4; //maximum input boxes allowed
    var wrapper         = $(".add-dinamic"); //Fields wrapper
    var add_button      = $(".add-item"); //Add button ID
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="box box-solid"><div class="box-body"><a class="btn btn-danger pull-right btn-xs delete-item"><i class="fa fa-fw fa-trash-o"></i> Hapus</a><div class="row"><div class="col-xs-3"><div class="form-group"><label>Keluaran</label><input type="text" autocomplete="off" name="keluaran[]" class="form-control" data-validation="required" data-validation-error-msg="This field is required."></div></div><div class="col-xs-2"><div class="form-group"><label>Anggaran</label><input type="text" autocomplete="off" name="anggaran[]" class="form-control anggaran" data-validation="required" data-validation-error-msg="This field is required."></div></div><div class="col-xs-2"><div class="form-group"><label>Prioritas</label><select name="prioritas[]" class="form-control" data-validation="required" data-validation-error-msg="This field is required."><option value="">- Pilih Prioritas -</option>'+
                @foreach($prioritas as $pri)
                '<option value="{{$pri->id}}">{{$pri->name}}</option>'+
                @endforeach
                '</select></div></div><div class="col-xs-2"><div class="form-group"><label>TOR</label><input type="file" name="tor[]"></div></div><div class="col-xs-2"><div class="form-group"><label>RAB</label><input type="file" name="rab[]"></div></div></div><div class="row"><div class="col-xs-4"><div class="form-group"><label>Kabupaten</label><select name="kabupaten[]" class="form-control kabupaten" data-validation="required" data-validation-error-msg="This field is required."><option value="">- Pilih Kabupaten -</option>'+
                @foreach($kabupaten as $kab)
                '<option value="{{$kab->id}}">{{$kab->nama}}</option>'+
                @endforeach
                            '</select></div></div><div class="col-xs-3"><div class="form-group"><label>Kecamatan</label><select name="kecamatan[]" class="form-control kecamatan" data-validation="required" data-validation-error-msg="This field is required."><option value="">- Pilih Kecamatan -</option></select></div></div><div class="col-xs-3"><div class="form-group"><label>Desa</label><select name="desa[]" class="form-control desa" data-validation="required" data-validation-error-msg="This field is required."><option value="">- Pilih Desa -</option></select></div></div><div class="col-xs-2"><div class="form-group"><label>Maps</label><a data-href="{{url($uri)}}" class="form-control btn btn-default map-modal please-waiting" data-index="'+x+'"><i class="fa fa-fw fa-map-marker"></i> Maps</a><input type="hidden" class="latlng" name="latlng[]"></div></div></div></div></div>').on('keyup','.anggaran',formats); //add input box
        }
    });
    
    $(document).on("click",".delete-item", function(e){ //user click on remove text
        e.preventDefault(); $(this).parents('.add-dinamic div').remove(); x--;
    });
    </script>
@endsection

@section('body-content')
@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
<!-- left column -->
    <div class="col-md-12">
     <!-- general form elements -->
        {!! Form::open(array('url' => $uri.'/store','method'=>'POST','id'=>'request','files'=>true)) !!}
        <div class="box box-primary">
            <div class="box-header">
             {{-- <h3 class="box-title">Quick Example</h3> --}}
            </div><!-- /.box-header -->
       <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-2">
                            <div class="form-group">
                                <label>Tahun</label>
                                <select name="tahun" class="form-control tahun" data-validation="required" data-validation-error-msg="This field is required.">
                                    @foreach($yearRange as $row)
                                        <option value="{{$row}}">{{$row}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-5">
                            <div class="form-group">
                                <label>Program</label>
                                <select name="program" class="form-control program" data-validation="required" data-validation-error-msg="This field is required.">
                                    <option value="">- Pilih Program -</option>
                                    @foreach($program as $pro)
                                        <option value="{{$pro->id}}">{{$pro->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-5">
                            <div class="form-group">
                                <label>Kegiatan</label>
                                <select name="kegiatan" class="form-control kegiatan" data-validation="required" data-validation-error-msg="This field is required.">
                                    <option value="">- Pilih Kegiatan -</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="box-header">
            <h3 class="box-title pull-left"></h3>
            <div class="pull-right" style="margin-left:5px">
                <a class="btn btn-xs btn-primary add-item"><i class="fa fa-fw fa-plus"></i> Tambah</a>
            </div>
        </div><!-- /.box-header -->
        <div class="add-dinamic">
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label>Keluaran</label>
                            <input type="text" name="keluaran[]" class="form-control" data-validation="required" data-validation-error-msg="This field is required." autocomplete="off">
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label>Anggaran</label>
                            <input type="text" name="anggaran[]" class="form-control anggaran" autocomplete="off" data-validation="required" data-validation-error-msg="This field is required.">
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label>Prioritas</label>
                            <select name="prioritas[]" class="form-control" data-validation="required" data-validation-error-msg="This field is required.">
                                <option value="">- Pilih Prioritas -</option>
                                @foreach($prioritas as $val)
                                    <option value="{{$val->id}}">{{$val->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label>TOR</label>
                            <input type="file" name="tor[]">
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label>RAB</label>
                            <input type="file" name="rab[]">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Kabupaten</label>
                            <select name="kabupaten[]" class="form-control kabupaten" data-validation="required" data-validation-error-msg="This field is required.">
                                <option value="">- Pilih Kabupaten -</option>
                                @foreach($kabupaten as $kab)
                                    <option value="{{$kab->id}}">{{$kab->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label>Kecamatan</label>
                            <select name="kecamatan[]" class="form-control kecamatan" data-validation="required" data-validation-error-msg="This field is required.">
                                <option value="">- Pilih Kecamatan -</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label>Desa</label>
                            <select name="desa[]" class="form-control desa" data-validation="required" data-validation-error-msg="This field is required.">
                                <option value="">- Pilih Desa -</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label>Maps</label>
                            <a class="form-control btn btn-default map-modal please-waiting" data-index="0" data-lalo><i class="fa fa-fw fa-map-marker"></i> Maps</a>
                            <input type="hidden" class="latlng" name="latlng[]">
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        </div>
        <div class="box-footer">
            <a href="{{url($uri)}}" class="btn btn-warning">Batal</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<div id="open-modal"></div>
@endsection
