<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Usulan Masyarakat</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Bad+Script' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="{{asset('bootstrap-3.3.6-dist/css/bootstrap.css')}}" rel="stylesheet">
    {{-- <link rel="stylesheet" href="{{asset('css/flexslider.css')}}" type="text/css" media="screen" /> --}}
    <link href="{{asset('bootstrap-3.3.6-dist/css/style.css')}}" rel="stylesheet">
    <style type="text/css">
   	#map {
        height: 400px;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <header id="usulan">
      <div class="form-box">
        <div class="row home-intro">
          <div class="col-lg-12">
            <h1 class="section-heading logo-name">DanaIsBud DIY Plan</h1>
            <div class="form-danais usulan-masyarakat">
           	<h1 class="text-center">Usulan Masyarakat</h1>
            {!! Form::open(array('url' => url('usulanMasyarakat'),'method'=>'POST','id'=>'request')) !!}
              	<div class="row">
	              	<div class="col-lg-12">
	                	<div class="panel panel-default">
							<div class="panel-heading text-center">Biodata</div>
							<div class="panel-body">
				                <div class="form-group">
				                  	<label>Nama Pengusul</label>
				                    <input type="text" class="form-control" name="nama_pengusul" data-validation="required" data-validation-error-msg="This field is required.">
				                </div>
				                <div class="form-group">
				                  	<label>Email</label>
				                    <input type="email" class="form-control" name="email" data-validation="required|email" data-validation-error-msg="This field is required.">
				                </div>
				                <div class="form-group">
				                  	<label>Nomor Telfon</label>
				                    <input class="form-control" name="nomor_telfon" data-validation="required" data-validation-error-msg="This field is required.">
				                </div>
				                <div class="form-group">
				                  	<label>Alamat Pengusul</label>
				                    <textarea class="form-control" rows="5" name="alamat_pengusul" data-validation="required" data-validation-error-msg="This field is required."></textarea>
				                </div>
{{-- 				                <div class="form-group">
				                  	<label>Ringkasan Permasalah</label>
				                    <textarea class="form-control" rows="5" name="ringkasan_permasalahan" data-validation="required" data-validation-error-msg="This field is required."></textarea>
				                </div> --}}
							</div>
						</div>
	                	<div class="panel panel-default">
						  	<div class="panel-heading text-center">Usulan Kegiatan</div>
						  	<div class="panel-body">
			                  	<div class="form-group">
			                  	<label>SKPD</label>
			                    	<select class="form-control" name="skpd">
			                    		<option value="">- Pilih SKPD -</option>
			                    		@foreach($skpd as $sk)
			                    			<option value="{{$sk->id}}">{{$sk->instansi_name}}</option>
			                    		@endforeach
			                    	</select>
			                  	</div>
			                  	<div class="form-group">
			                  		<label>Kabupaten</label>
			                    	<select class="form-control kabupaten" name="kabupaten" data-validation="required" data-validation-error-msg="This field is required.">
			                    		<option value="">- Pilih Kabupaten -</option>
			                    		@foreach($kabupaten as $kab)
			                    			<option value="{{$kab->id}}">{{$kab->nama}}</option>
			                    		@endforeach
			                    	</select>
			                  	</div>
			                  	<div class="form-group">
			                  		<label>Kecamatan</label>
			                    	<select class="form-control kecamatan" name="kecamatan" data-validation="required" data-validation-error-msg="This field is required.">
			                    		<option value="">- Pilih Kecamatan -</option>
			                    	</select>
			                  	</div>
			                  	<div class="form-group">
				                  	<label>Desa</label>
			                    	<select class="form-control desa" name="desa" data-validation="required" data-validation-error-msg="This field is required.">
			                    		<option value="">- Pilih Desa -</option>
			                    	</select>
			                  	</div>
			                  	<div class="form-group">
			                  		<label>Lokasi</label>
	                  		        <input id="pac-input" class="controls" type="text" placeholder="Cari Jalan">
								                <div id="map"></div>
			                  	</div>
				              	<div class="row">
		             				<div class="col-lg-6">
					                  	<div class="form-group">
					                  		<label>Latitude</label>
					                  		<input type="hidden" name="lat" class="form-control lat">
					                  		<input type="text" disabled="" class="form-control lat">
					                  	</div>
				                  	</div>
		             				<div class="col-lg-6">
					                  	<div class="form-group">
					                  		<label>Longitude</label>
					                  		<input type="hidden" name="lon" class="form-control lon">
					                  		<input type="text" disabled="" class="form-control lon">
					                  	</div>
				                  	</div>
			                  	</div>
			                  	<div class="form-group">
			                  	<label>Usulan</label>
			                    	<textarea class="form-control" rows="5" name="usulan" data-validation="required" data-validation-error-msg="This field is required."></textarea>
			                  	</div>
						  	</div>
						</div>
	             	</div>
              	</div>
              	<div class="row">
	             	<div class="col-lg-6">
               		<button type="submit" class="btn btn-primary btn-lg btn-block btn-learnmore">Submit</button>
	             	</div>
	             	<div class="col-lg-6">
               		<input type="reset" class="btn btn-warning btn-lg btn-block btn-learnmore" value="Reset">
	             	</div>
              	</div>
              	{{Form::close()}}
            </div>
          </div>
        </div>

      </div><!-- /.container -->
    </header>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{asset('bootstrap-3.3.6-dist/js/jquery.min.js')}}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{asset('bootstrap-3.3.6-dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.flexslider.js')}}"></script>
    <!--<script>document.body.className += ' fade-out';</script>-->

    <script src="{{asset('js/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
  var markerData= [
    {lat: 36.4 , lng: -120.9  , zoom: 7 , name: "California"},
    {lat: 28 , lng: -81  , zoom: 7 , name: "Florida"},
    {lat: 40.39 , lng: -3.67  , zoom: 8 , name: "Madrid"},
  ];
     $.validate({
        form : '#request',
        onSuccess : function() {
		    $.blockUI({ 
		        message: '<h1 style="font-weight:300">Please Wait</h1>',
		        css: { 
		            border: 'none', 
		            padding: '15px', 
		            backgroundColor: '#fff', 
		            '-webkit-border-radius': '0', 
		            '-moz-border-radius': '0',  
		            color: '#000',
		            'text-transform': 'uppercase'
		        },
		        baseZ:9999
		    });
        }
    });
    $('#request').on("keyup keypress", function(e) {
      var code = e.keyCode || e.which; 
      if (code  == 13) {               
        e.preventDefault();
        return false;
      }
    });
    $(document).on('change','.kabupaten',function(){
    	var CSRF_TOKEN = $('input[name="_token"]').attr('value');
        var kabupaten = $(this).val();
        $('.desa').html('<option>- Pilih Desa -</option>');
        $.ajax({
            type : 'POST',
            url  : '/kecamatan',
            data : {_token: CSRF_TOKEN, id_kabupaten : kabupaten},
            dataType: 'JSON',
            beforeSend : function() {
                $('.kecamatan').html("<option value=''>Loading...</option>");
            },
            success : function(data){
                $('.kecamatan').html(data);
                // console.log(data);
            }
        }).done(function(){
            // $('.kecamatan').val($('.kecamatan').attr('data-selected'));
        });
    });

    $(document).on('change','.kecamatan',function(){
    	var CSRF_TOKEN = $('input[name="_token"]').attr('value');
        var kecamatan = $(this).val();
        $.ajax({
            type : 'POST',
            url  : '/desa',
            data : {_token: CSRF_TOKEN, id_kecamatan : kecamatan},
            dataType: 'JSON',
            beforeSend : function() {
              $('.desa').html("<option value=''>Loading...</option>");
            },
            success : function(data){
                $('.desa').html(data);
                // console.log(data);
            }
        }).done(function(){
            // $('.kecamatan').val($('.kecamatan').attr('data-selected'));
        });
    });
 
    </script>

    <script>

   //    function initAutocomplete() {
   //      var map = new google.maps.Map(document.getElementById('map'), {
   //        center: {lat: -7.800868, lng: 110.373299},
   //        zoom: 13,
   //        mapTypeControl: false,
   //        streetViewControl: false,
   //        scrollwheel: false,
   //        mapTypeId: google.maps.MapTypeId.ROADMAP
   //      });

			// google.maps.event.addListener(map, "click", function(event) {
   //              placeMarker(event);
   //          });
   //      function setAllMap(map) {
   //          for (var i = 0; i < markers.length; i++) {
   //              markers[i].setMap(map);
   //          }
   //          markers = [];
   //      }
   //      function placeMarker(location) {
   //          setAllMap(null);
   //         	var clickLat = location.latLng.lat();
   //          var clickLon = location.latLng.lng();
            
   //          $('.lat').val(clickLat.toFixed(5));
   //          $('.lon').val(clickLon.toFixed(5));
   //        	var marker = new google.maps.Marker({
   //              position: new google.maps.LatLng(clickLat,clickLon),
   //              map: map
   //          });
   //          markers.push(marker);
        
   //      }
   //      // Create the search box and link it to the UI element.
   //      var input = document.getElementById('pac-input');
   //      var searchBox = new google.maps.places.SearchBox(input);
   //      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

   //      // Bias the SearchBox results towards current map's viewport.
   //      map.addListener('bounds_changed', function() {
   //        searchBox.setBounds(map.getBounds());
   //      });

   //      var markers = [];
   //      // Listen for the event fired when the user selects a prediction and retrieve
   //      // more details for that place.
   //      searchBox.addListener('places_changed', function() {
   //        var places = searchBox.getPlaces();

   //        if (places.length == 0) {
   //          return;
   //        }

   //        // Clear out the old markers.
   //        markers.forEach(function(marker) {
   //          marker.setMap(null);
   //        });
   //        markers = [];

   //        // For each place, get the icon, name and location.
   //        var bounds = new google.maps.LatLngBounds();
   //        places.forEach(function(place) {
   //          var icon = {
   //            url: place.icon,
   //            size: new google.maps.Size(71, 71),
   //            origin: new google.maps.Point(0, 0),
   //            anchor: new google.maps.Point(17, 34),
   //            scaledSize: new google.maps.Size(25, 25)
   //          };

   //          // Create a marker for each place.
   //          markers.push(new google.maps.Marker({
   //            map: map,
   //            icon: icon,
   //            title: place.name,
   //            position: place.geometry.location
   //          }));

   //          if (place.geometry.viewport) {
   //            // Only geocodes have viewport.
   //            bounds.union(place.geometry.viewport);
   //          } else {
   //            bounds.extend(place.geometry.location);
   //          }
   //        });
   //        map.fitBounds(bounds);
   //      });
   //    }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8HshEMMYjzaBhabEOoWmW2-Ix2lN8T8k&libraries=places&callback=initAutocomplete" async defer></script>
  </body>
</html>