<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>DanaIsBud DIY Plan</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Bad+Script' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="{{asset('bootstrap-3.3.6-dist/css/bootstrap.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/flexslider.css')}}" type="text/css" media="screen" />
    <link href="{{asset('bootstrap-3.3.6-dist/css/style.css')}}" rel="stylesheet">

    <link href="{{asset('css/animate.css')}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
{{--     <nav data-scroll-header class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand logo-name" href="#">Danais Plan</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a class="smooth-scroll" href="#home">home</a></li>
            <li><a class="smooth-scroll" href="#about">about</a></li>
            <li><a class="smooth-scroll" href="#features">features</a></li>
            <li><a class="smooth-scroll" href="#video">video</a></li>
            <li><a class="smooth-scroll" href="#pricing">pricing</a></li>
            <li><a class="smooth-scroll" href="#team">team</a></li>
            <li><a class="smooth-scroll" href="#portfolio">portfolio</a></li>
            <li><a class="smooth-scroll" href="#clients">clients</a></li>
            <li><a class="smooth-scroll" href="#contact">contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav> --}}
    <header id="home">
      <div class="container">
        <div class="row home-intro">
          <div class="col-lg-7">
            <h1 class="section-heading logo-name">DanaIsBud DIY Plan</h1>
            <div class="flexslider">
              <ul class="slides">
                <li><p class="lead">To practice patience, an enemy is the best teacher</p></li>
                <li><p class="lead">My pride fell with my fortunes</p></li>
                <li><p class="lead">Always do your best, and let God do next</p></li>
                <li><p class="lead">When you lost your attitude you lost everything</p></li>
                <li><p class="lead">The empty tube makes the loudest sound</p></li>
              </ul>
            </div>

          </div>
          <div class="col-lg-5">
            <div class="form-danais">
              <h1>Login Area</h1>
              {!! Form::open(array('url' => 'skpdAuth','method'=>'POST','id'=>'request')) !!}
                <div class="form-group">
                  <input type="text" name="email" class="form-control" autocomplete="off" data-validation="required" data-validation-error-msg="This field is required." placeholder="Username">
                </div>
                <div class="form-group">
                  <input type="password" name="password" class="form-control" autocomplete="off" data-validation="required" data-validation-error-msg="This field is required." placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block btn-learnmore">Login</button>
                <a href="{{url('usulanMasyarakat')}}" class="btn btn-primary btn-lg btn-block btn-usulan btn-learnmore">Form Usulan Masyarakat</a>
              {!! Form::close() !!}
            </div>
            {{-- <a href="#about" class="btn btn-primary btn-lg btn-learnmore smooth-scroll">Learn More</a>
            <a href="#about" class="btn btn-primary btn-lg btn-learnmore smooth-scroll">Buy Template</a> --}}
          </div>
        </div>

      </div><!-- /.container -->
    </header>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{asset('bootstrap-3.3.6-dist/js/jquery.min.js')}}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{asset('bootstrap-3.3.6-dist/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.flexslider.js')}}"></script>
    <!--<script>document.body.className += ' fade-out';</script>-->
    {{-- // <script src="{{asset('js/main.js')}}"></script> --}}
    <script type="text/javascript" src="{{asset('js/jquery.noty.packaged.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/relax.js')}}"></script>
    <script src="{{asset('js/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">

     $.validate({
        form : '#request',
        onSuccess : function() {
        $.blockUI({ 
            message: '<h1 style="font-weight:300">Please Wait</h1>',
            css: { 
                border: 'none', 
                padding: '15px', 
                backgroundColor: '#fff', 
                '-webkit-border-radius': '0', 
                '-moz-border-radius': '0',  
                color: '#000',
                'text-transform': 'uppercase'
            },
            baseZ:9999
        });
        }
    });
    @if(session()->has('success'))
    var n = noty({
        layout:'topLeft',
        theme:'relax',
        text: '{{session()->get("success")}}',
        animation: {
            open  : 'animated fadeIn',
            close : 'animated fadeOut',
            easing: 'swing', // easing
            speed: 500 // opening & closing animation speed
        },
        type: 'success'
    });
    n.setTimeout(5000);
    @endif

    @if(session()->has('error_login'))
    var n = noty({
        layout:'topLeft',
        theme:'relax',
        text: '{{session()->get("error_login")}}',
        animation: {
            open  : 'animated fadeIn',
            close : 'animated fadeOut',
            easing: 'swing', // easing
            speed: 500 // opening & closing animation speed
        },
        type: 'warning'
    });
    n.setTimeout(5000);
    @endif
    </script>
  </body>
</html>