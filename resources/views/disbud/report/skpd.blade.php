@extends($template)

@section('header-content')

<div class="pull-right" style="margin-left:5px">
    <a href="{{url($uri.'/create')}}" class="btn btn-primary"> <i class="fa fa-plus"></i> Tambah</a>
</div>

@endsection

@section('body-content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr class="text-uppercase">
            <th>#</th>
            <th>Nama Instansi</th>
            <th>Anggaran</th>
            <th>Kecamatan</th>
            <th>Desa</th>
            <th>Status</th>
            <th>Date</th>
          </tr>
         <?php 
         if($pages){
             $page = $pages;
         }else{
             $page = 1;
         }
         $nomor = $page + ($page-1) * ($limit-1);
         ?>
          @foreach($usulanMap as $row)
          <tr>
            <td>{{$nomor++}}</td>
            <td>{{$row['name']}}</td>
            <td>Rp {{$row['anggaran']}}</td>
            <td>{{$row['kecamatan']}}</td>
            <td>{{$row['desa']}}</td>
            <td>
               @if($row['status']==1)
                  <span class="label label-success">Approved</span>
               @elseif($row['status']==2)
                  <span class="label label-info">Revision</span>
               @elseif($row['status']==3)
                  <span class="label label-danger">Denied</span>
               @elseif($row['status']==4)
                  <span class="label label-warning">Improve Revision</span>
               @else
                  <span class="label label-warning">New</span>
               @endif
            </td>
            <td>{{date('d F Y',strtotime($row['created_at']))}}</td>
          </tr>
          @endforeach
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
      </div>
    </div><!-- /.box -->
</div>
@endsection
