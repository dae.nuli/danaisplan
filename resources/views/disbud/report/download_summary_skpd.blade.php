<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
        thead tr td{
            text-align: center;
        }
        .success{
            color:#00a65a;
        }
        .danger{
            color:#dd4b39;
        }
        .info{
            color:#00c0ef;
        }
        .warning{
            color:#f0ad4e;
        }
        </style>
    </head>
    <table border="1">
        <thead>
        <tr>
            <td><b>NO</b></td>
            <td><b>SKPD</b></td>
            <td><b>KODE</b></td>
            <td><b>PROGRAM</b></td>
            <td><b>KEGIATAN</b></td>
            <td><b>ANGGARAN</b></td>
            <td><b>STATUS</b></td>
            <td><b>CREATED AT</b></td>
        </tr>
        </thead>
        <tbody>
            <?php $nomor = 1; ?>
            @foreach($index as $i => $row)
            <?php $anggaran[] = Helper::anggaranCount($row->id); ?>
            <tr>
                <td>{{$nomor++}}</td>
                <td>{{$row->skpd->instansi_name}}</td>
                <td>{!!$row->program->code_program!!}</td>
                <td>{{$row->program->name}}</td>
                <td>{{$row->kegiatan->name}}</td>
                <td>{{Helper::anggaranCount($row->id)}}</td>
                <td>
                   @if($row->is_approved==1)
                      <span class="success">APPROVED</span>
                   @elseif($row->is_approved==2)
                      <span class="info">REVISION</span>
                   @elseif($row->is_approved==3)
                      <span class="danger">DENIED</span>
                   @elseif($row->is_approved==4)
                      <span class="warning">IMPROVE REVISION</span>
                   @else
                      <span class="warning">NEW</span>
                   @endif
                </td>
                <td>{{date('d F Y',strtotime($row->created_at))}}</td>
            </tr>
          @endforeach
          <tr></tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>Total</b></td>
            <td><b>{{array_sum($anggaran)}}</b></td>
            <td></td>
            <td></td>
          </tr>
        </tbody>
    </table>
</html>