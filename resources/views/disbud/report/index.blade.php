@extends($template)

@section('end-script')
    @parent
    <link href="{{asset('css/select2.css')}}" rel="stylesheet" />
    <script src="{{asset('js/select2.js')}}"></script>
    <script type="text/javascript">
    $(function() {
        $('.filter').select2({
            theme: "classic"
        });
    });
    </script>
@endsection

@section('header-content')

<div class="pull-right" style="margin-left:5px">
    <a href="{{url($uri.'/summary')}}" class="btn btn-primary"> <i class="fa fa-download"></i> Download Summary</a>
    <a href="{{url($uri.'/full')}}" class="btn btn-success"> <i class="fa fa-download"></i> Download Full </a>
</div>

@endsection

@section('body-content')
<div class="box box-solid">
<!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-xs-2">
                {{Form::open(array('url' => $action, 'method' => 'post'))}}
                <select class="form-control filter" name="year" style="padding:0;" onchange="this.form.submit()">
                    @foreach($tahun as $row)
                        <option value="{{$row}}" {{($year==$row) ? 'selected' : ''}}>{{$row}}</option>
                    @endforeach
                </select>
                {{Form::close()}}
            </div>
            <div class="col-xs-4">
                {{Form::open(array('url' => $actionProgram, 'method' => 'post'))}}
                <select class="form-control filter" name="program" style="padding:0;" onchange="this.form.submit()">
                      <option value="">- Pilih Program -</option>
                    @foreach($program as $pro)
                        <option value="{{$pro->id}}" {{($programSelected==$pro->id) ? 'selected' : ''}}>{{$pro->name}}</option>
                    @endforeach
                </select>
                {{Form::close()}}
            </div>
            <div class="col-xs-3">
                {{Form::open(array('url' => $actionKegiatan, 'method' => 'post'))}}
                <select class="form-control filter" name="kegiatan" style="padding:0;" onchange="this.form.submit()">
                    @if(isset($kegiatan))
                      <option value="">- Pilih Kegiatan -</option>
                      @foreach($kegiatan as $keg)
                          <option value="{{$keg->id}}" {{($kegiatanSelected==$keg->id) ? 'selected' : ''}}>{{$keg->name}}</option>
                      @endforeach
                    @else
                      <option value="">- Pilih Kegiatan -</option>
                    @endif
                </select>
                {{Form::close()}}
            </div>
            <div class="col-xs-3">
                {{Form::open(array('url' => $actionSkpd, 'method' => 'post'))}}
                <select class="form-control filter" name="skpd" style="padding:0;" onchange="this.form.submit()">
                  <option value="">- Pilih SKPD -</option>
                  @foreach($skpd as $val)
            <option value="{{$val->id}}" {{($skpdSelected==$val->id) ? 'selected' : ''}}>{{$val->instansi_name}}</option>
                  @endforeach
                </select>
                {{Form::close()}}
            </div>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr class="text-uppercase">
            <th>#</th>
            <th>SKPD</th>
            <th>Program</th>
            <th>Kegiatan</th>
            {{-- <th>Tahun</th> --}}
            <th>Status</th>
            <th>Date</th>
            <th></th>
          </tr>
         <?php 
         if($pages){
             $page = $pages;
         }else{
             $page = 1;
         }
         $nomor = $page + ($page-1) * ($limit-1);
         ?>
          @foreach($index as $row)
          <tr>
            <td>{{$nomor++}}</td>
            <td>{!!Str::words($row->skpd->instansi_name,3,' ...')!!}</td>
            <td>{!!Str::words($row->program->name,5,' ...')!!}</td>
            <td>{!!Str::words($row->kegiatan->name,5,' ...')!!}</td>
            {{-- <td>{{$row->years}}</td> --}}
            <td>
               @if($row->is_approved==1)
                  <span class="label label-success">Approved</span>
               @elseif($row->is_approved==2)
                  <span class="label label-info">Revision</span>
               @elseif($row->is_approved==3)
                  <span class="label label-danger">Denied</span>
               @elseif($row->is_approved==4)
                  <span class="label label-warning">Improve Revision</span>
               @else
                  <span class="label label-warning">New</span>
               @endif
            </td>
            <td>{{date('d F Y',strtotime($row->created_at))}}</td>
            <td>
                <a href="{{url($uri.'/detail/'.$row->id)}}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-info"></i> {{trans('button.det')}}</a>
            </td>
          </tr>
          @endforeach
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
         {!! $index->links() !!}
      </div>
    </div><!-- /.box -->
</div>
@endsection
