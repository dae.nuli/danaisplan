<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
        thead tr td{
            text-align: center;
        }
        .success{
            color:#00a65a;
        }
        .danger{
            color:#dd4b39;
        }
        .info{
            color:#00c0ef;
        }
        .warning{
            color:#f0ad4e;
        }
        </style>        
    </head>
    <table border="1">
        <thead>
        <tr>
            <td width="5"><b>NO</b></td>
            <td width="10"><b>TAHUN</b></td>
            <td><b>SKPD</b></td>
            <td width="10"><b>KODE</b></td>
            <td><b>PROGRAM</b></td>
            <td><b>KEGIATAN</b></td>
            <td><b>KELUARAN</b></td>
            <td></td>
            <td></td>
            <td width="15"><b>ANGGARAN</b></td>
            <td><b>LOKASI</b></td>
            <td></td>
            <td></td>
            <td width="10">STATUS</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>TOLAK UKUR KINERJA</b></td>
            <td><b>TARGET KINERJA</b></td>
            <td></td>
            <td></td>
            <td><b>KABUPATEN</b></td>
            <td><b>KECAMATAN</b></td>
            <td><b>DESA</b></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>JUMLAH</b></td>
            <td><b>SATUAN</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </thead>
        <tbody>
            <?php $nomor = 1; ?>
            @foreach($index as $j=> $row)
{{--             <tr>
            </tr> --}}
                @foreach(Helper::detailSKPD($row->id) as $i => $val)
                <?php $anggaranSKPD[]   = $val->anggaran; ?>
                <?php $anggaran[$j][$i] = $val->anggaran; ?>
                <tr>
                        @if($i==0)
                            <td>{{$nomor++}}</td>
                            <td>{{$row->program->years}}</td>
                            <td>{{$row->skpd->instansi_name}}</td>
                            <td>{{$row->program->code_program}}</td>
                            <td>{{$row->program->name}}</td>
                            <td>{{$row->kegiatan->name}}</td>
                        @else
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        @endif
                    <td>{{$val->tolak_ukur_kinerja}}</td>
                    <td>{{$val->jumlah_target_kinerja}}</td>
                    <td>{{$val->satuan_target_kinerja}}</td>
                    <td>{{$val->anggaran}}</td>
                    <td>{{$val->kabupaten->nama}}</td>
                    <td>{{$val->kecamatan->nama}}</td>
                    <td>{{$val->desa->nama}}</td>
                    <td>
                       @if($row->is_approved==1)
                          <span class="success">APPROVED</span>
                       @elseif($row->is_approved==2)
                          <span class="info">REVISION</span>
                       @elseif($row->is_approved==3)
                          <span class="danger">DENIED</span>
                       @elseif($row->is_approved==4)
                          <span class="warning">IMPROVE REVISION</span>
                       @else
                          <span class="warning">NEW</span>
                       @endif
                    </td>
                </tr>
                @endforeach
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>Total anggaran perkeluaran</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>{{array_sum($anggaran[$j])}}</b></td>
                </tr>
                <tr></tr>
          @endforeach
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>Total anggaran per SKPD</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>{{array_sum($anggaranSKPD)}}</b></td>
          </tr>
        </tbody>
    </table>
</html>