<script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.datetimepicker.css')}}"/ >
<script src="{{asset('js/jquery.datetimepicker.js')}}"></script>
<script type="text/javascript">
$.validate({
    form : '#requests',
    onSuccess : function() {
        waiting();
    }
});
jQuery('.datetimepicker2').datetimepicker({
    closeOnDateSelect:true,
    format:'Y-m-d',
    minDate:0,
    scrollInput:false
});
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        {!!Form::open(array('url'=>$action, 'method'=>'POST','id'=>'requests'))!!}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Masa Login</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Tanggal Mulai</label>
                                <input type="text" name="start" autocomplete="off" value="{{isset($index)?$index->start_date:''}}" class="form-control datetimepicker2" data-validation="required" data-validation-error-msg="This field is required.">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Tanggal Berakhir</label>
                                <input type="text" name="end" autocomplete="off" value="{{isset($index)?$index->end_date:''}}" class="form-control datetimepicker2" data-validation="required" data-validation-error-msg="This field is required.">
                            </div>
                        </div>
{{--                         <div class="col-xs-4">
                            <div class="form-group">
                                <label>Status</label>
                                @if(isset($index))
                                <select name="status" class="form-control">
                                    <option value="1" {{($index->status==1)?'selected':''}}>Aktif</option>
                                    <option value="0" {{($index->status==0)?'selected':''}}>Tidak Aktif</option>
                                </select>
                                @else
                                <select name="status" class="form-control">
                                    <option value="1">Aktif</option>
                                    <option value="0">Tidak Aktif</option>
                                </select>
                                @endif
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default close-waiting" data-dismiss="modal">{{trans('button.cl')}}</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        {!!Form::close()!!}
    </div>
</div>