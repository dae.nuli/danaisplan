@extends($template)

@section('header-content')

<div class="pull-right" style="margin-left:5px">
    <a data-url="{{url($uri.'/masa-login')}}" class="btn btn-info masa-login"> <i class="fa fa-clock-o"></i> Masa Login</a>
    <a href="{{url($uri.'/create')}}" class="btn btn-primary"> <i class="fa fa-plus"></i> Tambah</a>
</div>

@endsection

@section('body-content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      {{-- <div class="box-header">
        <h3 class="box-title">Responsive Hover Table</h3>
        <div class="box-tools">
          <div class="input-group">
            <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
            <div class="input-group-btn">
              <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div> --}}<!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr class="text-uppercase">
            <th>#</th>
            <th>Kode SKPD</th>
            <th>Nama instansi</th>
            <th>email</th>
            <th>Status</th>
            <th>Date</th>
            <th></th>
          </tr>
         <?php 
         if($pages){
             $page = $pages;
         }else{
             $page = 1;
         }
         $nomor = $page + ($page-1) * ($limit-1);
         ?>
          @foreach($index as $row)
          <tr>
            <td>{{$nomor++}}</td>
            <td>{{$row->code}}</td>
            <td><a href="{{url($uri.'/show/'.$row->id)}}">{{$row->instansi_name}}</a></td>
            <td>{{$row->email}}</td>
            <td>{{date('d F Y',strtotime($row->created_at))}}</td>
            <td>{!!($row->is_active)?'<span class="label label-success">Aktif</span>':'<span class="label label-danger">Tidak Aktif</span>'!!}</td>
            <td>
                @if($row->is_active)
                        <a href="{{url($uri.'/disable/'.$row->id)}}" class="btn btn-danger btn-xs confirm-href"><i class="fa fa-fw fa-ban"></i> Non Aktifkan</a>
                @else
                    @if(empty($row->password))
                        <a href="{{url($uri.'/first-activation/'.$row->id)}}" class="btn btn-success btn-xs confirm-href"><i class="fa fa-fw  fa-envelope"></i> Kirim Akun</a>
                    @else
                        <a href="{{url($uri.'/enable/'.$row->id)}}" class="btn btn-success btn-xs confirm-href"><i class="fa fa-fw  fa-check-circle"></i> Aktifkan</a>
                    @endif
                @endif

                <a href="{{url($uri.'/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> Edit</a>
                
{{--                 {!! Form::open(array('url' => $uri.'/destroy/'.$row->id,'method'=>'POST','class'=>'in-blo')) !!}
                <button type="submit" class="btn btn-danger btn-xs confirm">
                    <i class="fa fa-fw fa-trash-o"></i> {{trans('button.del')}}
                </button>
                {!! Form::close() !!}
 --}}
                <a href="{{url($uri.'/destroy/'.$row->id)}}" class="btn btn-danger btn-xs confirm-href"><i class="fa fa-fw fa-trash-o"></i> Delete</a>
                @if($row->is_active)
                <a href="{{url($uri.'/reset/'.$row->id)}}" class="btn btn-warning btn-xs confirm-href"><i class="fa fa-fw fa-refresh"></i> Reset</a>
                @else
                <a disabled class="btn btn-warning btn-xs confirm-href"><i class="fa fa-fw fa-refresh"></i> Reset</a>
                @endif
            </td>
          </tr>
          @endforeach
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
         {!! $index->links() !!}
      </div>
    </div><!-- /.box -->
</div>
<div id="open-modal"></div>
@endsection

@section('end-script')
  @parent
<script src="{{asset('js/jquery.blockUI.js')}}"></script>
<script src="{{asset('js/confirm-bootstrap.js')}}"></script>
<script src="{{asset('js/confirm.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script type="text/javascript">
@if(session()->has('success'))
var n = noty({
    layout:'topRight',
    theme:'relax',
    text: '{{session()->get("success")}}',
    animation: {
        open  : 'animated fadeIn',
        close : 'animated fadeOut',
        easing: 'swing', // easing
        speed: 500 // opening & closing animation speed
    },
    type: 'success'
});
n.setTimeout(3000);
@endif

@if(session()->has('warning'))
var n = noty({
    layout:'topRight',
    theme:'relax',
    text: '{{session()->get("warning")}}',
    animation: {
        open  : 'animated fadeIn',
        close : 'animated fadeOut',
        easing: 'swing', // easing
        speed: 500 // opening & closing animation speed
    },
    type: 'warning'
});
n.setTimeout(3000);
@endif
</script>
@endsection