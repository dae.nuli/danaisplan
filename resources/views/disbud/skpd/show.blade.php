@extends($template)
 
@section('body-content')

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Kode SKPD</label>
                            <input readonly="" value="{{$show->code}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Nama Instansi</label>
                            <input readonly="" value="{{$show->instansi_name}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Email</label>
                            <input readonly="" value="{{$show->email}}" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Nama Pananggung Jawab</label>
                            <input readonly="" value="{{$show->personil_name}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Username</label>
                            <input value="{{$show->username}}" readonly="" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Nomor Telefon</label>
                            <input readonly="" value="{{$show->phone}}" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Email Staff</label>
                            <input readonly="" value="{{$show->email_staff}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-8">
                        <div class="form-group">
                            <label>Address</label>
                            <input readonly="" value="{{$show->address}}" class="form-control">
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="box-footer">
            <a href="{{url($uri)}}" class="btn btn-warning">{{trans('button.bac')}}</a>
        </div>
    </div>
</div>
@endsection
