@extends($template)

@section('end-script')
    @parent

    <script src="{{asset('js/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
        form : '#request',
        onSuccess : function() {
            waiting();
        }
    });
    </script>
@endsection

@section('body-content')
@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
<!-- left column -->
    <div class="col-md-12">
     <!-- general form elements -->
        <div class="box box-primary">
            {!! Form::open(array('url' => $action,'method'=>'POST','id'=>'request')) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" autocomplete="off" value="{{isset($index)?$index->name:''}}" class="form-control" data-validation="required" data-validation-error-msg="This field is required.">
                            </div>
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea name="note" class="form-control">{{isset($index)?$index->note:''}}</textarea>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <a href="{{url($uri)}}" class="btn btn-warning">{{trans('button.bac')}}</a>
                    <button type="submit" class="btn btn-primary">{{trans('button.sub')}}</button>
                </div>
            {!! Form::close() !!}
        </div><!-- /.box -->
    </div>
</div>
@endsection
