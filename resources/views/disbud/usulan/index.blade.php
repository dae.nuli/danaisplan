@extends($template)

@section('header-content')

@endsection

@section('body-content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      {{-- <div class="box-header">
        <h3 class="box-title">Responsive Hover Table</h3>
        <div class="box-tools">
          <div class="input-group">
            <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
            <div class="input-group-btn">
              <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div> --}}<!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr class="text-uppercase">
            <th>#</th>
            <th>Program</th>
            <th>Kegiatan</th>
            <th>Tahun</th>
            <th>Status</th>
            <th>Date</th>
            <th></th>
          </tr>
         <?php 
         if($pages){
             $page = $pages;
         }else{
             $page = 1;
         }
         $nomor = $page + ($page-1) * ($limit-1);
         ?>
          @foreach($index as $row)
          <tr>
            <td>{{$nomor++}}</td>
            <td>{!!Str::words($row->program->name,5,' ...')!!}</td>
            <td>{!!Str::words($row->kegiatan->name,5,' ...')!!}</td>
            <td>{{$row->years}}</td>
            <td>
               @if($row->is_approved==1)
                  <span class="label label-success">Approved</span>
               @elseif($row->is_approved==2)
                  <span class="label label-info">Revision</span>
               @elseif($row->is_approved==3)
                  <span class="label label-danger">Denied</span>
               @elseif($row->is_approved==4)
                  <span class="label label-warning">Improve Revision</span>
               @else
                  <span class="label label-warning">New</span>
               @endif
            </td>
            <td>{{date('d F Y',strtotime($row->created_at))}}</td>
            <td>
                <a href="{{url($uri.'/detail/'.$row->id)}}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-info"></i> {{trans('button.det')}}</a>
                {{-- <a href="{{url($uri.'/delete/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> {{trans('button.del')}}</a> --}}
            </td>
          </tr>
          @endforeach
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
         {!! $index->links() !!}
      </div>
    </div><!-- /.box -->
</div>
@endsection
