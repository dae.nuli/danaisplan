@extends($template)

@section('end-script')
    @parent
    <script src="{{asset('js/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
        form : '#request',
        onSuccess : function() {
            waiting();
        }
    });

    $(document).on('click','.map-modal',function (e){
        var index = $(this).data('index');
        var latlon = $('a[data-index="'+index+'"').attr('data-lalo');
        if(latlon){
            console.log(latlon);
            $('#open-modal').load('{{url("mapmodal")}}/'+index+'/'+latlon);
        }else{
            $('#open-modal').load('{{url("mapmodal")}}/'+index);
        }
    });
 
    </script>
@stop

@section('body-content')
@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
<!-- left column -->
    <div class="col-md-12">
     <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header">
             {{-- <h3 class="box-title">Quick Example</h3> --}}
            </div><!-- /.box-header -->
       <!-- form start -->
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Nama</label>
                            <input class="form-control" value="{{($usulan->id_skpd)?$usulan->skpd->instansi_name:''}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control" value="{{($usulan->id_skpd)?$usulan->skpd->email:''}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>No. Telefon</label>
                            <input class="form-control" value="{{($usulan->id_skpd)?$usulan->skpd->phone:''}}" disabled="">
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

        <div class="box box-solid">
       <!-- form start -->
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-1">
                        <div class="form-group">
                            <label>Tahun</label>
                            <input class="form-control" value="{{$usulan->years}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Program</label>
                            <input class="form-control" value="{{$usulan->program->name}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-5">
                        <div class="form-group">
                            <label>Kegiatan</label>
                            <input class="form-control" value="{{$usulan->kegiatan->name}}" disabled="">
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

        <div class="box box-solid">
       <!-- form start -->
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Dasar PerDAIS</label>
                            <input class="form-control" value="{{$usulan->dais->name}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label>TOR</label><br>
                            <div class="btn-group">
                                <a href="{{$tor}}" class="btn btn-default btn-file">
                                  <i class="fa fa-download"></i> Download           
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label>RAB</label><br>
                            <div class="btn-group">
                                <a href="{{$rab}}" class="btn btn-default btn-file">
                                  <i class="fa fa-download"></i> Download           
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

        @foreach($detail as $key => $row)
        <div class="box box-solid">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-8">
                        <div class="form-group">
                            <label>Tolak Ukur Kinerja</label>
                            <input class="form-control" value="{{$row->tolak_ukur_kinerja}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Target Kinerja (Jumlah)</label>
                            <input class="form-control" value="{{$row->jumlah_target_kinerja}}" disabled="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Target Kinerja (Satuan)</label>
                            <input class="form-control" value="{{$row->satuan_target_kinerja}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Anggaran</label>
                            <input class="form-control" value="{{$row->anggaran}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Prioritas</label>
                            <input class="form-control" value="{{$row->priority->name}}" disabled="">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Kabupaten</label>
                            <input class="form-control" value="{{$row->kabupaten->nama}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label>Kecamatan</label>
                            <input class="form-control" value="{{$row->kecamatan->nama}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label>Desa</label>
                            <input class="form-control" value="{{$row->desa->nama}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label>Maps</label>
                            <?php
                            if(!empty($row->latitude)&&!empty($row->longitude)){
                                $maps = $row->latitude.','.$row->longitude;      
                            }
                            ?>
                            <a data-lat="{{$row->latitude}}" data-lng="{{$row->longitude}}" class="form-control btn btn-default map-modal please-waiting" data-index="{{$key}}" data-lalo="{{isset($maps)?$maps:''}}"><i class="fa fa-fw fa-map-marker"></i> Maps</a>
                            <input type="hidden" class="latlng" name="latlng[]" value="{{isset($maps)?$maps:''}}">
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div>
        @endforeach
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
       <!-- form start -->
            {!! Form::open(array('url' => $action,'method'=>'POST', 'id'=>'request', 'class'=>'submit-proses')) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Catatan Revisi</label>
                                <textarea name="catatan_revisi" class="form-control" rows="7">{{$usulan->revision_note}}</textarea>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <a href="{{url($uri)}}" class="btn btn-warning">Kembali</a>
                    <button type="submit" {{($usulan->is_approved==1)?'disabled=""':''}} name="type" value="approve" class="btn btn-primary text-uppercase">approve</button>
                    <button type="submit" {{($usulan->is_approved==2)?'disabled=""':''}} name="type" value="revision" class="btn btn-info text-uppercase">revision</button>
                    <button type="submit" {{($usulan->is_approved==3)?'disabled=""':''}} name="type" value="denied" class="btn btn-danger text-uppercase">denied</button>
{{--                     <input type="submit" name="type" class="btn btn-primary text-uppercase" value="approve">
                    <input type="submit" name="type" class="btn btn-danger text-uppercase" value="denied">
                    <input type="submit" name="type" class="btn btn-info text-uppercase" value="revision"> --}}
                </div>
            {!! Form::close() !!}
        </div><!-- /.box -->
    </div>
</div>
<div id="open-modal"></div>
@endsection
