@extends($template)

@section('body-content')
@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
<!-- left column -->
    <div class="col-md-12">
     <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header">
             {{-- <h3 class="box-title">Quick Example</h3> --}}
            </div><!-- /.box-header -->
       <!-- form start -->
            {!! Form::open(array('url' => $action,'method'=>'POST')) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="form-group">
                                <label>Program</label>
                                <select disabled="" class="form-control">
                                    <option>- Pilih Program -</option>
                                    @foreach($program as $pro)
                                        <option value="{{$pro->id}}" {{($index->id_program==$pro->id)?'selected':''}}>{{$pro->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Kegiatan</label>
                                <select disabled="" class="form-control">
                                    <option>- Pilih Kegiatan -</option>
                                    @foreach($kegiatan as $keg)
                                        <option value="{{$keg->id}}" {{($index->id_kegiatan==$keg->id)?'selected':''}}>{{$keg->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Keluaran</label>
                                <input type="text" disabled="" value="{{$index->keluaran}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Anggaran</label>
                                <input type="text" disabled="" value="{{$index->anggaran}}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Catatan</label>
                                <textarea disabled="" class="form-control">{{$index->note}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Catatan Revisi</label>
                                <textarea name="catatan_revisi" class="form-control" rows="15">{{$index->revision_note}}</textarea>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <a href="{{url($uri)}}" class="btn btn-warning">Batal</a>
                    <input type="submit" name="type" class="btn btn-primary text-uppercase " value="approve">
                    <input type="submit" name="type" class="btn btn-danger text-uppercase " value="denied">
                    <input type="submit" name="type" class="btn btn-info text-uppercase " value="revision">
                </div>
            {!! Form::close() !!}
        </div><!-- /.box -->
    </div>
</div>
@endsection
