@extends($template)

@section('end-script')
    @parent

    <script src="{{asset('js/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
        form : '#request',
        onSuccess : function() {
            waiting();
        }
    });
    </script>
@endsection

@section('body-content')
@if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {{-- <h4>    <i class="icon fa fa-check"></i> Alert!</h4> --}}
    {{session()->get('success')}} sdsds
    </div>
@endif
<div class="row">
<!-- left column -->
    <div class="col-md-12">
     <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user"></i> Biodata Pengusul</h3>
            </div><!-- /.box-header -->
       <!-- form start -->
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Nama</label>
                            <input class="form-control" value="{{$detail->name}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control" value="{{$detail->email}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>No. Telefon</label>
                            <input class="form-control" value="{{$detail->phone}}" disabled="">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" disabled="" value="{{$detail->address}}" class="form-control">
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-tag"></i> Usulan</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Kabupaten</label>
                            <input type="text" disabled="" value="{{$detail->kabupaten->nama}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Kecamatan</label>
                            <input type="text" disabled="" value="{{$detail->kecamatan->nama}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Desa</label>
                            <input type="text" disabled="" value="{{$detail->desa->nama}}" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Usulan</label>
                    <textarea disabled="" rows="5" class="form-control">{{$detail->proposal}}</textarea>
                </div>
                <div class="form-group">
                    <a href="{{$pdf}}" class="btn btn-default btn-file">
                      <i class="fa fa-download"></i> Download
                    </a>
                    <p class="help-block">Size : {{$size}}</p>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
{{-- </div>
<div class="row"> --}}
    <div class="col-md-6">
        {!! Form::open(array('url' => $action,'method'=>'POST','id'=>'request')) !!}
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-tag"></i> Usulan</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label>Ditujukan Pada</label>
                    <select name="skpd" class="form-control" data-validation="required" data-validation-error-msg="This field is required.">>
                        <option value="">- Pilih SKPD -</option>
                        @foreach($skpd as $row)
                            <option value="{{$row->id}}" {{(!empty($detail->id_skpd))?'selected':''}}>{{$row->instansi_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>Catatan</label>
                    <textarea name="catatan" class="form-control" rows="10">{{$detail->note}}</textarea>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <a href="{{url($uri)}}" class="btn btn-warning">Kembali</a>
            <button type="submit" {{($detail->status==1)?'disabled=""':''}} class="btn btn-primary text-uppercase" name="type" value="approve">approve</button>
            {{-- <input type="submit" {{($detail->status==1)?'disabled=""':''}} class="btn btn-primary text-uppercase" name="type" value="approve"> --}}
            <button type="submit" {{($detail->status==2)?'disabled=""':''}} class="btn btn-danger text-uppercase" name="type" value="denied">denied</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
