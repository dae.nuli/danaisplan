@extends($template)

@section('body-content')

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Nama</label>
                            <input disabled="" value="{{isset($detail)?$detail->name:''}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Email</label>
                            <input disabled="" value="{{isset($detail)?$detail->email:''}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>No. Telefon</label>
                            <input disabled="" value="{{isset($detail)?$detail->phone:''}}" class="form-control">
                        </div>
                    </div>
                </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input disabled="" value="{{isset($detail)?$detail->address:''}}" class="form-control">
                        </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="box-footer">
            <a href="{{url($uri)}}" class="btn btn-warning">{{trans('button.bac')}}</a>
        </div>
    </div>
</div>
@endsection
