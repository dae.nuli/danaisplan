@extends($template)

@section('header-content')

<div class="pull-right" style="margin-left:5px">
    <a href="{{url($uri.'/create')}}" class="btn btn-primary"> <i class="fa fa-plus"></i> Tambah</a>
</div>

@endsection

@section('body-content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr class="text-uppercase">
            <th>#</th>
            <th>Nama</th>
            <th>email</th>
            <th>no. Telefon</th>
            <th>Status</th>
            <th></th>
          </tr>
         <?php 
         if($pages){
             $page = $pages;
         }else{
             $page = 1;
         }
         $nomor = $page + ($page-1) * ($limit-1);
         ?>
          @foreach($index as $row)
          <tr>
            <td>{{$nomor++}}</td>
            <td><a href="{{url($uri.'/show/'.$row->id)}}">{{$row->name}}</a></td>
            <td>{{$row->email}}</td>
            <td>{{$row->phone}}</td>
            <td>{!!($row->is_active)?'<span class="label label-success">Aktif</span>':'<span class="label label-danger">Tidak Aktif</span>'!!}</td>
            <td>{{date('d F Y',strtotime($row->created_at))}}</td>
            <td>
                @if($row->is_active)
                  <a href="{{url($uri.'/disable/'.$row->id)}}" class="btn btn-danger btn-xs confirm-href"><i class="fa fa-fw fa-ban"></i> Non Aktifkan</a>
                @else
                  <a href="{{url($uri.'/enable/'.$row->id)}}" class="btn btn-success btn-xs confirm-href"><i class="fa fa-fw  fa-check-circle"></i> Aktifkan</a>
                @endif

                <a href="{{url($uri.'/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> Edit</a>
{{-- 
                {!! Form::open(array('url' => $uri.'/destroy/'.$row->id,'method'=>'POST','class'=>'in-blo')) !!}
                <button type="submit" class="btn btn-danger btn-xs confirm">
                    <i class="fa fa-fw fa-trash-o"></i> {{trans('button.del')}}
                </button>
                {!! Form::close() !!}
                 --}}
                <a href="{{url($uri.'/destroy/'.$row->id)}}" class="btn btn-danger btn-xs confirm-href"><i class="fa fa-fw fa-trash-o"></i> Delete</a>
{{--                 @if($row->is_active)
                <a href="{{url($uri.'/reset/'.$row->id)}}" class="btn btn-warning btn-xs confirm-href"><i class="fa fa-fw fa-refresh"></i> Reset</a>
                @else
                <a disabled class="btn btn-warning btn-xs confirm-href"><i class="fa fa-fw fa-refresh"></i> Reset</a>
                @endif --}}
            </td>
          </tr>
          @endforeach
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
         {!! $index->links() !!}
      </div>
    </div><!-- /.box -->
</div>
@endsection

@section('end-script')
  @parent
<script src="{{asset('js/confirm-bootstrap.js')}}"></script>
<script src="{{asset('js/confirm.js')}}"></script>
<script type="text/javascript">
@if(session()->has('success'))
var n = noty({
    layout:'topRight',
    theme:'relax',
    text: '{{session()->get("success")}}',
    animation: {
        open  : 'animated fadeIn',
        close : 'animated fadeOut',
        easing: 'swing', // easing
        speed: 500 // opening & closing animation speed
    },
    type: 'success'
});
n.setTimeout(3000);
@endif
</script>
@endsection