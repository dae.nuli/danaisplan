{{-- <link href="{{asset('css/tables.css')}}" rel="stylesheet" type="text/css" /> --}}
<style type="text/css">

/** page structure **/
#wrapper {
  display: block;
  width: 100%;
  background: #fff;
  margin: 0 auto;
  padding: 10px 17px;
  -webkit-box-shadow: 2px 2px 3px -1px rgba(0,0,0,0.35);
}

#keywords {
  width: 100%;
  font-size: 12px;
  margin-bottom: 15px;
}


#keywords thead {
  cursor: pointer;
  background: #c9dff0;
}
#keywords thead tr th { 
  padding: 5px;
  text-transform: uppercase;
}
#keywords thead tr th span { 
  background-repeat: no-repeat;
  background-position: 100% 100%;
}

#keywords tbody tr { 
  color: #555;
}

#keywords tbody tr td {
  padding: 10px;
}
#keywords tbody tr td.lalign {
  text-align: left;
}
h3{
  text-transform: uppercase;
}
</style>
<center><h3>Usulan Masyarakat</h3></center>
<table id="keywords" class="pure-table">
    <thead>
        <tr>
	      <th><span>No</span></th>
	      <th><span>Kabupaten</span></th>
	      <th><span>Usulan</span></th>
	      <th><span>Status</span></th>
	      <th><span>Date</span></th>
        </tr>
    </thead>

    <tbody>
	 <?php
	 $nomor = 1;
	 ?>
	  @foreach($masyarakat as $row)
	  <tr>
	    <td>{{$nomor++}}</td>
	    <td>{{$row->kabupaten->nama}}</td>
	    <td>{!!Str::words($row->proposal,6,' ...')!!}</td>
	    <td>
	       @if($row->status==1)
	          <span class="label label-success">Approved</span>
	       @elseif($row->status==2)
	          <span class="label label-danger">Denied</span>
	       @else
	          <span class="label label-info">New</span>
	       @endif
	    </td>
	    <td>{{date('d F Y',strtotime($row->created_at))}}</td>
	  </tr>
	  @endforeach
    </tbody>
</table>