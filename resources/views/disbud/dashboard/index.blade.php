@extends($template)

@section('head-script')
  @parent
  <!-- Ion Slider -->
  <link href="{{asset('adminlte2/plugins/ionslider/ion.rangeSlider.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('adminlte2/plugins/ionslider/ion.rangeSlider.skinNice.css')}}" rel="stylesheet" type="text/css" />
  <!-- ion slider Nice -->
  <style type="text/css">
     #map,#maps {
        height: 500px;
      }
.hr-map{
  margin-top: 0;
  margin-bottom: 0;
}
  </style>
@endsection

@section('body-content')

<div class="row">
<div class="col-md-12">
 
  <div class="box" id="masyarakat">
    <div class="box-header with-border">
      <h3 class="box-title">Lokasi Usulan Masyarakat</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <div class="btn-group">
          <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>
        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div><!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <p class="text-center">
          {{ csrf_field() }}
          {!! Form::open(array('url' => $action.'#masyarakat', 'method'=>'GET')) !!}
          <div class="row">
            <div class="col-xs-2">
                <div class="form-group">
                    <label>Tahun</label>
                    <select name="years" class="form-control mtahun">
                    @foreach($yearRangeMasyarakat as $row)
                        @if(!empty($years))
                            <option value="{{$row}}" {{($years==$row)?'selected':''}}>{{$row}}</option>
                        @else
                            <option value="{{$row}}" {{($tahunSekarang==$row)?'selected':''}}>{{$row}}</option>
                        @endif
                    @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-2">
                <div class="form-group">
                    <label>Kabupaten</label>
                    <select name="mkabupaten" class="form-control mkabupaten">
                        <option value="">- Pilih Kabupaten -</option>
                        @foreach($mkabupaten as $mkab)
                            <option value="{{$mkab->id}}" {{($mIdKab==$mkab->id)?'selected':''}}>{{$mkab->nama}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xs-2">
                <div class="form-group">
                    <label>Kecamatan</label>
                    <select name="mkecamatan" class="form-control mkecamatan">
                        <option value="">- Pilih Kecamatan -</option>
                      @if(!empty($mkecamatan))
                          @foreach($mkecamatan as $mkec)
                              <option value="{{$mkec->id}}" {{($mIdKec==$mkec->id)?'selected':''}}>{{$mkec->nama}}</option>
                          @endforeach
                      @endif
                    </select>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="form-group">
                    <label>Desa</label>
                    <select name="mdesa" class="form-control mdesa">
                        <option value="">- Pilih Desa -</option>
                      @if(!empty($mdesa))
                          @foreach($mdesa as $mdes)
                              <option value="{{$mdes->id}}" {{($mIdDes==$mdes->id)?'selected':''}}>{{$mdes->nama}}</option>
                          @endforeach
                      @endif
                    </select>
                </div>
            </div>
            <div class="col-xs-1">
                <div class="form-group">
                    <label></label><br>
                    <button type="submit" class="btn btn-primary">{{trans('button.sub')}}</button>
                </div>
            </div>
            <div class="col-xs-1">
                <div class="form-group">
                    <label></label><br>
                    <a href="{{$printMasyarakat}}" target="_blank" class="btn btn-default"><i class="fa fa-print"></i>  Print</a>
                </div>
            </div>
          </div>
          {!! Form::close() !!}
          </p>
          <div class="chart">
            <div id="map"></div>
          </div><!-- /.chart-responsive -->
        </div><!-- /.col -->
 
      </div><!-- /.row -->
    </div><!-- ./box-body -->

  </div><!-- /.box -->
<div class="box box-solid">
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th>#</th>
              <th>Kabupaten</th>
              <th>Usulan</th>
              <th>Status</th>
              <th>Created Date</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
         <?php
         $nomor = 1;
         ?>
          @foreach($masyarakat as $row)
          <tr>
            <td>{{$nomor++}}</td>
            <td>{{$row->kabupaten->nama}}</td>
            <td>{!!Str::words($row->proposal,6,' ...')!!}</td>
            <td>
               @if($row->status==1)
                  <span class="label label-success">Approved</span>
               @elseif($row->status==2)
                  <span class="label label-danger">Denied</span>
               @else
                  <span class="label label-info">New</span>
               @endif
            </td>
            <td>{{date('d F Y',strtotime($row->created_at))}}</td>
            <td>
                <a href="{{url($uri.'/detail/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-info"></i> Detail</a>
            </td>
          </tr>
          @endforeach
          </tbody>
        </table>
      </div><!-- /.table-responsive -->
    </div><!-- /.box-body -->
 
  </div><!-- /.box -->
<h2 class="page-header">SKPD</h2>

  <div class="box" id="skpd">
    <div class="box-header with-border">
      <h3 class="box-title">Lokasi Usulan SKPD</h3>
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        <div class="btn-group">
          <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>
        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
    </div><!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <p class="text-center">
            {!! Form::open(array('url' => $action.'#skpd', 'method'=>'GET')) !!}
            <div class="row">
              <div class="col-xs-2">
                  <div class="form-group">
                      <label>Tahun</label>
                      <select name="syears" class="form-control tahun">
                      @foreach($yearRangeSKPD as $row)
                          @if(!empty($syears))
                              <option value="{{$row}}" {{($syears==$row)?'selected':''}}>{{$row}}</option>
                          @else
                              <option value="{{$row}}" {{($tahunSekarang==$row)?'selected':''}}>{{$row}}</option>
                          @endif
                      @endforeach
                      </select>
                  </div>
              </div>
              <div class="col-xs-5">
                  <div class="form-group">
                      <label>Program</label>
                      <select name="program" class="form-control program">
                          <option value="">- Pilih Program -</option>
                          @foreach($program as $pro)
                              <option value="{{$pro->id}}" {{($idProgram==$pro->id)?'selected':''}}>{{$pro->name}}</option>
                          @endforeach
                      </select>
                  </div>
              </div>
              <div class="col-xs-5">
                  <div class="form-group">
                      <label>Kegiatan</label>
                      <select name="kegiatan" class="form-control kegiatan">
                          <option value="">- Pilih Kegiatan -</option>
                        @if(!empty($kegiatan))
                            @foreach($kegiatan as $keg)
                                <option value="{{$keg->id}}" {{($idKegiatan==$keg->id)?'selected':''}}>{{$keg->name}}</option>
                            @endforeach
                        @endif
                      </select>
                  </div>
              </div>
{{--               <div class="col-xs-3">
                  <div class="form-group">
                    <label>Anggaran</label>
                      <input id="range_1" type="text" name="anggaran" value="" />
                  </div>
              </div> --}}
            </div>
            {{-- ROW --}}
            <div class="row">
                <div class="col-xs-3">
                    <div class="form-group">
                        <label>Kabupaten</label>
                        <select name="skabupaten" class="form-control skabupaten">
                            <option value="">- Pilih Kabupaten -</option>
                            @foreach($skabupaten as $skab)
                                <option value="{{$skab->id}}" {{($sIdKab==$skab->id)?'selected':''}}>{{$skab->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label>Kecamatan</label>
                        <select name="skecamatan" class="form-control skecamatan">
                            <option value="">- Pilih Kecamatan -</option>
                          @if(!empty($skecamatan))
                              @foreach($skecamatan as $skec)
                                  <option value="{{$skec->id}}" {{($sIdKec==$skec->id)?'selected':''}}>{{$skec->nama}}</option>
                              @endforeach
                          @endif
                        </select>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label>Desa</label>
                        <select name="sdesa" class="form-control sdesa">
                            <option value="">- Pilih Desa -</option>
                          @if(!empty($sdesa))
                              @foreach($sdesa as $sdes)
                                  <option value="{{$sdes->id}}" {{($sIdDes==$sdes->id)?'selected':''}}>{{$sdes->nama}}</option>
                              @endforeach
                          @endif
                        </select>
                    </div>
                </div>
                <div class="col-xs-1">
                    <div class="form-group">
                        <label></label><br>
                        <button type="submit" class="btn btn-primary pull-right">{{trans('button.sub')}}</button>
                    </div>
                </div>
                <div class="col-xs-1">
                    <div class="form-group">
                        <label></label><br>
                        <a href="{{$printSkpd}}" target="_blank" class="btn btn-default"><i class="fa fa-print"></i>  Print</a>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
          </p>
          <div class="chart">
                <div id="maps"></div>
          </div><!-- /.chart-responsive -->
        </div><!-- /.col -->
 
      </div><!-- /.row -->
    </div><!-- ./box-body -->
  </div><!-- /.box -->
  <div class="box box-solid">
    <div class="box-body">
      <div class="table-responsive">
        <table class="table no-margin">
          <thead>
            <tr>
              <th>#</th>
              <th>Nama Instansi</th>
              <th>Anggaran</th>
              <th>Kecamatan</th>
              <th>Desa</th>
              <th>Status</th>
              <th>Created Date</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
         <?php
         $nomor = 1;
         ?>
          @foreach($usulanMap as $row)
          <tr>
            <td>{{$nomor++}}</td>
            <td>{{$row['name']}}</td>
            <td>Rp {{$row['anggaran']}}</td>
            <td>{{$row['kecamatan']}}</td>
            <td>{{$row['desa']}}</td>
            <td>
               @if($row['status']==1)
                  <span class="label label-success">Approved</span>
               @elseif($row['status']==2)
                  <span class="label label-info">Revision</span>
               @elseif($row['status']==3)
                  <span class="label label-danger">Denied</span>
               @elseif($row['status']==4)
                  <span class="label label-warning">Improve Revision</span>
               @else
                  <span class="label label-warning">New</span>
               @endif
            </td>
            <td>{{date('d F Y',strtotime($row['created_at']))}}</td>
            <td>
                <a href="{{url($uriSkpd.'/detail/'.$row['id'])}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-info"></i> Detail</a>
            </td>
          </tr>
          @endforeach
          </tbody>
        </table>
      </div><!-- /.table-responsive -->
    </div><!-- /.box-body -->
 
  </div><!-- /.box -->
</div><!-- /.col -->
</div><!-- /.row -->

@endsection

@section('end-script')
  @parent
  <!-- Ion Slider -->
  <script src="{{asset('adminlte2/plugins/ionslider/ion.rangeSlider.js')}}" type="text/javascript"></script>

  <!-- Bootstrap slider -->
  {{-- // <script src="{{asset('adminlte2/plugins/bootstrap-slider/bootstrap-slider.js')}}" type="text/javascript"></script> --}}
  <script type="text/javascript">
  // $("#range_1").ionRangeSlider({
  //   min: 100000,
  //   max: 10000000,
  //   from: {{isset($from)?$from:1000000}},
  //   to: {{isset($to)?$to:7000000}},
  //   type: 'double',
  //   step: 1,
  //   prefix: "IDR ",
  //   grid: true,
  //   prettify_separator: ".",
  //   prettify_enabled: true,
  //   values_separator: " → "
  // });
    $(document).on('change','.mkabupaten',function(){
        var CSRF_TOKEN     = $('input[name="_token"]').attr('value');
        var kabupaten      = $(this).val();
        // var kabupatenClass = $(this);
        // kabupatenClass.parents('.col-xs-4').next().next().find('.desa').html('<option value="">- Pilih Desa -</option>');
        $.ajax({
            type : 'POST',
            url  : '/kecamatan',
            data : {_token: CSRF_TOKEN, id_kabupaten : kabupaten},
            dataType: 'JSON',
            beforeSend : function() {
                $('.mkecamatan').html("<option value=''>Loading...</option>");
            },
            success : function(data){
                $('.mkecamatan').html(data);
            }
        }).done(function(){
        });
    });

    $(document).on('change','.mkecamatan',function(){
        var CSRF_TOKEN     = $('input[name="_token"]').attr('value');
        var kecamatan      = $(this).val();
        $.ajax({
            type : 'POST',
            url  : '/desa',
            data : {_token: CSRF_TOKEN, id_kecamatan : kecamatan},
            cache:false,
            beforeSend : function() {
                // $('.desa').html("<option value=''>Loading...</option>");
                $('.mdesa').html("<option value=''>Loading...</option>");
            },
            success : function(data){
                // $('.desa').html(data);
                $('.mdesa').html(data);
            }
        }).done(function(){
        });
    });

    $(document).on('change','.skabupaten',function(){
        var CSRF_TOKEN     = $('input[name="_token"]').attr('value');
        var kabupaten      = $(this).val();
        $.ajax({
            type : 'POST',
            url  : '/kecamatan',
            data : {_token: CSRF_TOKEN, id_kabupaten : kabupaten},
            dataType: 'JSON',
            beforeSend : function() {
                $('.skecamatan').html("<option value=''>Loading...</option>");
            },
            success : function(data){
                $('.skecamatan').html(data);
            }
        }).done(function(){
        });
    });

    $(document).on('change','.skecamatan',function(){
        var CSRF_TOKEN     = $('input[name="_token"]').attr('value');
        var kecamatan      = $(this).val();
        $.ajax({
            type : 'POST',
            url  : '/desa',
            data : {_token: CSRF_TOKEN, id_kecamatan : kecamatan},
            cache:false,
            beforeSend : function() {
                $('.sdesa').html("<option value=''>Loading...</option>");
            },
            success : function(data){
                $('.sdesa').html(data);
            }
        }).done(function(){
        });
    });

  $(document).on('change','.tahun',function(){
      var CSRF_TOKEN = $('input[name="_token"]').attr('value');
      var tahun    = $(this).val();
        $('.kegiatan').html('<option value="">- Pilih Kegiatan -</option>');
      $.ajax({
          type : 'POST',
          url  : '/disbud/home/tahun',
          data : {_token: CSRF_TOKEN, tahun : tahun},
          dataType: 'JSON',
          beforeSend : function() {
              $('.program').html("<option value=''>Loading...</option>");
          },
          success : function(data){
              $('.program').html(data);
          }
      }).done(function(){
          // $('.kecamatan').val($('.kecamatan').attr('data-selected'));
      });
  });
  $(document).on('change','.program',function(){
      var CSRF_TOKEN = $('input[name="_token"]').attr('value');
      var program    = $(this).val();
      $.ajax({
          type : 'POST',
          url  : '/disbud/home/program',
          data : {_token: CSRF_TOKEN, id_program : program},
          dataType: 'JSON',
          beforeSend : function() {
              $('.kegiatan').html("<option value=''>Loading...</option>");
          },
          success : function(data){
              $('.kegiatan').html(data);
          }
      }).done(function(){
          // $('.kecamatan').val($('.kecamatan').attr('data-selected'));
      });
  });
  //Usulan SKPD

  <?php $totSkpd = count($usulanMap); ?>
  var usulanMap = [
  @foreach($usulanMap as $i => $row)
    @if(!empty($row['latitude'])&&!empty($row['longitude']))
      @if(($i+1)==$totSkpd)
        [{{$row['latitude']}},{{$row['longitude']}}, "{{$row['kabupaten']}}, Kec. {{$row['kecamatan']}}, Des. {{$row['desa']}}","{{$row['status']}}","{{Helper::statusUsulanSkpd($row['status'])}}","{{$row['tolak_ukur']}}, {{$row['jumlah']}} {{$row['satuan']}} ","Rp {{$row['anggaran']}}"]
      @else
        [{{$row['latitude']}},{{$row['longitude']}}, "{{$row['kabupaten']}}, Kec. {{$row['kecamatan']}}, Des. {{$row['desa']}}","{{$row['status']}}","{{Helper::statusUsulanSkpd($row['status'])}}","{{$row['tolak_ukur']}}, {{$row['jumlah']}} {{$row['satuan']}} ","Rp {{$row['anggaran']}}"],
      @endif
    @endif
  @endforeach
  ];

    var animationStatus=null;
  var map1;
  // function initMap() {
  //   map1 = new google.maps.Map(document.getElementById('maps'), {
  //     center: {lat: -7.800868, lng: 110.373299},
  //     // mapTypeControl: false,
  //     streetViewControl: false,
  //     scrollwheel: false,
  //     zoom: 11
  //   });

    // for (var i = 0; i < usulanMap.length; i++) {
    //       var marker = new google.maps.Marker({
    //           position: {lat: usulanMap[i][0], lng: usulanMap[i][1]},
    //           map: map1,
    //           title: usulanMap[i][2]
    //       });
    //   // google.maps.event.addListener(marker, 'click', function() {
    //   //       window.location.href = this.url;
    //   //   });
    //   map1.addListener('center_changed', function() {
    //      // 3 seconds after the center of the map has changed, pan back to the
    //      // marker.
    //      window.setTimeout(function() {
    //        map1.panTo(this.getPosition());
    //      }, 3000);
    //    });

    //    marker.addListener('click', function() {
    //      map1.setZoom(19);
    //      map1.setCenter(this.getPosition());
    //    });        
    // }
  // }

  //Usulan Masyarakat
	<?php $tot = count($masyarakat); ?>
	var masyarakat = [
	@foreach($masyarakat as $i => $row)
		@if(!empty($row->latitude)&&!empty($row->longitude))
			@if(($i+1)==$tot)
				[{{$row->latitude}},{{$row->longitude}},'{{Helper::statusUsulanMasyarakat($row->status)}}',{{$row->status}},"{{url($uriMasyarakat.'/'.$row->id)}}","{{$row->kabupaten->nama}}, Kec. {{$row->kecamatan->nama}}, Des. {{$row->desa->nama}}","{{$row->name}}, {{$row->phone}}, {{$row->email}}"]
			@else
				[{{$row->latitude}},{{$row->longitude}},'{{Helper::statusUsulanMasyarakat($row->status)}}',{{$row->status}},"{{url($uriMasyarakat.'/'.$row->id)}}","{{$row->kabupaten->nama}}, Kec. {{$row->kecamatan->nama}}, Des. {{$row->desa->nama}}","{{$row->name}}, {{$row->phone}}, {{$row->email}}"],
			@endif
		@endif
	@endforeach
	];

      var map;
      function initMap() {
        var infowindow = new google.maps.InfoWindow();
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -7.800868, lng: 110.373299},
          mapTypeControl: false,
          streetViewControl: false,
          scrollwheel: false,
          zoom: 10
        });

        map1 = new google.maps.Map(document.getElementById('maps'), {
          center: {lat: -7.800868, lng: 110.373299},
          mapTypeControl: false,
          streetViewControl: false,
          scrollwheel: false,
          zoom: 10
        });

        for (var i = 0; i < usulanMap.length; i++) {
              var marker = new google.maps.Marker({
                  position: {lat: usulanMap[i][0], lng: usulanMap[i][1]},
                  map: map1,
                  icon: imageskpd(usulanMap[i][3]),
                  titles: usulanMap[i][2],
                  status: usulanMap[i][4],
                  keluaran: usulanMap[i][5],
                  anggaran: usulanMap[i][6]
              });
          // google.maps.event.addListener(marker, 'click', function() {
          //       window.location.href = this.url;
          //   });
          map1.addListener('center_changed', function() {
             // 3 seconds after the center of the map has changed, pan back to the
             // marker.
             window.setTimeout(function() {
               map1.panTo(this.getPosition());
             }, 3000);
           });

           marker.addListener('click', function() {
             map1.setZoom(15);
             map1.setCenter(this.getPosition());
           });

          google.maps.event.addListener(marker, 'mouseover', function() {

            var content = "<div class='infowindow-content'><b>Lokasi</b> : "+this.titles+"<br><hr class='hr-map'> <b>Keluaran</b> : "+this.keluaran+"<br><hr class='hr-map'> <b>Anggaran</b> : "+this.anggaran+"<br><hr class='hr-map'> <b>Keterangan</b> : "+this.status+"</div>" ;

            infowindow.setContent(content);
            infowindow.open(map1, this);
          });

          google.maps.event.addListener(marker, 'mouseout', function() {
            if(infowindow)
            {
              infowindow.close()
            }
          });

        }

    		for (var i = 0; i < masyarakat.length; i++) {
    	       	var marker = new google.maps.Marker({
    	          	position: {lat: masyarakat[i][0], lng: masyarakat[i][1]},
    	          	map: map,
    	          	icon: image(masyarakat[i][3]),
    	          	status: masyarakat[i][2],
                  location:masyarakat[i][5],
                  url: masyarakat[i][4],
    	          	contact: masyarakat[i][6]
    	        });
           marker.addListener('click', function() {
             map.setZoom(15);
             map.setCenter(this.getPosition());
           });
    			// google.maps.event.addListener(marker, 'click', function() {
    		 //        window.location.href = this.url;
    		 //    });
          google.maps.event.addListener(marker, 'mouseover', function() {

            var content = "<div class='infowindow-content'><b>Lokasi</b> : "+this.location+"<br><hr class='hr-map'> <b>Kontak</b> : "+this.contact+"<br><hr class='hr-map'> <b>Keterangan</b> : "+this.status+"</div>" ;

            infowindow.setContent(content);
            infowindow.open(map, this);
          });

          google.maps.event.addListener(marker, 'mouseout', function() {
            if(infowindow)
            {
              infowindow.close()
            }
          });

    		}

      }

    function image(status){
    	if(status==0) {
        return '{{asset("marker/mm_20_red.png")}}';
    	}else if(status==1) {
    		return '{{asset("marker/mm_20_green.png")}}';
    	}else if(status==2) {
        return '{{asset("marker/mm_20_gray.png")}}';
    	}
    }

    function imageskpd(status){
      if(status==0) {
        return '{{asset("marker/mm_20_red.png")}}';
      }else if(status==1) {
        return '{{asset("marker/mm_20_green.png")}}';
      }else if(status==2) {
        return '{{asset("marker/mm_20_blue.png")}}';
      }else if(status==3) {
        return '{{asset("marker/mm_20_gray.png")}}';
      }
    }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8HshEMMYjzaBhabEOoWmW2-Ix2lN8T8k&callback=initMap" async defer></script>
@endsection