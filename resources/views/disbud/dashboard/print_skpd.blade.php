{{-- <link href="{{asset('css/tables.css')}}" rel="stylesheet" type="text/css" /> --}}
<style type="text/css">

/** page structure **/
#wrapper {
  display: block;
  width: 100%;
  background: #fff;
  margin: 0 auto;
  padding: 10px 17px;
  -webkit-box-shadow: 2px 2px 3px -1px rgba(0,0,0,0.35);
}

#keywords {
  margin: 0 auto;
  font-size: 15px;
  margin-bottom: 15px;
}


#keywords thead {
  cursor: pointer;
  background: #c9dff0;
}
#keywords thead tr th { 
  font-weight: bold;
  padding: 12px 30px;
  padding-left: 42px;
}
#keywords thead tr th span { 
  padding-right: 20px;
  background-repeat: no-repeat;
  background-position: 100% 100%;
}

#keywords thead tr th.headerSortUp, #keywords thead tr th.headerSortDown {
  background: #acc8dd;
}

#keywords thead tr th.headerSortUp span {
  /*background-image: url('http://i.imgur.com/SP99ZPJ.png');*/
}
#keywords thead tr th.headerSortDown span {
  /*background-image: url('http://i.imgur.com/RkA9MBo.png');*/
}


#keywords tbody tr { 
  color: #555;
}
#keywords tbody tr th {
  text-align: center;
  padding: 15px 10px;
}
#keywords tbody tr td {
  padding: 15px 10px;
}
#keywords tbody tr td.lalign {
  text-align: left;
}
</style>
<center><h3>Usulan SKPD</h3></center>
<table id="keywords" class="pure-table">
    <thead>
        <tr>
          <th><span>No</span></th>
          <th><span>Nama Instansi</span></th>
          <th><span>Anggaran</span></th>
          <th><span>Kecamatan</span></th>
          <th><span>Desa</span></th>
          <th><span>Status</span></th>
          <th><span>Date</span></th>
        </tr>
    </thead>

    <tbody>
	 <?php
	 $nomor = 1;
	 ?>
	  @foreach($skpd as $row)
      <tr>
        <td>{{$nomor++}}</td>
        <td>{{$row['name']}}</td>
        <td>Rp {{$row['anggaran']}}</td>
        <td>{{$row['kecamatan']}}</td>
        <td>{{$row['desa']}}</td>
        <td>
           @if($row['status']==1)
              <span class="label label-success">Approved</span>
           @elseif($row['status']==2)
              <span class="label label-info">Revision</span>
           @elseif($row['status']==3)
              <span class="label label-danger">Denied</span>
           @elseif($row['status']==4)
              <span class="label label-warning">Improve Revision</span>
           @else
              <span class="label label-warning">New</span>
           @endif
        </td>
        <td>{{date('d F Y',strtotime($row['created_at']))}}</td>
      </tr>
	  @endforeach
    </tbody>
</table>