
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Disbud | Log in</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{asset('adminlte2/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{asset('adminlte2/font-awesome-4.4.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{asset('adminlte2/dist/css/AdminLTE.css')}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="#">Disbud</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        @if(session()->has('error_login'))
        <p class="text-red login-box-msg">{{session()->get('error_login')}}</p>
        @endif
        {!! Form::open(array('url' => url('login'),'method'=>'POST','id'=>'request')) !!}
          <div class="form-group has-feedback">
            <input type="email" class="form-control" name="email" autocomplete="off" placeholder="Email" data-validation="required|email" data-validation-error-msg="This field must be a valid email address."/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" autocomplete="off" placeholder="Password" data-validation="required" data-validation-error-msg="This field is required."/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            {{-- <div class="col-xs-8">    
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>                        
            </div> --}}<!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
        {!!Form::close()!!}

{{--         <div class="social-auth-links text-center">
          <p>- OR -</p>
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
        </div> --}}<!-- /.social-auth-links -->

{{--         <a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a> --}}

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="{{asset('adminlte2/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>

    <script src="{{asset('js/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
        form : '#request',
        onSuccess : function() {
          $.blockUI({ 
              message: '<h1 style="font-weight:300">Loging in</h1>',
              css: { 
                  border: 'none', 
                  padding: '15px', 
                  backgroundColor: '#fff', 
                  '-webkit-border-radius': '0', 
                  '-moz-border-radius': '0',  
                  color: '#000',
                  'text-transform': 'uppercase'
              },
              baseZ:9999
          });
        }
    });
    </script>
  </body>
</html>