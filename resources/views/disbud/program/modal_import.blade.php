<link href="{{asset('css/select2.css')}}" rel="stylesheet" />
<script src="{{asset('js/select2.js')}}"></script>
<script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
<script type="text/javascript">
$.validate({
    form : '#requests',
    onSuccess : function() {
        waiting();
    }
});
$('.nama_program').select2({
    theme: "classic"
});
$(function() {
    $.unblockUI();
    $('#compose-modal').modal({backdrop: 'static', keyboard: false, show:true});
});
</script>
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        {!!Form::open(array('url'=>$action, 'method'=>'POST','id'=>'requests'))!!}
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Import Program</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label>Nama Program</label>
                                <select class="form-control nama_program" style="padding:0;" name="program" data-validation="required" data-validation-error-msg="This field is required.">
                                    <option value="">- Pilih Program -</option>
                                    @foreach($program as $row)
                                        <option value="{{$row->id}}">({{$row->code_program}}) {{$row->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer clearfix">
                <button class="btn btn-default close-waiting" data-dismiss="modal">{{trans('button.cl')}}</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
        {!!Form::close()!!}
    </div>
</div>