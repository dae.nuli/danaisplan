@extends($template)

@section('header-content')

<div class="pull-right" style="margin-left:5px">
    <a href="{{url($uri.'/create')}}" class="btn btn-primary"> <i class="fa fa-plus"></i> Tambah</a>
</div>

@endsection

@section('body-content')
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      {{-- <div class="box-header">
        <h3 class="box-title">Responsive Hover Table</h3>
        <div class="box-tools">
          <div class="input-group">
            <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
            <div class="input-group-btn">
              <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div> --}}<!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr class="text-uppercase">
            <th style="width: 50px">#</th>
            <th>Kode</th>
            <th>Nama</th>
            <th>Total Kegiatan</th>
            <th>Tahun</th>
            <th></th>
          </tr>
         <?php 
         if($pages){
             $page = $pages;
         }else{
             $page = 1;
         }
         $nomor = $page + ($page-1) * ($limit-1);
         ?>
          @foreach($index as $row)
          <tr>
            <td>{{$nomor++}}</td>
            <td>{{$row->code_program}}</td>
            <td>{!!Str::words($row->name,4,' ...')!!}</td>
            <td>{{Helper::totalKegiatan($row->id)}}</td>
            <td>{{$row->years}}</td>
            <td>
                <a href="{{url($uri.'/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> {{trans('button.ed')}}</a>
                {{-- <a href="{{url($uri.'/detail/'.$row->id)}}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-info"></i> {{trans('button.det')}}</a> --}}
                <a href="{{url($uri.'/delete/'.$row->id)}}" class="btn btn-danger btn-xs confirm-href"><i class="fa fa-fw fa-info"></i> {{trans('button.del')}}</a>
                {{-- 
                {!! Form::open(array('url' => $uri.'/delete/'.$row->id,'method'=>'POST','class'=>'in-blo')) !!}
                <button type="submit" class="btn btn-danger btn-xs confirm">
                    <i class="fa fa-fw fa-trash-o"></i> {{trans('button.del')}}
                </button>
                {!! Form::close() !!}
 --}}
            </td>
          </tr>
          @endforeach
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
         {!! $index->links() !!}
      </div>
    </div><!-- /.box -->
</div>
@endsection


@section('end-script')
  @parent
<script src="{{asset('js/confirm-bootstrap.js')}}"></script>
<script src="{{asset('js/confirm.js')}}"></script>
<script type="text/javascript">
@if(session()->has('success'))
var n = noty({
    layout:'topRight',
    theme:'relax',
    text: '{{session()->get("success")}}',
    animation: {
        open  : 'animated fadeIn',
        close : 'animated fadeOut',
        easing: 'swing', // easing
        speed: 500 // opening & closing animation speed
    },
    type: 'success'
});
@endif
</script>
@endsection