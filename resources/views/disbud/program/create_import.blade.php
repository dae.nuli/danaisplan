@extends($template)

@section('header-content')

<div class="pull-right" style="margin-left:5px">
    <a class="btn btn-primary import-program" data-url="{{$import}}">Import</a>
</div>

@endsection

@section('end-script')
    @parent

    <script src="{{asset('js/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
        form : '#request',
        onSuccess : function() {
            waiting();
        }
    });
   
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".add-dinamic"); //Fields wrapper
    var add_button      = $(".add-item"); //Add button ID
    
    var x = {{count($kegiatan)+1}}; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<tr>'+
            '<td><input type="text" name="kegiatan_code[]" class="form-control" autocomplete="off" data-validation="required" data-validation-error-msg="This field is required."/></td>'+
            '<td><input type="text" name="kegiatan_name[]" class="form-control" autocomplete="off" data-validation="required" data-validation-error-msg="This field is required."/></td>'+
            '<td><input name="description[]" class="form-control" autocomplete="off"/></td>'+
            '<td><a href="" class="btn btn-danger btn-xs delete-item"><i class="fa fa-fw fa-trash-o"></i> {{trans("button.del")}}</a></td>'+
        '</tr>'); //add input box
        }
    });
    
    $(document).on("click",".delete-item", function(e){ //user click on remove text
        e.preventDefault(); $(this).parents('.add-dinamic tr').remove(); x--;
    })
    </script>
@endsection

@section('body-content')
@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
    <div class="col-md-12">
        {!! Form::open(array('url' => $action,'method'=>'POST','id'=>'request')) !!}
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-2">
                        <div class="form-group">
                            <label>Kode Program</label>
                            <input type="text" name="program_code" value="{{$program->code_program}}" class="form-control" autocomplete="off" data-validation="required" data-validation-error-msg="This field is required.">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Nama Program</label>
                            <input type="text" name="program_name" value="{{$program->name}}" class="form-control" data-validation="required" autocomplete="off" data-validation-error-msg="This field is required.">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Tahun</label>
                            <select name="program_years" class="form-control">
                                @foreach($yearRange as $row)
                                    <option value="{{$row}}">{{$row}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        

        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title pull-left">Kegiatan</h3>
                <div class="pull-right" style="margin-left:5px">
                    <button class="btn btn-xs btn-primary add-item"><i class="fa fa-fw fa-plus"></i> Tambah</button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
            <table class="table">
                <thead>
                    <tr>
                        <th>Kode Kegiatan</th>
                        <th>Nama Kegiatan</th>
                        <th>Keterangan</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="add-dinamic">
                    @foreach($kegiatan as $row)
                        <input type="hidden" name="item_id[]" value="{{$row->id}}">
                        <tr><td><input type="text" name="kegiatan_code[]" value="{{$row->code_kegiatan}}" class="form-control" autocomplete="off" data-validation="required" data-validation-error-msg="This field is required."></td><td><input type="text" name="kegiatan_name[]" value="{{$row->name}}" class="form-control" autocomplete="off" data-validation="required" data-validation-error-msg="This field is required."></td><td><input name="description[]" value="{{$row->description}}" class="form-control" autocomplete="off"></td><td><a href="" class="btn btn-danger btn-xs delete-item"><i class="fa fa-fw fa-trash-o"></i> Hapus</a></td></tr>
                    @endforeach
                </tbody>
            </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->

        <div class="box-footer">
            <a href="{{url($uri)}}" class="btn btn-warning">{{trans('button.bac')}}</a>
            <button type="submit" class="btn btn-primary">{{trans('button.sub')}}</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<div id="open-modal"></div>
@endsection
