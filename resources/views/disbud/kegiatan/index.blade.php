@extends($template)

@section('header-content')

<div class="pull-right" style="margin-left:5px">
    <a href="{{url($uri.'/create')}}" class="btn btn-primary"> <i class="fa fa-plus"></i> Tambah</a>
</div>

@stop

@section('body-content')
<div class="row">
  <div class="col-xs-12">
<div class="box">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <thead>
            <tr class="text-uppercase">
                <th style="width: 50px">#</th>
                <th>Nama Kegiatan</th>
                <th>Nama Program</th>
                <th></th>
            </tr>
            </thead>
             <?php 
             if($pages){
                 $page = $pages;
             }else{
                 $page = 1;
             }
             $nomor = $page + ($page-1) * ($limit-1);
             ?>
            <tbody>
            @foreach($index as $row)
            <tr>
                <td>{{$nomor++}}.</td>
                <td><a href="{{url($uri.'/detail/'.$row->id)}}">{{Str::words($row->name,5,' ...')}}</a></td>
                <td>{{(isset($row->program->name)?Str::words($row->program->name,5,' ...'):'-')}}</td>
                <td>
                    <a href="{{url($uri.'/detail/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-info"></i> Detail</a>
                    <a href="{{url($uri.'/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> Edit</a>
                    <a href="{{url($uri.'/delete/'.$row->id)}}" class="btn btn-danger btn-xs delete"><i class="fa fa-fw fa-trash-o"></i> Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div><!-- /.box-body -->
    <div class="box-footer clearfix">
        {!!$index->links()!!}
    </div>
</div>
</div>
</div>
@stop