@extends($template)

@section('end-script')
    @parent
    <link href="{{asset('css/select2.css')}}" rel="stylesheet" />
    <script src="{{asset('js/select2.js')}}"></script>
    <script type="text/javascript">
    $(function() {
        $('.filter').select2({
            theme: "classic"
        });
    });
    </script>
@endsection

@section('header-content')

<div class="pull-right" style="margin-left:5px">
    <a href="{{url($uri.'/summary')}}" class="btn btn-primary"> <i class="fa fa-download"></i> Download Summary</a>
    <a href="{{url($uri.'/full')}}" class="btn btn-success"> <i class="fa fa-download"></i> Download Full </a>
</div>


@endsection

@section('body-content')

<div class="box box-solid">
<!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-xs-2">
              {{Form::open(array('url' => $action, 'method' => 'post'))}}
              <select class="form-control filter" style="padding:0;" name="year" onchange="this.form.submit()">
                  @foreach($tahun as $row)
                      <option value="{{$row}}" {{($year==$row) ? 'selected' : ''}}>{{$row}}</option>
                  @endforeach
              </select>
              {{Form::close()}}
            </div>
            <div class="col-xs-4">
                {{Form::open(array('url' => $actionKabupaten, 'method' => 'post'))}}
                <select class="form-control filter" name="kabupaten" style="padding:0;" onchange="this.form.submit()">
                      <option value="">- Pilih Kabupaten -</option>
                    @foreach($kabupaten as $kab)
                        <option value="{{$kab->id}}" {{($kabSelected==$kab->id) ? 'selected' : ''}}>{{$kab->nama}}</option>
                    @endforeach
                </select>
                {{Form::close()}}
            </div>
            <div class="col-xs-3">
                {{Form::open(array('url' => $actionKecamatan, 'method' => 'post'))}}
                <select class="form-control filter" name="kecamatan" style="padding:0;" onchange="this.form.submit()">
                    @if(isset($kecamatan))
                      <option value="">- Pilih Kecamatan -</option>
                      @foreach($kecamatan as $kec)
                          <option value="{{$kec->id}}" {{($kecSelected==$kec->id) ? 'selected' : ''}}>{{$kec->nama}}</option>
                      @endforeach
                    @else
                      <option value="">- Pilih Kecamatan -</option>
                    @endif
                </select>
                {{Form::close()}}
            </div>
            <div class="col-xs-3">
                {{Form::open(array('url' => $actionDesa, 'method' => 'post'))}}
                <select class="form-control filter" name="desa" style="padding:0;" onchange="this.form.submit()">
                  @if(isset($desa))
                      <option value="">- Pilih Desa -</option>
                      @foreach($desa as $des)
                          <option value="{{$des->id}}" {{($desSelected==$des->id) ? 'selected' : ''}}>{{$des->nama}}</option>
                      @endforeach
                    @else
                      <option value="">- Pilih Desa -</option>
                    @endif
                </select>
                {{Form::close()}}
            </div>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->

<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr class="text-uppercase">
            <th>#</th>
            <th>Nama</th>
            <th>Usulan</th>
            <th>Lokasi</th>
            <th>Status</th>
            <th>Date</th>
            <th></th>
          </tr>
         <?php 
         if($pages){
             $page = $pages;
         }else{
             $page = 1;
         }
         $nomor = $page + ($page-1) * ($limit-1);
         ?>
          @foreach($index as $row)
          <tr>
            <td>{{$nomor++}}</td>
            <td>{{$row->name}}</td>
            <td>{!!Str::words($row->proposal,6,' ...')!!}</td>
            <td>{{$row->kabupaten->nama}}, {{$row->kecamatan->nama}}, {{$row->desa->nama}}</td>
            <td>
               @if($row->status==1)
                  <span class="label label-success">Approved</span>
               @elseif($row->status==2)
                  <span class="label label-danger">Denied</span>
               @else
                  <span class="label label-info">New</span>
               @endif
            </td>
            <td>{{date('d F Y',strtotime($row->created_at))}}</td>
            <td>
                <a href="{{url($uri.'/detail/'.$row->id)}}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-info"></i> {{trans('button.det')}}</a>
            </td>
          </tr>
          @endforeach
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
         {!! $index->links() !!}
      </div>
    </div><!-- /.box -->
</div>
@endsection