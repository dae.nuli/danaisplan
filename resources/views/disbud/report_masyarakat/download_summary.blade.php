<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
        thead tr td{
            text-align: center;
        }
        .success{
            color:#00a65a;
        }
        .danger{
            color:#dd4b39;
        }
        .info{
            color:#00c0ef;
        }
        </style>
    </head>
    <table border="1">
        <thead>
        <tr>
            <td><b>NO</b></td>
            <td><b>NAMA</b></td>
            <td><b>NO. TELEFON</b></td>
            <td><b>EMAIL</b></td>
            <td><b>ALAMAT</b></td>
            <td><b>USULAN</b></td>
            <td><b>STATUS</b></td>
            <td><b>CREATED AT</b></td>
        </tr>
        </thead>
        <tbody>
            <?php $nomor = 1; ?>
            @foreach($index as $row)
            <tr>
                <td>{{$nomor++}}</td>
                <td>{{$row->name}}</td>
                <td>{{$row->phone}}</td>
                <td>{{$row->email}}</td>
                <td>{{$row->address}}</td>
                <td>{{$row->proposal}}</td>
                <td>
                    @if($row->status==1)
                        <span class="success">APPROVED</span>
                    @elseif($row->status==2)
                        <span class="danger">DENIED</span>
                    @else
                        <span class="info">NEW</span>
                    @endif
                </td>
                <td>{{date('d F Y',strtotime($row->created_at))}}</td>
            </tr>
          @endforeach
        </tbody>
    </table>
</html>