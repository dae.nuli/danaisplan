@extends($template)

@section('body-content')
<div class="row">
<!-- left column -->
    <div class="col-md-12">
     <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-user"></i> Biodata Pengusul</h3>
            </div><!-- /.box-header -->
       <!-- form start -->
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Nama</label>
                            <input class="form-control" value="{{$detail->name}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Email</label>
                            <input class="form-control" value="{{$detail->email}}" disabled="">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>No. Telefon</label>
                            <input class="form-control" value="{{$detail->phone}}" disabled="">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Alamat</label>
                    <input disabled="" value="{{$detail->address}}" class="form-control">
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-tag"></i> Usulan</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Kabupaten</label>
                            <input disabled="" value="{{$detail->kabupaten->nama}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Kecamatan</label>
                            <input disabled="" value="{{$detail->kecamatan->nama}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Desa</label>
                            <input disabled="" value="{{$detail->desa->nama}}" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Usulan</label>
                    <textarea disabled="" rows="5" class="form-control">{{$detail->proposal}}</textarea>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
{{-- </div>
<div class="row"> --}}
    <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-tag"></i> Usulan</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <label>Ditujukan Pada</label>
                    <input class="form-control" value="{{isset($detail->id_skpd)?$detail->skpd->instansi_name:''}}" disabled="">
                </div>
                <div class="form-group">
                    <label>Catatan</label>
                    <textarea disabled="" class="form-control" rows="10">{{$detail->note}}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
