@extends($template)
 
@section('body-content')
{{-- @if(session()->has('success')) --}}
{{--     <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> --}}
    {{-- <h4><i class="icon fa fa-check"></i></h4> --}}
{{--     {{session()->get('success')}}
    </div> --}}
{{-- @endif --}}
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr class="text-uppercase">
            <th>#</th>
            <th>Kecamatan</th>
            <th>desa</th>
            <th>latitude</th>
            <th>longitude</th>
            <th></th>
          </tr>
         <?php 
         if($pages){
             $page = $pages;
         }else{
             $page = 1;
         }
         $nomor = $page + ($page-1) * ($limit-1);
         ?>
          @foreach($index as $row)
          <tr>
            <td>{{$nomor++}}</td>
            <td>{{$row->kecamatan->nama}}</td>
            <td>{{$row->nama}}</td>
            <td>{{$row->latitude}}</td>
            <td>{{$row->longitude}}</td>
            <td>
              @if($pages)
                <a href="{{url($uri.'/edit/'.$row->id.'?page='.$pages)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> {{trans('button.ed')}}</a>
              @else
                <a href="{{url($uri.'/edit/'.$row->id)}}" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i> {{trans('button.ed')}}</a>
              @endif
            </td>
          </tr>
          @endforeach
        </table>
      </div><!-- /.box-body -->
      <div class="box-footer clearfix">
         {!! $index->links() !!}
      </div>
    </div><!-- /.box -->
</div>
@endsection

@section('end-script')
  @parent
<script type="text/javascript">
@if(session()->has('success'))
var n = noty({
    layout:'topRight',
    theme:'relax',
    text: '{{session()->get("success")}}',
    animation: {
        open  : 'animated fadeIn',
        close : 'animated fadeOut',
        easing: 'swing', // easing
        speed: 500 // opening & closing animation speed
    },
    type: 'success'
});
n.setTimeout(3000);
@endif
</script>
@endsection