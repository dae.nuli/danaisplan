@extends($template)

@section('end-script')
    @parent

    <script src="{{asset('js/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
        form : '#request',
        onSuccess : function() {
            waiting();
        }
    });
    </script>
@endsection

@section('body-content')
@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row">
<!-- left column -->
    <div class="col-md-12">
     <!-- general form elements -->
        <div class="box box-primary">
            {!! Form::open(array('url' => $action,'method'=>'POST','id'=>'request')) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama Kecamatan</label>
                                <input type="text" value="{{isset($index)?$index->kecamatan->nama:''}}" autocomplete="off" class="form-control" data-validation="required" data-validation-error-msg="This field is required.">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nama Desa</label>
                                <input type="text" name="name" value="{{isset($index)?$index->nama:''}}" autocomplete="off" class="form-control" data-validation="required" data-validation-error-msg="This field is required.">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Latitude</label>
                                <input name="lat" class="form-control" data-validation="required" autocomplete="off" data-validation-error-msg="This field is required." value="{{isset($index)?$index->latitude:''}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Longitude</label>
                                <input name="lon" class="form-control" data-validation="required" autocomplete="off" data-validation-error-msg="This field is required." value="{{isset($index)?$index->longitude:''}}">
                            </div>
                        </div>
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    @if($pages)
                        <a href="{{url($uri.'?page='.$pages)}}" class="btn btn-warning">{{trans('button.bac')}}</a>
                    @else
                        <a href="{{url($uri)}}" class="btn btn-warning">{{trans('button.bac')}}</a>
                    @endif
                    <button type="submit" class="btn btn-primary">{{trans('button.sub')}}</button>
                </div>
            {!! Form::close() !!}
        </div><!-- /.box -->
    </div>
</div>
@endsection
