@extends($template)

@section('end-script')
    @parent

    <script src="{{asset('js/jquery.blockUI.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/jquery.form-validator.min.js')}}"></script>
    <script type="text/javascript">
    $.validate({
        form : '#request',
        // modules : 'security',
        onSuccess : function() {
            waiting();
        },
        // onModulesLoaded : function() {
        //     var optionalConfig = {
        //     fontSize: '12pt',
        //     padding: '4px',
        //     bad : 'Very bad',
        //     weak : 'Weak',
        //     good : 'Good',
        //     strong : 'Strong'
        // };

        // $('input[name="pass"]').displayPasswordStrength(optionalConfig);
      // }
    });

    @if(session()->has('success'))
    var n = noty({
        layout:'topRight',
        theme:'relax',
        text: '{{session()->get("success")}}',
        animation: {
            open  : 'animated fadeIn',
            close : 'animated fadeOut',
            easing: 'swing', // easing
            speed: 500 // opening & closing animation speed
        },
        type: 'success'
    });
    n.setTimeout(3000);
    @endif

    </script>
@endsection

@section('body-content')

<div class="row">
    <div class="col-md-12">
        {!! Form::open(array('url' => $action,'method'=>'POST','id'=>'request')) !!}
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="name" autocomplete="off" value="{{isset($index)?$index->name:''}}" class="form-control" data-validation="required" data-validation-error-msg="This field is required.">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Email</label>
                            <input disabled="" value="{{isset($index)?$index->email:''}}" class="form-control">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Nomor Telefon</label>
                            <input type="text" name="phone" autocomplete="off" value="{{isset($index)?$index->phone:''}}" class="form-control" data-validation="number">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" {{!isset($index)?' disabled':''}} name="password" class="form-control" data-validation="length" data-validation-length="min6" data-validation-optional="true">
                        </div>
                        <small class="help-block"> {{!isset($index)?'':'Kosongkan apabila tidak diubah.'}}</small>
                    </div>
                    <div class="col-xs-8">
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" name="address" autocomplete="off" value="{{isset($index)?$index->address:''}}" class="form-control">
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="box-footer">
            <a href="{{url($uri)}}" class="btn btn-warning">{{trans('button.bac')}}</a>
            <button type="submit" class="btn btn-primary">{{trans('button.sub')}}</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
