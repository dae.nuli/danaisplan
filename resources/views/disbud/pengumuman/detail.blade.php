@extends($template)

@section('body-content')
<div class="row">
<!-- left column -->
    <div class="col-md-12">
     <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group">
                            <label>Content</label>
                            <textarea class="form-control" rows="10" disabled="">{{isset($index)?$index->content:''}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Berakhir</label>
                            <input disabled="" value="{{isset($index)?date('Y-m-d',strtotime($index->end_show)):''}}" class="form-control">
                        </div>
                    </div>
                </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
                <a href="{{url($uri)}}" class="btn btn-warning">{{trans('button.bac')}}</a>
            </div>
        </div><!-- /.box -->
    </div>
</div>
@endsection
