<?php 
$prefix = 'disbud';
$uri = Request::segment(2);
$us  = Helper::count_usulan_skpd();
$um  = Helper::count_usulan_masyarakat();
?>
<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>
  <li {!!($uri=='home')?'class="active"':''!!}>
    <a href="{{url($prefix.'/home')}}">
      <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
  </li>

  <li {!!($uri=='usulan')?'class="active"':''!!}>
    <a href="{{url($prefix.'/usulan')}}">
      <i class="fa fa-files-o"></i> <span>Usulan Skpd</span>
      {!!($us)?'<small class="label pull-right bg-yellow">'.$us.'</small>':''!!}
    </a>
  </li>
  <li {!!($uri=='masyarakat')?'class="active"':''!!}>
    <a href="{{url($prefix.'/masyarakat')}}">
      <i class="fa fa-book"></i> <span>Usulan Masyarakat</span>
      {!!($um)?'<small class="label pull-right bg-yellow">'.$um.'</small>':''!!}
    </a>
  </li>
  <li {!!($uri=='program')?'class="active"':''!!}>
    <a href="{{url($prefix.'/program')}}">
      <i class="fa fa-list-ol"></i> <span>Program & Kegiatan</span>
    </a>
  </li>
  <li {!!($uri=='pengumuman')?'class="active"':''!!}>
    <a href="{{url($prefix.'/pengumuman')}}">
      <i class="fa fa-bullhorn"></i> <span>Pengumuman</span>
    </a>
  </li>
  <li class="treeview {!!($uri=='report'||$uri=='reportMasyarakat')?'active':''!!}">
    <a href="#">
      <i class="fa fa-file-text-o"></i> <span>Laporan</span>
      <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
      <li {!!($uri=='report')?'class="active"':''!!}><a href="{{url($prefix.'/report')}}"><i class="fa fa-circle-o"></i> Usulan SKPD</a></li>
      <li {!!($uri=='reportMasyarakat')?'class="active"':''!!}><a href="{{url($prefix.'/reportMasyarakat')}}"><i class="fa fa-circle-o"></i> Usulan Masyarakat</a></li>
    </ul>
  </li>
  <li {!!($uri=='skpd')?'class="active"':''!!}>
    <a href="{{url($prefix.'/skpd')}}">
      <i class="fa fa-users"></i> <span>SKPD</span>
    </a>
  </li>
  <li {!!($uri=='users')?'class="active"':''!!}>
    <a href="{{url($prefix.'/users')}}">
      <i class="fa fa-users"></i> <span>Users</span>
    </a>
  </li>
{{--   <li>
    <a href="pages/mailbox/mailbox.html">
      <i class="fa fa-envelope"></i> <span>Mailbox</span>
      <small class="label pull-right bg-yellow">12</small>
    </a>
  </li> --}}
  <li class="header">LABELS</li>
  <li><a href="{{url($prefix.'/prioritas')}}"><i class="fa fa-circle-o text-red"></i> <span>Prioritas</span></a></li>
  <li><a href="{{url($prefix.'/perdais')}}"><i class="fa fa-circle-o text-red"></i> <span>Perdais</span></a></li>
  <li><a href="{{url($prefix.'/desa')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Desa</span></a></li>
</ul>