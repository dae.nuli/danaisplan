@extends('disbud.layout.base')

@section('head-script')
    <!-- Bootstrap 3.3.4 -->
    <link href="{{asset('adminlte2/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="{{asset('adminlte2/font-awesome-4.4.0/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="{{asset('adminlte2/ionicons-2.0.1/css/ionicons.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{asset('adminlte2/dist/css/AdminLTE.css')}}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="{{asset('adminlte2/dist/css/skins/_all-skins.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/animate.css')}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
@endsection

@section('header')
<header class="main-header">

  <!-- Logo -->
  <a href="index2.html" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>A</b>LT</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Admin</b>LTE</span>
  </a>

  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        {{-- @include('disbud.layout.part.message')                   --}}
        
        <!-- Notifications: style can be found in dropdown.less -->
        {{-- @include('disbud.layout.part.notif')                   --}}
        
        <!-- Tasks: style can be found in dropdown.less -->
        {{-- @include('disbud.layout.part.task')                   --}}
        
        <!-- User Account: style can be found in dropdown.less -->
        @include('disbud.layout.part.user')                  
        
        <!-- Control Sidebar Toggle Button -->
        {{-- <li>
          <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
        </li> --}}
      </ul>
    </div>

  </nav>
</header>
@endsection

@section('body')
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{asset('storage/avatar/disbud/'.session('loginDisbud')['avatar'])}}" class="img-circle" alt="User Image" />
      </div>
      <div class="pull-left info">
        <p>{{Str::words(session('loginDisbud')['name'],2,'')}}</p>

        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    @include('disbud.layout.part.search')                  

    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    @include('disbud.layout.part.menu')                  
    
  </section>
  <!-- /.sidebar -->
</aside>
<!-- Full Width Column -->
<div class="content-wrapper">
  {{-- <div class=""> --}}
  {{-- <div class="container"> --}}
    <!-- Content Header (Page header) -->
    <section class="content-header">
          @yield('header-content')
      <h1>
        {{(isset($title)?$title:'')}}
        {{-- <small>{{$path}}</small> --}}
      </h1>
      {{-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol> --}}
      {{-- <div class="clearfix"></div> --}}
    </section>

    <!-- Main content -->
    <section class="content">
          @yield('body-content')
    </section><!-- /.content -->
  {{-- </div>/.container --}}
</div><!-- /.content-wrapper -->
@endsection

@section('footer')
{{--     <footer class="main-footer">
        <div class="container">
            <div class="pull-right hidden-xs">
                <b>Version</b> 2.0
        </div>
            <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
        </div><!-- /.container -->
    </footer> --}}
@endsection

@section('end-script')
    <!-- jQuery 2.1.4 -->
    <script src="{{asset('adminlte2/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="{{asset('adminlte2/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="{{asset('adminlte2/plugins/slimScroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='{{asset("adminlte2/plugins/fastclick/fastclick.min.js")}}'></script>
    <!-- AdminLTE App -->
    <script src="{{asset('adminlte2/dist/js/app.min.js')}}" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('adminlte2/dist/js/demo.js')}}" type="text/javascript"></script>

    <script type="text/javascript" src="{{asset('js/jquery.noty.packaged.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/relax.js')}}"></script>
@endsection