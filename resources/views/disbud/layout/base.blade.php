<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Disbud {{isset($title)?'| '.$title:''}}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @yield('head-script')
    </head>
    <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
    <body class="skin-red-light fixed sidebar-mini">
        <div class="wrapper">
            @yield('header')
            @yield('body')
            @yield('footer')
        </div><!-- ./wrapper -->

        @yield('end-script')
  </body>
</html>
