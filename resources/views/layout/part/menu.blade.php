<?php 
$uri = Request::segment(2);
$um = Helper::usulan_masyarakat_for_skpd(session('loginskpd')['id']);
?>
<ul class="sidebar-menu">
  <li class="header">MAIN NAVIGATION</li>
  <li {!!($uri=='home')?'class="active"':''!!}>
    <a href="{{url('skpd/home')}}">
      <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
  </li>

  <li {!!($uri=='usulan')?'class="active"':''!!}>
    <a href="{{url('skpd/usulan')}}">
      <i class="fa fa-files-o"></i> <span>Usulan Dana Istimewa</span>
    </a>
  </li>
  <li {!!($uri=='masyarakat')?'class="active"':''!!}>
    <a href="{{url('skpd/masyarakat')}}">
      <i class="fa fa-book"></i> <span>Usulan Masyarakat</span>
      {!!($um)?'<small class="label pull-right bg-yellow">'.$um.'</small>':''!!}
    </a>
  </li>
{{--   <li>
    <a href="pages/mailbox/mailbox.html">
      <i class="fa fa-envelope"></i> <span>Mailbox</span>
      <small class="label pull-right bg-yellow">12</small>
    </a>
  </li> --}}
{{--   <li class="header">LABELS</li>
  <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
  <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
  <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> --}}
</ul>