<li class="dropdown user user-menu">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <img src="{{asset('storage/avatar/skpd/'.session('loginskpd')['avatar'])}}" class="user-image" alt="User Image"/>
    <span class="hidden-xs">{{session('loginskpd')['name']}}</span>
  </a>
  <ul class="dropdown-menu">
    <!-- User image -->
    <li class="user-header">
      <img src="{{asset('storage/avatar/skpd/'.session('loginskpd')['avatar'])}}" class="img-circle" alt="User Image" />
      <p>
        {{Str::words(session('loginskpd')['name'],2,'')}} - Web Developer
        <small>Member since {{date('M. Y',strtotime(session('loginskpd')['created_at']))}}</small>
      </p>
    </li>
    <!-- Menu Body -->
{{--     <li class="user-body">
      <div class="col-xs-4 text-center">
        <a href="#">Followers</a>
      </div>
      <div class="col-xs-4 text-center">
        <a href="#">Sales</a>
      </div>
      <div class="col-xs-4 text-center">
        <a href="#">Friends</a>
      </div>
    </li> --}}
    <!-- Menu Footer-->
    <li class="user-footer">
      <div class="pull-left">
        <a href="{{url('skpd/profile/data')}}" class="btn btn-default btn-flat">Profile</a>
      </div>
      <div class="pull-right">
        <a href="{{url('skpd/logout')}}" class="btn btn-default btn-flat">Sign out</a>
      </div>
    </li>
  </ul>
</li>