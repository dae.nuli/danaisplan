-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 17, 2018 at 06:39 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `danaisplan_2`
--

-- --------------------------------------------------------

--
-- Table structure for table `disbud`
--

CREATE TABLE `disbud` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `level` int(11) DEFAULT NULL,
  `avatar` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `disbud`
--

INSERT INTO `disbud` (`id`, `name`, `email`, `password`, `address`, `phone`, `is_active`, `level`, `avatar`, `deleted_at`, `created_at`, `updated_at`) VALUES
('', 'Dinas Kebudayaan', 'admin@admin.com', '$2y$10$TfWQaV2/mxEAWIE9R8fCguO2AmxxSVEqzhUqIaIS0X0vom01gAnG.', 'Jl. Kaliurang', '', '1', 1, NULL, NULL, '2017-11-06 07:11:03', NULL),
('3d099c90843244a28c14fa5931966aeb', 'Dinas Kebudayaan', 'perencanaandisbuddiy@gmail.com', '$2y$10$k64xt5PWNbNIcX/faMwvVu./FTy/fXidE6bUic6kvXkrEzkhH08HC', 'JL Cendana', '081328785716', '1', NULL, 'mLtCGEgp0ZerMBfsliB4.png', NULL, '2017-11-06 08:57:33', '2017-11-06 08:57:33'),
('56251560dbf147b59165a826dd47a3aa', 'firman', 'firmansyah16@gmail.com', '$2y$10$ioGV3zdx6T5SfNlYJtooMevLzxk1qnpr/1gKHBzUAnhBlNV/V0NyG', 'jalan bulak', '087878778787', '1', NULL, 'oIOrcA1wEaEUtQ233sNq.png', NULL, '2017-11-06 09:06:43', '2017-11-06 09:06:43');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_program` int(11) NOT NULL,
  `code_kegiatan` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kegiatan`
--

INSERT INTO `kegiatan` (`id`, `id_program`, `code_kegiatan`, `name`, `description`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, '3.03.55.001', 'Pembinaan Desa Budaya', NULL, NULL, NULL, NULL),
(2, 1, '3.03.55.003', 'Pembinaan dan Pengembangan Seni Rupa Daerah', NULL, NULL, NULL, NULL),
(3, 1, '3.03.55.004', 'Pembinaan dan Pengembangan Perfilman', NULL, NULL, NULL, NULL),
(4, 1, '3.03.55.005', 'Pengadaaan Sarana dan Prasarana Kesenian ke Masyarakat', NULL, NULL, NULL, NULL),
(5, 1, '3.03.55.008', 'Promosi dan Publikasi Seni Budaya', NULL, NULL, NULL, NULL),
(6, 1, '3.03.55.009', 'Penyelenggaraan Even Lembaga Penggiat Seni', NULL, NULL, NULL, NULL),
(7, 1, '3.03.55.010', 'Gelar Budaya Jogja', NULL, NULL, NULL, NULL),
(8, 1, '3.03.55.011', 'Jogja International Heritage Festival', NULL, NULL, NULL, NULL),
(9, 1, '3.03.55.012', 'Festival Kebudayaan Yogyakarta', NULL, NULL, NULL, NULL),
(10, 2, '3.03.56.001', 'Misi Kebudayaan ke Luar Negeri Dalam Rangka Diplomasi Budaya', NULL, NULL, NULL, NULL),
(11, 2, '3.03.56.002', 'Membangun Kemitraan Dengan Instansi di Luar DIY', NULL, NULL, NULL, NULL),
(12, 2, '3.03.56.003', 'Membangun Kemitraan dengan Lembaga Pelestari Budaya', NULL, NULL, NULL, NULL),
(13, 2, '3.03.56.004', 'Selendang Sutera (Semarak Legenda Suku-suku Nusantara)', NULL, NULL, NULL, NULL),
(14, 2, '3.03.56.005', 'Penghargaan Bagi Pelestari dan Penggiat Budaya', NULL, NULL, NULL, NULL),
(15, 2, '3.03.56.008', 'Perencanaan, Monitoring dan Evaluasi Program dan Kegiatan Keistimewaan Urusan Kebudayaan', NULL, NULL, NULL, NULL),
(16, 3, '3.03.57.001', 'Pembinaan dan Pengembangan Kesejarahan', NULL, NULL, NULL, NULL),
(17, 3, '3.03.57.002', 'Pembinaan dan Pengembangan Bahasa dan Sastra', NULL, NULL, NULL, NULL),
(18, 3, '3.03.57.003', 'Pelestarian, Pengembangan dan Aplikasi Nilai-nilai Luhur Budaya Luhur di Masyarakat', NULL, NULL, NULL, NULL),
(19, 3, '3.03.57.004', 'Pelestarian Kepercayaan dan Tradisi', NULL, NULL, NULL, NULL),
(20, 4, '3.03.58.001', 'Tata Kelola Warisan Budaya dan Cagar Budaya', NULL, NULL, NULL, NULL),
(21, 4, '3.03.58.002', 'Penguatan Lembaga Pengelola dan Pelestari Warisan Budaya', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `masa_login`
--

CREATE TABLE `masa_login` (
  `id` int(10) UNSIGNED NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `masa_login`
--

INSERT INTO `masa_login` (`id`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, '2017-11-06', '2017-11-30', '2017-11-06 07:11:03', '2017-11-06 08:58:41');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_04_30_071914_create_table_desa', 1),
('2016_04_30_071921_create_table_kabupaten', 1),
('2016_04_30_071929_create_table_kecamatan', 1),
('2016_04_30_071935_create_table_provinsi', 1),
('2016_04_30_071957_create_table_skpd', 1),
('2016_04_30_095036_create_table_program', 1),
('2016_04_30_095053_create_table_kegiatan', 1),
('2016_04_30_102157_create_table_disbud', 1),
('2016_04_30_102215_create_table_usulan_skpd', 1),
('2016_04_30_102225_create_table_usulan_masyarakat', 1),
('2016_05_12_101834_create_jobs_table', 1),
('2016_05_15_095713_create_table_usulan_skpd_detail', 1),
('2016_05_16_023952_create_table_prioritas', 1),
('2016_06_07_033841_create_perdais_table', 1),
('2016_06_13_150012_create_table_pengumuman', 1),
('2016_06_14_171440_create_table_masa_login', 1),
('2016_07_14_132945_add_column_years_usulan_masyarakat', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `end_show` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `perdais`
--

CREATE TABLE `perdais` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `perdais`
--

INSERT INTO `perdais` (`id`, `name`, `note`, `created_at`, `updated_at`) VALUES
(1, 'Perdais nomor', 'ini adalah keterangan', '2017-11-06 09:20:33', '2017-11-06 09:20:33');

-- --------------------------------------------------------

--
-- Table structure for table `prioritas`
--

CREATE TABLE `prioritas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `prioritas`
--

INSERT INTO `prioritas` (`id`, `name`, `note`, `created_at`, `updated_at`) VALUES
(1, 'penting', 'penting sekali', '2016-05-19 00:33:25', '2016-05-19 00:33:25');

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(10) UNSIGNED NOT NULL,
  `code_program` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `years` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `code_program`, `name`, `years`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '3.03.55', 'PROGRAM PENGEMBANGAN KESENIAN DAN BUDAYA DAERAH', '2017', NULL, NULL, NULL),
(2, '3.03.56', 'PROGRAM PROMOSI DAN KEMITRAAN BUDAYA DIY DI DALAM DAN LUAR NEGERI', '2017', NULL, NULL, NULL),
(3, '3.03.57', 'PROGRAM PENGELOLAAN NILAI DAN SEJARAH', '2017', NULL, NULL, NULL),
(4, '3.03.58', 'PROGRAM PENGELOLAAN CAGAR BUDAYA DAN WARISAN BUDAYA', '2017', NULL, NULL, NULL),
(5, '3.03.59', 'PROGRAM PEMBINAAN DAN PENGEMBANGAN MUSEUM', '2017', NULL, NULL, NULL),
(6, '3.03.60', 'PROGRAM PENINGKATAN SARANA DAN PRASARANA APARATUR', '2017', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `skpd`
--

CREATE TABLE `skpd` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `instansi_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email_staff` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personil_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `is_complete` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `activation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `skpd`
--

INSERT INTO `skpd` (`id`, `code`, `instansi_name`, `email`, `email_staff`, `username`, `phone`, `address`, `personil_name`, `password`, `is_active`, `is_complete`, `activation`, `avatar`, `deleted_at`, `created_at`, `updated_at`) VALUES
('04b10086e21845648483da294861d259', '17', 'Dinas Kebudayaan DIY', 'perencanaandisbuddiy@gmail.com', 'nrahmanto@gmail.com', 'perencanaandisbuddiy', '081328785716', 'JL Cendana 11', 'Nur Ikhwan Rahmanto', '$2y$10$TfWQaV2/mxEAWIE9R8fCguO2AmxxSVEqzhUqIaIS0X0vom01gAnG.', '1', '2', NULL, 'Qn0b5wTmcklXtOAC2Gku.png', NULL, '2017-11-06 08:59:54', '2017-11-06 09:11:29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `usulan_masyarakat`
--

CREATE TABLE `usulan_masyarakat` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `id_skpd` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_kabupaten` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `id_kecamatan` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `id_desa` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `proposal` text COLLATE utf8_unicode_ci NOT NULL,
  `document` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `assign` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `years` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usulan_masyarakat`
--

INSERT INTO `usulan_masyarakat` (`id`, `id_skpd`, `id_kabupaten`, `id_kecamatan`, `id_desa`, `latitude`, `longitude`, `name`, `email`, `address`, `phone`, `proposal`, `document`, `status`, `assign`, `deleted_at`, `created_at`, `updated_at`, `years`) VALUES
('2eef21d321654a4396765f7927bcfca3', '04b10086e21845648483da294861d259', '3401', '3401120', '3401120007', 'NaN', 'NaN', 'Alexander', 'jjjjjj@yahii.com', 'Boyosari. Ponjor, kulonprogo', '087838945863', 'Desa budaya ', NULL, '2', '1', NULL, '2017-11-06 10:17:07', '2017-11-06 10:23:25', '2018'),
('64f3af5b7441407e874f616e68c5a483', '04b10086e21845648483da294861d259', '3402', '3402130', '3402130001', 'NaN', 'NaN', 'Joy', 'joy@gmail.com', 'Bantul', '081812345678', 'Pengadaan alat band', NULL, '0', '0', NULL, '2017-11-06 10:06:16', '2017-11-06 10:06:16', '2018'),
('787e03c29e414faeb68027d66eeccec7', '04b10086e21845648483da294861d259', '3404', '3404120', '3404120005', 'NaN', 'NaN', 'Nanang dwi aryanto', 'nanankmartosuwito@gmail.com', 'Padukuhan Dayakan Sardonoharjo Ngaglik Sleman', '082231773172', 'Gelar Budaya Merti Desa', NULL, '0', '0', NULL, '2017-11-06 10:07:26', '2017-11-06 10:07:26', '2018'),
('7f4b162fc12b43379ce182c10d986bab', '04b10086e21845648483da294861d259', '3404', '3404110', '3404110004', 'NaN', 'NaN', 'wahyudi', 'yudi.arsipjogja@ymail.com', 'Jl.Tentara rakyat mataran no.29 Yk', '08174116958', 'Lapangan Voly', NULL, '0', '0', NULL, '2017-11-06 10:06:11', '2017-11-06 10:06:11', '2018'),
('c1b2d4763d16467e8498dfb5f781b480', '04b10086e21845648483da294861d259', '3402', '3402150', '3402150003', 'NaN', 'NaN', 'Vishnu Coba', 'mahavishnu.58@gmail.com', 'Kasihan Bantul Yoyakarta', '087876186188', 'Usulan program 2025', NULL, '0', '0', NULL, '2017-11-06 10:05:30', '2017-11-06 10:05:30', '2018'),
('fce038087718491882b1c770a9c0f82c', '04b10086e21845648483da294861d259', '3403', '3403120', '3403120004', 'NaN', 'NaN', 'Legino', 'gelasbening@gmail.com', 'Kulon progo', '123456788', 'Perangkat gamelan', NULL, '0', '0', NULL, '2017-11-06 10:04:31', '2017-11-06 10:04:31', '2018');

-- --------------------------------------------------------

--
-- Table structure for table `usulan_skpd`
--

CREATE TABLE `usulan_skpd` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `id_skpd` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_program` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `years` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `tor` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rab` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perdais` int(11) NOT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `revision_note` text COLLATE utf8_unicode_ci,
  `is_approved` enum('0','1','2','3','4') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usulan_skpd`
--

INSERT INTO `usulan_skpd` (`id`, `id_skpd`, `id_program`, `id_kegiatan`, `years`, `tor`, `rab`, `perdais`, `note`, `revision_note`, `is_approved`, `deleted_at`, `created_at`, `updated_at`) VALUES
('07f26ed0e16f4f14ae1fd12e0f8e2dd9', '04b10086e21845648483da294861d259', 1, 1, '2017', NULL, NULL, 1, NULL, NULL, '0', NULL, '2017-11-06 10:12:03', '2017-11-06 10:12:03'),
('39c61309560e4189bc2f934b5a3100b6', '04b10086e21845648483da294861d259', 2, 11, '2017', NULL, NULL, 1, NULL, NULL, '0', NULL, '2017-11-06 10:14:36', '2017-11-06 10:14:36'),
('60650d37915a4e6183bb57be14ee0846', '04b10086e21845648483da294861d259', 2, 11, '2017', '3453780.pdf', '1581967.pdf', 1, NULL, NULL, '0', NULL, '2017-11-06 10:15:10', '2017-11-06 10:15:10'),
('66b4c8d77a4d42d7ac05c930a1a633c1', '04b10086e21845648483da294861d259', 4, 20, '2017', NULL, NULL, 1, NULL, NULL, '0', NULL, '2017-11-06 10:19:08', '2017-11-06 10:19:08'),
('683701754dd14ca9861581a575ba0b46', '04b10086e21845648483da294861d259', 3, 18, '2017', NULL, NULL, 1, NULL, NULL, '0', NULL, '2017-11-06 10:15:36', '2017-11-06 10:15:36'),
('702bfeddcdf540b9a2895d0a41af012c', '04b10086e21845648483da294861d259', 1, 2, '2017', NULL, NULL, 1, NULL, NULL, '0', NULL, '2017-11-06 10:13:09', '2017-11-06 10:13:09'),
('7b9087dabcaf4514b6d973b80a74acfd', '04b10086e21845648483da294861d259', 3, 16, '2017', NULL, NULL, 1, NULL, NULL, '0', NULL, '2017-11-06 10:17:22', '2017-11-06 10:17:22'),
('b2c83e77cb8645fc88e1f008e2091a2d', '04b10086e21845648483da294861d259', 1, 1, '2017', NULL, NULL, 1, NULL, NULL, '0', NULL, '2017-11-06 10:20:58', '2017-11-06 10:20:58'),
('b518e26290b6473e842b5c3ebb18e9c8', '04b10086e21845648483da294861d259', 1, 4, '2017', NULL, NULL, 1, NULL, NULL, '0', NULL, '2017-11-06 10:19:51', '2017-11-06 10:19:51'),
('b5a62de69d8a4cb28c402a03792c38e5', '04b10086e21845648483da294861d259', 1, 1, '2017', NULL, NULL, 1, NULL, NULL, '0', NULL, '2017-11-06 10:15:38', '2017-11-06 10:15:38'),
('ba2d4f9355434a4882ef8f66e524ef75', '04b10086e21845648483da294861d259', 1, 6, '2017', NULL, NULL, 1, NULL, 'Lampiran kurang lengkap, sertakan foto dan keterangan kondisi saat ini', '1', NULL, '2017-11-06 10:15:19', '2017-11-06 10:18:20'),
('e97a2c9bd9e34cdbbe4db00a1cb98e79', '04b10086e21845648483da294861d259', 2, 11, '2017', '8052088.pdf', '8977941.pdf', 1, NULL, NULL, '0', NULL, '2017-11-06 09:32:58', '2017-11-06 09:32:58'),
('f7cfc1bd049c4f00af631899ff67d0ee', '04b10086e21845648483da294861d259', 2, 13, '2017', NULL, NULL, 1, NULL, NULL, '0', NULL, '2017-11-06 10:16:10', '2017-11-06 10:16:10');

-- --------------------------------------------------------

--
-- Table structure for table `usulan_skpd_detail`
--

CREATE TABLE `usulan_skpd_detail` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `id_usulan` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `kabupaten_id` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `kecamatan_id` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `desa_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tolak_ukur_kinerja` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah_target_kinerja` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `satuan_target_kinerja` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `anggaran` int(11) NOT NULL,
  `prioritas` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usulan_skpd_detail`
--

INSERT INTO `usulan_skpd_detail` (`id`, `id_usulan`, `kabupaten_id`, `kecamatan_id`, `desa_id`, `latitude`, `longitude`, `tolak_ukur_kinerja`, `jumlah_target_kinerja`, `satuan_target_kinerja`, `anggaran`, `prioritas`, `created_at`, `updated_at`) VALUES
('0502ec746a6c4c5bbc40886a926c63b8', 'b5a62de69d8a4cb28c402a03792c38e5', '3471', '3471130', '3471130002', '-7.787014115401992', '110.36384369661575', 'lalala land', '70', '50', 25000, 1, '2017-11-06 10:15:38', '2017-11-06 10:15:38'),
('0bcafb9c59974899b4045f8418ccedb4', '683701754dd14ca9861581a575ba0b46', '3471', '3471130', '3471130001', '-7.788041925008726', '110.36046231644937', 'Meningkatnya Kualitas Kebersihan Sungai', '100', '%', 1724000000, 1, '2017-11-06 10:15:36', '2017-11-06 10:15:36'),
('2988f1cf949b4fdbb15a12c90712b73a', 'e97a2c9bd9e34cdbbe4db00a1cb98e79', '3471', '3471140', '3471140004', '-7.773614', '110.364394', 'Desa budaya', '100', 'desa', 1000000000, 1, '2017-11-06 09:32:58', '2017-11-06 09:32:58'),
('507d03c2cc2f472d8fd54fdf2d0699b8', 'b2c83e77cb8645fc88e1f008e2091a2d', '3402', '3402080', '3402080004', 'NaN', 'NaN', 'menggiatkan kesenian macapat', '50', 'org', 100000000, 1, '2017-11-06 10:20:58', '2017-11-06 10:20:58'),
('6c8c2eeb72df4c97b9ac0a753f6f2180', '60650d37915a4e6183bb57be14ee0846', '3403', '3403090', '3403090010', 'NaN', 'NaN', 'Terlaksananya promosi ke papua', '1', 'tim seni', 400000000, 1, '2017-11-06 10:15:10', '2017-11-06 10:15:10'),
('6ee879f09f53407aa3067c158ce41505', '702bfeddcdf540b9a2895d0a41af012c', '3404', '3404110', '3404110004', 'NaN', 'NaN', 'WLDKOWQJWEO[QFJHOWQ', '12', 'LEMBAR', 3000, 1, '2017-11-06 10:13:09', '2017-11-06 10:13:09'),
('a50b88a09f214ed097a0e621858008f5', '39c61309560e4189bc2f934b5a3100b6', '3471', '3471050', '3471050002', '-7.82940752862644', '110.40332424455414', 'Promosi Budaya Jogja ke Pandemen Jogja yang berada di Luar Daerah', '3', 'kali', 800000000, 1, '2017-11-06 10:14:36', '2017-11-06 10:14:36'),
('a9d1e203ddfb47209a42eaa8491ae819', 'f7cfc1bd049c4f00af631899ff67d0ee', '3471', '3471040', '3471040007', '-7.796478706336887', '110.38205594347006', 'Karnaval Budaya Nusantara', '34', 'Provinsi', 500000000, 1, '2017-11-06 10:16:10', '2017-11-06 10:16:10'),
('b30cc1bd23114006a11b0bd8f00e1b47', '7b9087dabcaf4514b6d973b80a74acfd', '3471', '3471020', '3471020002', '-7.808194705582411', '110.36831772950484', 'jumlah arsip yang diselamatkan', '1000', 'arsip', 500000000, 1, '2017-11-06 10:17:22', '2017-11-06 10:17:22'),
('d67b0f34337240d38c1238c60622db13', 'ba2d4f9355434a4882ef8f66e524ef75', '3404', '3404120', '3404120005', 'NaN', 'NaN', 'Meningkatnya event wisata di DIY', '1000', 'orang', 100000000, 1, '2017-11-06 10:15:19', '2017-11-06 10:15:19'),
('e7b88ee2284249ac8fcf844f54c41e59', '66b4c8d77a4d42d7ac05c930a1a633c1', '3471', '3471050', '3471050003', '-7.810160205705539', '110.4006030284751', 'Terkelolanya cagar budaya dengan baik', '5', 'gedung peninggalan belanda', 1250000000, 1, '2017-11-06 10:19:08', '2017-11-06 10:19:08'),
('e965948ebdca43808d6c511e81a1ce87', '07f26ed0e16f4f14ae1fd12e0f8e2dd9', '3402', '3402110', '3402110002', 'NaN', 'NaN', 'desa budaya maju', '21', 'desa', 500000000, 1, '2017-11-06 10:12:03', '2017-11-06 10:12:03'),
('f9c30871f5fc44cc8391ff4812cc6428', 'b518e26290b6473e842b5c3ebb18e9c8', '3401', '3401050', '3401050006', 'NaN', 'NaN', 'Meningkatnya Kesenian Jatilan di Kecamatan Lendah', '60', 'Orang', 50000000, 1, '2017-11-06 10:19:51', '2017-11-06 10:19:51');

-- --------------------------------------------------------

--
-- Table structure for table `wilayah_desa`
--

CREATE TABLE `wilayah_desa` (
  `id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `kecamatan_id` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wilayah_desa`
--

INSERT INTO `wilayah_desa` (`id`, `kecamatan_id`, `nama`, `latitude`, `longitude`) VALUES
('3401010001', '3401010', 'Jangkaran', '-7.896026', '110.037155'),
('3401010002', '3401010', 'Sindutan', '-7.880042', '110.048083'),
('3401010003', '3401010', 'Palihan', '-7.892757', '110.054747'),
('3401010004', '3401010', 'Glagah', '-7.903997', '110.070056'),
('3401010005', '3401010', 'Kali Dengen', '-7.893955', '110.085350'),
('3401010006', '3401010', 'Plumbon', '-7.901041', '110.100729'),
('3401010007', '3401010', 'Kedundang', '-7.878827', '110.098566'),
('3401010008', '3401010', 'Demen', '-7.887662', '110.096003'),
('3401010009', '3401010', 'Kulur', '-7.873611', '110.101545'),
('3401010010', '3401010', 'Kaligintung', '-7.875418', '110.091122'),
('3401010011', '3401010', 'Temon Wetan', '-7.875488', '110.083008'),
('3401010012', '3401010', 'Temon Kulon', '-7.886478', '110.076233'),
('3401010013', '3401010', 'Kebonrejo', '-7.884341', '110.067299'),
('3401010014', '3401010', 'Janten', '-7.876748', '110.064322'),
('3401010015', '3401010', 'Karang Wuluh', '-7.874611', '110.055388'),
('3401020001', '3401020', 'Karang Wuni', '-7.917626', '110.098567'),
('3401020002', '3401020', 'Sogan', '-7.898818', '110.113457'),
('3401020003', '3401020', 'Kulwaru', '-7.902168', '110.123880'),
('3401020004', '3401020', 'Ngestiharjo', '-7.903087', '110.131325'),
('3401020005', '3401020', 'Triharjo', '-7.882151', '110.137281'),
('3401020006', '3401020', 'Bendungan', '-7.891875', '110.149194'),
('3401020007', '3401020', 'Giri Peni', '-7.875509', '110.167063'),
('3401020008', '3401020', 'Wates', '-7.870649', '110.161106'),
('3401030001', '3401030', 'Garongan', '-7.891031', '110.129386'),
('3401030002', '3401030', 'Pleret', '-7.820815', '110.393887'),
('3401030003', '3401030', 'Bugel', NULL, NULL),
('3401030004', '3401030', 'Kanoman', NULL, NULL),
('3401030005', '3401030', 'Depok', NULL, NULL),
('3401030006', '3401030', 'Bojong', NULL, NULL),
('3401030007', '3401030', 'Tayuban', NULL, NULL),
('3401030008', '3401030', 'Gotakan', NULL, NULL),
('3401030009', '3401030', 'Panjatan', NULL, NULL),
('3401030010', '3401030', 'Cerme', NULL, NULL),
('3401030011', '3401030', 'Krembangan', NULL, NULL),
('3401040001', '3401040', 'Karang Sewu', NULL, NULL),
('3401040002', '3401040', 'Banaran', NULL, NULL),
('3401040003', '3401040', 'Kranggan', NULL, NULL),
('3401040004', '3401040', 'Nomporejo', NULL, NULL),
('3401040005', '3401040', 'Brosot', NULL, NULL),
('3401040006', '3401040', 'Pandowan', NULL, NULL),
('3401040007', '3401040', 'Tirta Rahayu', NULL, NULL),
('3401050001', '3401050', 'Wahyuharjo', NULL, NULL),
('3401050002', '3401050', 'Bumirejo', NULL, NULL),
('3401050003', '3401050', 'Jatirejo', NULL, NULL),
('3401050004', '3401050', 'Sidorejo', NULL, NULL),
('3401050005', '3401050', 'Gulurejo', NULL, NULL),
('3401050006', '3401050', 'Ngentakrejo', NULL, NULL),
('3401060001', '3401060', 'Demangrejo', NULL, NULL),
('3401060002', '3401060', 'Srikayangan', NULL, NULL),
('3401060003', '3401060', 'Tuksono', NULL, NULL),
('3401060004', '3401060', 'Salamrejo', NULL, NULL),
('3401060005', '3401060', 'Sukoreno', NULL, NULL),
('3401060006', '3401060', 'Kaliagung', NULL, NULL),
('3401060007', '3401060', 'Sentolo', NULL, NULL),
('3401060008', '3401060', 'Banguncipto', NULL, NULL),
('3401070001', '3401070', 'Tawangsari', NULL, NULL),
('3401070002', '3401070', 'Karangsari', NULL, NULL),
('3401070003', '3401070', 'Kedungsari', NULL, NULL),
('3401070004', '3401070', 'Margosari', NULL, NULL),
('3401070005', '3401070', 'Pengasih', NULL, NULL),
('3401070006', '3401070', 'Sendangsari', NULL, NULL),
('3401070007', '3401070', 'Sidomulyo', NULL, NULL),
('3401080001', '3401080', 'Hargomulyo', NULL, NULL),
('3401080002', '3401080', 'Hargorejo', NULL, NULL),
('3401080003', '3401080', 'Hargowilis', NULL, NULL),
('3401080004', '3401080', 'Kalirejo', NULL, NULL),
('3401080005', '3401080', 'Hargotirto', NULL, NULL),
('3401090001', '3401090', 'Jatimulyo', NULL, NULL),
('3401090002', '3401090', 'Giripurwo', NULL, NULL),
('3401090003', '3401090', 'Pendoworejo', NULL, NULL),
('3401090004', '3401090', 'Purwosari', NULL, NULL),
('3401100001', '3401100', 'Banyuroto', NULL, NULL),
('3401100002', '3401100', 'Donomulyo', NULL, NULL),
('3401100003', '3401100', 'Wijimulyo', NULL, NULL),
('3401100004', '3401100', 'Tanjungharjo', NULL, NULL),
('3401100005', '3401100', 'Jati Sarono', NULL, NULL),
('3401100006', '3401100', 'Kembang', NULL, NULL),
('3401110001', '3401110', 'Banjararum', NULL, NULL),
('3401110002', '3401110', 'Banjarasri', NULL, NULL),
('3401110003', '3401110', 'Banjarharjo', NULL, NULL),
('3401110004', '3401110', 'Banjaroyo', NULL, NULL),
('3401120001', '3401120', 'Kebon Harjo', '-7.679099', '110.165921'),
('3401120002', '3401120', 'Banjarsari', NULL, NULL),
('3401120003', '3401120', 'Purwoharjo', NULL, NULL),
('3401120004', '3401120', 'Sidoharjo', NULL, NULL),
('3401120005', '3401120', 'Gerbosari', NULL, NULL),
('3401120006', '3401120', 'Ngargosari', NULL, NULL),
('3401120007', '3401120', 'Pagerharjo', NULL, NULL),
('3402010001', '3402010', 'Poncosari', NULL, NULL),
('3402010002', '3402010', 'Trimurti', NULL, NULL),
('3402020001', '3402020', 'Gadingsari', NULL, NULL),
('3402020002', '3402020', 'Gadingharjo', NULL, NULL),
('3402020003', '3402020', 'Srigading', NULL, NULL),
('3402020004', '3402020', 'Murtigading', NULL, NULL),
('3402030001', '3402030', 'Tirtohargo', NULL, NULL),
('3402030002', '3402030', 'Parangtritis', NULL, NULL),
('3402030003', '3402030', 'Donotirto', NULL, NULL),
('3402030004', '3402030', 'Tirtosari', NULL, NULL),
('3402030005', '3402030', 'Tirtomulyo', NULL, NULL),
('3402040001', '3402040', 'Seloharjo', NULL, NULL),
('3402040002', '3402040', 'Panjangrejo', NULL, NULL),
('3402040003', '3402040', 'Srihardono', NULL, NULL),
('3402050001', '3402050', 'Sidomulyo', NULL, NULL),
('3402050002', '3402050', 'Mulyodadi', NULL, NULL),
('3402050003', '3402050', 'Sumbermulyo', NULL, NULL),
('3402060001', '3402060', 'Caturharjo', NULL, NULL),
('3402060002', '3402060', 'Triharjo', NULL, NULL),
('3402060003', '3402060', 'Gilangharjo', NULL, NULL),
('3402060004', '3402060', 'Wijirejo', NULL, NULL),
('3402070001', '3402070', 'Palbapang', NULL, NULL),
('3402070002', '3402070', 'Ringin Harjo', NULL, NULL),
('3402070003', '3402070', 'Bantul', NULL, NULL),
('3402070004', '3402070', 'Trirenggo', NULL, NULL),
('3402070005', '3402070', 'Sabdodadi', NULL, NULL),
('3402080001', '3402080', 'Patalan', NULL, NULL),
('3402080002', '3402080', 'Canden', NULL, NULL),
('3402080003', '3402080', 'Sumber Agung', NULL, NULL),
('3402080004', '3402080', 'Trimulyo', NULL, NULL),
('3402090001', '3402090', 'Selopamioro', NULL, NULL),
('3402090002', '3402090', 'Sriharjo', NULL, NULL),
('3402090003', '3402090', 'Kebon Agung', NULL, NULL),
('3402090004', '3402090', 'Karang Tengah', NULL, NULL),
('3402090005', '3402090', 'Girirejo', NULL, NULL),
('3402090006', '3402090', 'Karangtalun', NULL, NULL),
('3402090007', '3402090', 'Imogiri', NULL, NULL),
('3402090008', '3402090', 'Wukirsari', NULL, NULL),
('3402100001', '3402100', 'Mangunan', NULL, NULL),
('3402100002', '3402100', 'Muntuk', NULL, NULL),
('3402100003', '3402100', 'Dlingo', NULL, NULL),
('3402100004', '3402100', 'Temuwuh', NULL, NULL),
('3402100005', '3402100', 'Jatimulyo', NULL, NULL),
('3402100006', '3402100', 'Terong', NULL, NULL),
('3402110001', '3402110', 'Wonokromo', NULL, NULL),
('3402110002', '3402110', 'Pleret', NULL, NULL),
('3402110003', '3402110', 'Segoroyoso', NULL, NULL),
('3402110004', '3402110', 'Bawuran', NULL, NULL),
('3402110005', '3402110', 'Wonolelo', NULL, NULL),
('3402120001', '3402120', 'Sitimulyo', NULL, NULL),
('3402120002', '3402120', 'Srimulyo', NULL, NULL),
('3402120003', '3402120', 'Srimartani', NULL, NULL),
('3402130001', '3402130', 'Tamanan', NULL, NULL),
('3402130002', '3402130', 'Jagalan', NULL, NULL),
('3402130003', '3402130', 'Singosaren', NULL, NULL),
('3402130004', '3402130', 'Wirokerten', NULL, NULL),
('3402130005', '3402130', 'Jambidan', NULL, NULL),
('3402130006', '3402130', 'Potorono', NULL, NULL),
('3402130007', '3402130', 'Baturetno', NULL, NULL),
('3402130008', '3402130', 'Banguntapan', NULL, NULL),
('3402140001', '3402140', 'Pendowoharjo', NULL, NULL),
('3402140002', '3402140', 'Timbulharjo', NULL, NULL),
('3402140003', '3402140', 'Bangunharjo', NULL, NULL),
('3402140004', '3402140', 'Panggungharjo', NULL, NULL),
('3402150001', '3402150', 'Bangunjiwo', NULL, NULL),
('3402150002', '3402150', 'Tirtonirmolo', NULL, NULL),
('3402150003', '3402150', 'Tamantirto', NULL, NULL),
('3402150004', '3402150', 'Ngestiharjo', NULL, NULL),
('3402160001', '3402160', 'Triwidadi', NULL, NULL),
('3402160002', '3402160', 'Sendangsari', NULL, NULL),
('3402160003', '3402160', 'Guwosari', NULL, NULL),
('3402170001', '3402170', 'Argodadi', NULL, NULL),
('3402170002', '3402170', 'Argorejo', NULL, NULL),
('3402170003', '3402170', 'Argosari', NULL, NULL),
('3402170004', '3402170', 'Argomulyo', NULL, NULL),
('3403010006', '3403010', 'Giriharjo', NULL, NULL),
('3403010007', '3403010', 'Giriwungu', NULL, NULL),
('3403010008', '3403010', 'Girimulyo', NULL, NULL),
('3403010009', '3403010', 'Girikarto', NULL, NULL),
('3403010010', '3403010', 'Girisekar', NULL, NULL),
('3403010011', '3403010', 'Girisuko', NULL, NULL),
('3403011001', '3403011', 'Girijati', NULL, NULL),
('3403011002', '3403011', 'Giriasih', NULL, NULL),
('3403011003', '3403011', 'Giricahyo', NULL, NULL),
('3403011004', '3403011', 'Giripurwo', NULL, NULL),
('3403011005', '3403011', 'Giritirto', NULL, NULL),
('3403020001', '3403020', 'Karang Duwet', NULL, NULL),
('3403020002', '3403020', 'Karang Asem', NULL, NULL),
('3403020003', '3403020', 'Mulusan', NULL, NULL),
('3403020004', '3403020', 'Giring', NULL, NULL),
('3403020005', '3403020', 'Sodo', NULL, NULL),
('3403020006', '3403020', 'Pampang', NULL, NULL),
('3403020007', '3403020', 'Grogol', NULL, NULL),
('3403030001', '3403030', 'Krambil Sawit', NULL, NULL),
('3403030002', '3403030', 'Kanigoro', NULL, NULL),
('3403030003', '3403030', 'Planjan', NULL, NULL),
('3403030004', '3403030', 'Monggol', NULL, NULL),
('3403030005', '3403030', 'Kepek', NULL, NULL),
('3403030006', '3403030', 'Nglora', NULL, NULL),
('3403030007', '3403030', 'Jetis', NULL, NULL),
('3403040005', '3403040', 'Sidoharjo', NULL, NULL),
('3403040006', '3403040', 'Tepus', NULL, NULL),
('3403040007', '3403040', 'Purwodadi', NULL, NULL),
('3403040008', '3403040', 'Giripanggung', NULL, NULL),
('3403040009', '3403040', 'Sumber Wungu', NULL, NULL),
('3403041001', '3403041', 'Kemadang', NULL, NULL),
('3403041002', '3403041', 'Kemiri', NULL, NULL),
('3403041003', '3403041', 'Banjarejo', NULL, NULL),
('3403041004', '3403041', 'Ngestirejo', NULL, NULL),
('3403041005', '3403041', 'Hargosari', NULL, NULL),
('3403050007', '3403050', 'Melikan', NULL, NULL),
('3403050010', '3403050', 'Bohol', NULL, NULL),
('3403050011', '3403050', 'Pringombo', NULL, NULL),
('3403050012', '3403050', 'Botodayakan', NULL, NULL),
('3403050013', '3403050', 'Petir', NULL, NULL),
('3403050014', '3403050', 'Semugih', NULL, NULL),
('3403050015', '3403050', 'Karangwuni', NULL, NULL),
('3403050016', '3403050', 'Pucanganom', NULL, NULL),
('3403051001', '3403051', 'Balong', NULL, NULL),
('3403051002', '3403051', 'Jepitu', NULL, NULL),
('3403051003', '3403051', 'Karangawen', NULL, NULL),
('3403051004', '3403051', 'Tileng', NULL, NULL),
('3403051005', '3403051', 'Nglindur', NULL, NULL),
('3403051006', '3403051', 'Jerukwudel', NULL, NULL),
('3403051007', '3403051', 'Pucung', NULL, NULL),
('3403051008', '3403051', 'Songbanyu', NULL, NULL),
('3403060001', '3403060', 'Pacarejo', NULL, NULL),
('3403060002', '3403060', 'Candirejo', NULL, NULL),
('3403060003', '3403060', 'Dadapayu', NULL, NULL),
('3403060004', '3403060', 'Ngeposari', NULL, NULL),
('3403060005', '3403060', 'Semanu', NULL, NULL),
('3403070001', '3403070', 'Gombang', NULL, NULL),
('3403070002', '3403070', 'Sidorejo', NULL, NULL),
('3403070003', '3403070', 'Bedoyo', NULL, NULL),
('3403070004', '3403070', 'Karang Asem', NULL, NULL),
('3403070005', '3403070', 'Ponjong', NULL, NULL),
('3403070006', '3403070', 'Genjahan', NULL, NULL),
('3403070007', '3403070', 'Sumber Giri', NULL, NULL),
('3403070008', '3403070', 'Kenteng', NULL, NULL),
('3403070009', '3403070', 'Tambakromo', NULL, NULL),
('3403070010', '3403070', 'Sawahan', NULL, NULL),
('3403070011', '3403070', 'Umbul Rejo', NULL, NULL),
('3403080001', '3403080', 'Bendungan', NULL, NULL),
('3403080002', '3403080', 'Bejiharjo', NULL, NULL),
('3403080003', '3403080', 'Wiladeg', NULL, NULL),
('3403080004', '3403080', 'Kelor', NULL, NULL),
('3403080005', '3403080', 'Ngipak', NULL, NULL),
('3403080006', '3403080', 'Karangmojo', NULL, NULL),
('3403080007', '3403080', 'Gedang Rejo', NULL, NULL),
('3403080008', '3403080', 'Ngawis', NULL, NULL),
('3403080009', '3403080', 'Jati Ayu', NULL, NULL),
('3403090001', '3403090', 'Wunung', NULL, NULL),
('3403090002', '3403090', 'Mulo', NULL, NULL),
('3403090003', '3403090', 'Duwet', NULL, NULL),
('3403090004', '3403090', 'Wareng', NULL, NULL),
('3403090005', '3403090', 'Pulutan', NULL, NULL),
('3403090006', '3403090', 'Siraman', NULL, NULL),
('3403090007', '3403090', 'Karang Rejek', NULL, NULL),
('3403090008', '3403090', 'Baleharjo', NULL, NULL),
('3403090009', '3403090', 'Selang', NULL, NULL),
('3403090010', '3403090', 'Wonosari', NULL, NULL),
('3403090011', '3403090', 'Kepek', NULL, NULL),
('3403090012', '3403090', 'Piyaman', NULL, NULL),
('3403090013', '3403090', 'Karang Tengah', NULL, NULL),
('3403090014', '3403090', 'Gari', NULL, NULL),
('3403100001', '3403100', 'Banyusoco', NULL, NULL),
('3403100002', '3403100', 'Plembutan', NULL, NULL),
('3403100003', '3403100', 'Bleberan', NULL, NULL),
('3403100004', '3403100', 'Getas', NULL, NULL),
('3403100005', '3403100', 'Dengok', NULL, NULL),
('3403100006', '3403100', 'Ngunut', NULL, NULL),
('3403100007', '3403100', 'Playen', NULL, NULL),
('3403100008', '3403100', 'Ngawu', NULL, NULL),
('3403100009', '3403100', 'Bandung', NULL, NULL),
('3403100010', '3403100', 'Logandeng', NULL, NULL),
('3403100011', '3403100', 'Gading', NULL, NULL),
('3403100012', '3403100', 'Banaran', NULL, NULL),
('3403100013', '3403100', 'Ngleri', NULL, NULL),
('3403110001', '3403110', 'Semoyo', NULL, NULL),
('3403110002', '3403110', 'Pengkok', NULL, NULL),
('3403110003', '3403110', 'Beji', NULL, NULL),
('3403110004', '3403110', 'Bunder', NULL, NULL),
('3403110005', '3403110', 'Nglegi', NULL, NULL),
('3403110006', '3403110', 'Putat', NULL, NULL),
('3403110007', '3403110', 'Salam', NULL, NULL),
('3403110008', '3403110', 'Patuk', NULL, NULL),
('3403110009', '3403110', 'Ngoro Oro', NULL, NULL),
('3403110010', '3403110', 'Nglanggeran', NULL, NULL),
('3403110011', '3403110', 'Terbah', NULL, NULL),
('3403120001', '3403120', 'Ngalang', NULL, NULL),
('3403120002', '3403120', 'Hargo Mulyo', NULL, NULL),
('3403120003', '3403120', 'Mertelu', NULL, NULL),
('3403120004', '3403120', 'Tegalrejo', NULL, NULL),
('3403120005', '3403120', 'Watu Gajah', NULL, NULL),
('3403120006', '3403120', 'Sampang', NULL, NULL),
('3403120007', '3403120', 'Serut', NULL, NULL),
('3403130001', '3403130', 'Kedung Keris', NULL, NULL),
('3403130002', '3403130', 'Nglipar', NULL, NULL),
('3403130003', '3403130', 'Pengkol', NULL, NULL),
('3403130004', '3403130', 'Kedungpoh', NULL, NULL),
('3403130005', '3403130', 'Katongan', NULL, NULL),
('3403130006', '3403130', 'Pilang Rejo', NULL, NULL),
('3403130007', '3403130', 'Natah', NULL, NULL),
('3403140001', '3403140', 'Watu Sigar', NULL, NULL),
('3403140002', '3403140', 'Beji', NULL, NULL),
('3403140003', '3403140', 'Kampung', NULL, NULL),
('3403140004', '3403140', 'Jurang Jero', NULL, NULL),
('3403140005', '3403140', 'Sambirejo', NULL, NULL),
('3403140006', '3403140', 'Tancep', NULL, NULL),
('3403150001', '3403150', 'Kalitekuk', NULL, NULL),
('3403150002', '3403150', 'Kemejing', NULL, NULL),
('3403150003', '3403150', 'Semin', NULL, NULL),
('3403150004', '3403150', 'Pundung Sari', NULL, NULL),
('3403150005', '3403150', 'Karang Sari', NULL, NULL),
('3403150006', '3403150', 'Rejosari', NULL, NULL),
('3403150007', '3403150', 'Bulurejo', NULL, NULL),
('3403150008', '3403150', 'Bendung', NULL, NULL),
('3403150009', '3403150', 'Sumberrejo', NULL, NULL),
('3403150010', '3403150', 'Candi Rejo', NULL, NULL),
('3404010001', '3404010', 'Sumberrahayu', NULL, NULL),
('3404010002', '3404010', 'Sumbersari', NULL, NULL),
('3404010003', '3404010', 'Sumber Agung', NULL, NULL),
('3404010004', '3404010', 'Sumberarum', NULL, NULL),
('3404020001', '3404020', 'Sendang Mulyo', NULL, NULL),
('3404020002', '3404020', 'Sendang Arum', NULL, NULL),
('3404020003', '3404020', 'Sendang Rejo', NULL, NULL),
('3404020004', '3404020', 'Sendangsari', NULL, NULL),
('3404020005', '3404020', 'Sendangagung', NULL, NULL),
('3404030001', '3404030', 'Margoluwih', NULL, NULL),
('3404030002', '3404030', 'Margodadi', NULL, NULL),
('3404030003', '3404030', 'Margomulyo', NULL, NULL),
('3404030004', '3404030', 'Margoagung', NULL, NULL),
('3404030005', '3404030', 'Margokaton', NULL, NULL),
('3404040001', '3404040', 'Sidorejo', NULL, NULL),
('3404040002', '3404040', 'Sidoluhur', NULL, NULL),
('3404040003', '3404040', 'Sidomulyo', NULL, NULL),
('3404040004', '3404040', 'Sidoagung', NULL, NULL),
('3404040005', '3404040', 'Sidokarto', NULL, NULL),
('3404040006', '3404040', 'Sidoarum', NULL, NULL),
('3404040007', '3404040', 'Sidomoyo', NULL, NULL),
('3404050001', '3404050', 'Balecatur', NULL, NULL),
('3404050002', '3404050', 'Ambarketawang', NULL, NULL),
('3404050003', '3404050', 'Banyuraden', NULL, NULL),
('3404050004', '3404050', 'Nogotirto', NULL, NULL),
('3404050005', '3404050', 'Trihanggo', NULL, NULL),
('3404060001', '3404060', 'Tirtoadi', NULL, NULL),
('3404060002', '3404060', 'Sumberadi', NULL, NULL),
('3404060003', '3404060', 'Tlogoadi', NULL, NULL),
('3404060004', '3404060', 'Sendangadi', NULL, NULL),
('3404060005', '3404060', 'Sinduadi', NULL, NULL),
('3404070001', '3404070', 'Catur Tunggal', NULL, NULL),
('3404070002', '3404070', 'Maguwoharjo', NULL, NULL),
('3404070003', '3404070', 'Condong Catur', NULL, NULL),
('3404080001', '3404080', 'Sendang Tirto', NULL, NULL),
('3404080002', '3404080', 'Tegal Tirto', NULL, NULL),
('3404080003', '3404080', 'Jogo Tirto', NULL, NULL),
('3404080004', '3404080', 'Kali Tirto', NULL, NULL),
('3404090001', '3404090', 'Sumber Harjo', NULL, NULL),
('3404090002', '3404090', 'Wukir Harjo', NULL, NULL),
('3404090003', '3404090', 'Gayam Harjo', NULL, NULL),
('3404090004', '3404090', 'Sambi Rejo', NULL, NULL),
('3404090005', '3404090', 'Madu Rejo', NULL, NULL),
('3404090006', '3404090', 'Boko Harjo', NULL, NULL),
('3404100001', '3404100', 'Purwo Martani', NULL, NULL),
('3404100002', '3404100', 'Tirto Martani', NULL, NULL),
('3404100003', '3404100', 'Taman Martani', NULL, NULL),
('3404100004', '3404100', 'Selo Martani', NULL, NULL),
('3404110001', '3404110', 'Wedomartani', NULL, NULL),
('3404110002', '3404110', 'Umbulmartani', NULL, NULL),
('3404110003', '3404110', 'Widodo Martani', NULL, NULL),
('3404110004', '3404110', 'Bimo Martani', NULL, NULL),
('3404110005', '3404110', 'Sindumartani', NULL, NULL),
('3404120001', '3404120', 'Sari Harjo', NULL, NULL),
('3404120002', '3404120', 'Sinduharjo', NULL, NULL),
('3404120003', '3404120', 'Minomartani', NULL, NULL),
('3404120004', '3404120', 'Suko Harjo', NULL, NULL),
('3404120005', '3404120', 'Sardonoharjo', NULL, NULL),
('3404120006', '3404120', 'Donoharjo', NULL, NULL),
('3404130001', '3404130', 'Catur Harjo', NULL, NULL),
('3404130002', '3404130', 'Triharjo', NULL, NULL),
('3404130003', '3404130', 'Tridadi', NULL, NULL),
('3404130004', '3404130', 'Pandowo Harjo', NULL, NULL),
('3404130005', '3404130', 'Tri Mulyo', NULL, NULL),
('3404140001', '3404140', 'Banyu Rejo', NULL, NULL),
('3404140002', '3404140', 'Tambak Rejo', NULL, NULL),
('3404140003', '3404140', 'Sumber Rejo', NULL, NULL),
('3404140004', '3404140', 'Pondok Rejo', NULL, NULL),
('3404140005', '3404140', 'Moro Rejo', NULL, NULL),
('3404140006', '3404140', 'Margo Rejo', NULL, NULL),
('3404140007', '3404140', 'Lumbung Rejo', NULL, NULL),
('3404140008', '3404140', 'Merdiko Rejo', NULL, NULL),
('3404150001', '3404150', 'Bangun Kerto', NULL, NULL),
('3404150002', '3404150', 'Donokerto', NULL, NULL),
('3404150003', '3404150', 'Giri Kerto', NULL, NULL),
('3404150004', '3404150', 'Wono Kerto', NULL, NULL),
('3404160001', '3404160', 'Purwo Binangun', NULL, NULL),
('3404160002', '3404160', 'Candi Binangun', NULL, NULL),
('3404160003', '3404160', 'Harjo Binangun', NULL, NULL),
('3404160004', '3404160', 'Pakem Binangun', NULL, NULL),
('3404160005', '3404160', 'Hargo Binangun', NULL, NULL),
('3404170001', '3404170', 'Wukir Sari', NULL, NULL),
('3404170002', '3404170', 'Argo Mulyo', NULL, NULL),
('3404170003', '3404170', 'Glagah Harjo', NULL, NULL),
('3404170004', '3404170', 'Kepuh Harjo', NULL, NULL),
('3404170005', '3404170', 'Umbul Harjo', NULL, NULL),
('3471010001', '3471010', 'Gedongkiwo', NULL, NULL),
('3471010002', '3471010', 'Suryodiningratan', NULL, NULL),
('3471010003', '3471010', 'Mantrijeron', NULL, NULL),
('3471020001', '3471020', 'Patehan', '-7.811142', '110.359925'),
('3471020002', '3471020', 'Panembahan', '-7.807664', '110.365139'),
('3471020003', '3471020', 'Kadipaten', '-7.806069', '110.358436'),
('3471030001', '3471030', 'Brontokusuman', '-7.820234', '110.371096'),
('3471030002', '3471030', 'Keparakan', '-7.812513', '110.371096'),
('3471030003', '3471030', 'Wirogunan', '-7.807069', '110.377054'),
('3471040001', '3471040', 'Giwangan', '-7.829633', '110.388970'),
('3471040002', '3471040', 'Sorosutan', '-7.823572', '110.381523'),
('3471040003', '3471040', 'Pandeyan', '-7.816766', '110.388970'),
('3471040004', '3471040', 'Warungboto', '-7.809046', '110.388970'),
('3471040005', '3471040', 'Tahunan', '-7.806771', '110.383012'),
('3471040006', '3471040', 'Muja Muju', '-7.798606', '110.391949'),
('3471040007', '3471040', 'Semaki', '-7.799053', '110.383012'),
('3471050001', '3471050', 'Prenggan', '-7.821464', '110.397907'),
('3471050002', '3471050', 'Purbayan', '-7.827068', '110.401631'),
('3471050003', '3471050', 'Rejowinangun', '-7.811172', '110.397907'),
('3471060001', '3471060', 'Baciro', '-7.791186', '110.385991'),
('3471060002', '3471060', 'Demangan', '-7.786573', '110.388226'),
('3471060003', '3471060', 'Klitren', '-7.783618', '110.383012'),
('3471060004', '3471060', 'Kotabaru', '-7.786636', '110.374075'),
('3471060005', '3471060', 'Terban', '-7.778919', '110.374075'),
('3471070001', '3471070', 'Suryatmajan', '-7.794045', '110.367373'),
('3471070002', '3471070', 'Tegal Panggung', '-7.791929', '110.371096'),
('3471070003', '3471070', 'Bausasran', '-7.793599', '110.376310'),
('3471080001', '3471080', 'Purwo Kinanti', '-7.799499', '110.374075'),
('3471080002', '3471080', 'Gunung Ketur', '-7.801243', '110.377799'),
('3471090001', '3471090', 'Prawirodirjan', '-7.803725', '110.369443'),
('3471090002', '3471090', 'Ngupasan', '-7.799121', '110.365111'),
('3471100001', '3471100', 'Notoprajan', '-7.802964', '110.356202'),
('3471100002', '3471100', 'Ngampilan', '-7.797669', '110.359181'),
('3471110001', '3471110', 'Patangpuluhan', '-7.810982', '110.350244'),
('3471110002', '3471110', 'Wirobrajan', '-7.803262', '110.350244'),
('3471110003', '3471110', 'Pakuncen', '-7.798115', '110.350244'),
('3471120001', '3471120', 'Pringgokusuman', '-7.792524', '110.359181'),
('3471120002', '3471120', 'Sosromenduran', '-7.791621', '110.364394'),
('3471130001', '3471130', 'Bumijo', '-7.784806', '110.359181'),
('3471130002', '3471130', 'Gowongan', '-7.784509', '110.365139'),
('3471130003', '3471130', 'Cokrodiningratan', '-7.779216', '110.368117'),
('3471140001', '3471140', 'Tegalrejo', '-7.460321', '110.275775'),
('3471140002', '3471140', 'Bener', '-7.779957', '110.353223'),
('3471140003', '3471140', 'Kricak', '-7.774517', '110.359181'),
('3471140004', '3471140', 'Karangwaru', '-7.773614', '110.364394');

-- --------------------------------------------------------

--
-- Table structure for table `wilayah_kabupaten`
--

CREATE TABLE `wilayah_kabupaten` (
  `id` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `provinsi_id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wilayah_kabupaten`
--

INSERT INTO `wilayah_kabupaten` (`id`, `provinsi_id`, `nama`) VALUES
('3401', '34', 'Kab. Kulon Progo'),
('3402', '34', 'Kab. Bantul'),
('3403', '34', 'Kab. Gunung Kidul'),
('3404', '34', 'Kab. Sleman'),
('3471', '34', 'Kota Yogyakarta');

-- --------------------------------------------------------

--
-- Table structure for table `wilayah_kecamatan`
--

CREATE TABLE `wilayah_kecamatan` (
  `id` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `kabupaten_id` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wilayah_kecamatan`
--

INSERT INTO `wilayah_kecamatan` (`id`, `kabupaten_id`, `nama`) VALUES
('3401010', '3401', ' Temon'),
('3401020', '3401', ' Wates'),
('3401030', '3401', ' Panjatan'),
('3401040', '3401', ' Galur'),
('3401050', '3401', ' Lendah'),
('3401060', '3401', ' Sentolo'),
('3401070', '3401', ' Pengasih'),
('3401080', '3401', ' Kokap'),
('3401090', '3401', ' Girimulyo'),
('3401100', '3401', ' Nanggulan'),
('3401110', '3401', ' Kalibawang'),
('3401120', '3401', ' Samigaluh'),
('3402010', '3402', ' Srandakan'),
('3402020', '3402', ' Sanden'),
('3402030', '3402', ' Kretek'),
('3402040', '3402', ' Pundong'),
('3402050', '3402', ' Bambang Lipuro'),
('3402060', '3402', ' Pandak'),
('3402070', '3402', ' Bantul'),
('3402080', '3402', ' Jetis'),
('3402090', '3402', ' Imogiri'),
('3402100', '3402', ' Dlingo'),
('3402110', '3402', ' Pleret'),
('3402120', '3402', ' Piyungan'),
('3402130', '3402', ' Banguntapan'),
('3402140', '3402', ' Sewon'),
('3402150', '3402', ' Kasihan'),
('3402160', '3402', ' Pajangan'),
('3402170', '3402', ' Sedayu'),
('3403010', '3403', ' Panggang'),
('3403011', '3403', ' Purwosari'),
('3403020', '3403', ' Paliyan'),
('3403030', '3403', ' Sapto Sari'),
('3403040', '3403', ' Tepus'),
('3403041', '3403', ' Tanjungsari'),
('3403050', '3403', ' Rongkop'),
('3403051', '3403', ' Girisubo'),
('3403060', '3403', ' Semanu'),
('3403070', '3403', ' Ponjong'),
('3403080', '3403', ' Karangmojo'),
('3403090', '3403', ' Wonosari'),
('3403100', '3403', ' Playen'),
('3403110', '3403', ' Patuk'),
('3403120', '3403', ' Gedang Sari'),
('3403130', '3403', ' Nglipar'),
('3403140', '3403', ' Ngawen'),
('3403150', '3403', ' Semin'),
('3404010', '3404', ' Moyudan'),
('3404020', '3404', ' Minggir'),
('3404030', '3404', ' Seyegan'),
('3404040', '3404', ' Godean'),
('3404050', '3404', ' Gamping'),
('3404060', '3404', ' Mlati'),
('3404070', '3404', ' Depok'),
('3404080', '3404', ' Berbah'),
('3404090', '3404', ' Prambanan'),
('3404100', '3404', ' Kalasan'),
('3404110', '3404', ' Ngemplak'),
('3404120', '3404', ' Ngaglik'),
('3404130', '3404', ' Sleman'),
('3404140', '3404', ' Tempel'),
('3404150', '3404', ' Turi'),
('3404160', '3404', ' Pakem'),
('3404170', '3404', ' Cangkringan'),
('3471010', '3471', ' Mantrijeron'),
('3471020', '3471', ' Kraton'),
('3471030', '3471', ' Mergangsan'),
('3471040', '3471', ' Umbulharjo'),
('3471050', '3471', ' Kotagede'),
('3471060', '3471', ' Gondokusuman'),
('3471070', '3471', ' Danurejan'),
('3471080', '3471', ' Pakualaman'),
('3471090', '3471', ' Gondomanan'),
('3471100', '3471', ' Ngampilan'),
('3471110', '3471', ' Wirobrajan'),
('3471120', '3471', ' Gedong Tengen'),
('3471130', '3471', ' Jetis'),
('3471140', '3471', ' Tegalrejo');

-- --------------------------------------------------------

--
-- Table structure for table `wilayah_provinsi`
--

CREATE TABLE `wilayah_provinsi` (
  `id` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wilayah_provinsi`
--

INSERT INTO `wilayah_provinsi` (`id`, `nama`) VALUES
('11', 'Aceh'),
('12', 'Sumatera Utara'),
('13', 'Sumatera Barat'),
('14', 'Riau'),
('15', 'Jambi'),
('16', 'Sumatera Selatan'),
('17', 'Bengkulu'),
('18', 'Lampung'),
('19', 'Kepulauan Bangka Belitung'),
('21', 'Kepulauan Riau'),
('31', 'Dki Jakarta'),
('32', 'Jawa Barat'),
('33', 'Jawa Tengah'),
('34', 'Di Yogyakarta'),
('35', 'Jawa Timur'),
('36', 'Banten'),
('51', 'Bali'),
('52', 'Nusa Tenggara Barat'),
('53', 'Nusa Tenggara Timur'),
('61', 'Kalimantan Barat'),
('62', 'Kalimantan Tengah'),
('63', 'Kalimantan Selatan'),
('64', 'Kalimantan Timur'),
('65', 'Kalimantan Utara'),
('71', 'Sulawesi Utara'),
('72', 'Sulawesi Tengah'),
('73', 'Sulawesi Selatan'),
('74', 'Sulawesi Tenggara'),
('75', 'Gorontalo'),
('76', 'Sulawesi Barat'),
('81', 'Maluku'),
('82', 'Maluku Utara'),
('91', 'Papua Barat'),
('94', 'Papua');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `disbud`
--
ALTER TABLE `disbud`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `disbud_email_unique` (`email`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`);

--
-- Indexes for table `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masa_login`
--
ALTER TABLE `masa_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perdais`
--
ALTER TABLE `perdais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prioritas`
--
ALTER TABLE `prioritas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skpd`
--
ALTER TABLE `skpd`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `skpd_email_unique` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `usulan_masyarakat`
--
ALTER TABLE `usulan_masyarakat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usulan_skpd`
--
ALTER TABLE `usulan_skpd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usulan_skpd_detail`
--
ALTER TABLE `usulan_skpd_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wilayah_desa`
--
ALTER TABLE `wilayah_desa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wilayah_kabupaten`
--
ALTER TABLE `wilayah_kabupaten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wilayah_kecamatan`
--
ALTER TABLE `wilayah_kecamatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wilayah_provinsi`
--
ALTER TABLE `wilayah_provinsi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `masa_login`
--
ALTER TABLE `masa_login`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `perdais`
--
ALTER TABLE `perdais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `prioritas`
--
ALTER TABLE `prioritas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
