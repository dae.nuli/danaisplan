$(function() {
    "use strict";
    /* =========================================================================
    Smooth Scroll
    ============================================================================ */
    $('.smooth-scroll').on('click', function() {
	    var target = $(this.hash);
	    if (target.length) {
	        $('html,body').animate({
	            scrollTop: (target.offset().top - 70 + 1)
	        }, 1000);
	        return false;
	    }
	});
  $(window).scroll(function() { // check if scroll event happened
    // console.log($(document).scrollTop());
    if ($(document).scrollTop() > 50) { // check if user scrolled more than 50 from top of the browser window
      $('.navbar-fixed-top').addClass('navbar-scroll-down');
      // $(".navbar-fixed-top").css("background-color", "rgba(249, 249, 249, 0.95)"); // if yes, then change the color of class "navbar-fixed-top" to white (#f8f8f8)
    } else {
      $('.navbar-fixed-top').removeClass('navbar-scroll-down');
      // $(".navbar-fixed-top").css("background-color", "transparent"); // if not, change it back to transparent
    }
  });
    // $(window).load(function(){
    //   $('.flexslider').flexslider({
    //     slideshowSpeed: 5000,
    //     animation: 'fade',
    //     directionNav: false,
    //     controlNav: false,
    //     touch: false,
    //   });
    // });
    // $('body').removeClass('fade-out');

    $(document).on('click','.waiting',function(){
        $('.waiting').addClass('disabled');
    });

    $(document).on('click','.close-waiting,.close-button',function(){
        $('.waiting').removeClass('disabled');
    });

    $(document).on('click','.please-waiting',function(){
        waiting();
    });

    $(document).on('click','.modal-confirmation',function(){
        waiting();
        var uri     = $('.information-link').data('link');
        var title   = $(this).data('title');
        var message = $(this).data('message');
        var value   = $(this).val();
        // console.log(value);
        $('#open-modal').load('/confirmation/'+uri+'/'+title+'/'+message+'/'+value);
    });

    $('.import-program').on('click',function (e){
        waiting();
        $('#open-modal').load($(this).data('url'));
    });

    $('.masa-login').on('click',function (e){
        waiting();
        $('#open-modal').load($(this).data('url'));
    });
});

function waiting(){
    $.blockUI({ 
        message: '<h1 style="font-weight:300">Please Wait</h1>',
        css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#fff', 
            '-webkit-border-radius': '0', 
            '-moz-border-radius': '0',  
            color: '#000',
            'text-transform': 'uppercase'
        },
        baseZ:9999
    });
}