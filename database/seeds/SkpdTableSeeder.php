<?php

use Illuminate\Database\Seeder;

class SkpdTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('disbud')->insert([
			'name'      => 'Dinas Kebudayaan',
			'email'     => 'admin@admin.com',
			'address'     => 'Jl. Kaliurang',
			'password'  => bcrypt('111111'),
			'is_active'  => 1,
			'level'  => 1,
			'created_at'  => date('Y-m-d H:i:s'),
        ]);
		DB::table('masa_login')->insert([
			'id'         => 1,
			'start_date' => date('Y-m-d'),
			'end_date'   => date('Y-m-d'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s')
        ]);
		// DB::table('skpd')->insert([
		// 	'id'            => str_random(32),
		// 	'code'          => str_random(4),
		// 	'instansi_name' => 'PT Nuli',
		// 	'email'         => 'skpd@gmail.com',
		// 	'username'      => 'skpd',
		// 	'phone'         => 123,
		// 	'is_active'     => 1,
		// 	'is_complete'   => 2,
		// 	'address'       => 'Jl. Kaliurang KM 14',
		// 	'personil_name' => 'Nuli Giarsyani',
		// 	'password'      => bcrypt('111111'),
		// 	'created_at'    => date('Y-m-d H:i:s'),
  //       ]);
   //      DB::table('skpd')->insert([
			// 'id'            => str_random(32),
			// 'code'          => str_random(4),
			// 'instansi_name' => 'PT Komuri',
			// 'email'         => 'firman@geekgarden.id',
			// 'username'      => 'firman',
			// 'phone'         => 123,
			// 'address'       => 'Jl. Kaliurang KM 14',
			// 'personil_name' => 'Firmansyah',
			// 'password'      => bcrypt('111111'),
			// 'created_at'  => date('Y-m-d H:i:s'),
   //      ]);
    }
}
