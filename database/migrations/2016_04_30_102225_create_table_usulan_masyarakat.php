<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsulanMasyarakat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('usulan_masyarakat')){
            Schema::create('usulan_masyarakat', function (Blueprint $table) {
                $table->string('id',32)->primary();
                $table->string('id_skpd',32)->nullable();
                // $table->string('id_program',32);
                // $table->string('id_kegiatan',32);
                // $table->integer('id_provinsi');
                $table->string('id_kabupaten',4);
                $table->string('id_kecamatan',7);
                $table->string('id_desa',10);
                
                // $table->string('kabupaten_id',4);
                // $table->string('kecamatan_id',7);
                // $table->string('desa_id',10);

                $table->string('latitude',100)->nullable();
                $table->string('longitude',100)->nullable();
                $table->string('name',30);
                $table->string('email',30);
                $table->string('address');
                // $table->text('problems_summary');
                $table->string('phone',20);
                $table->text('proposal');
                $table->string('document',30)->nullable();
                $table->enum('status', ['0', '1', '2'])->default(0); // 0. New, 1. Approved, 2. Denied
                $table->enum('assign', ['0', '1'])->default(0); // 0. Not assign, 1. Has been assigned
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usulan_masyarakat');
    }
}
