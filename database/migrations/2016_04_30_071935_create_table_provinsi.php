<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProvinsi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('wilayah_provinsi')){
            Schema::create('wilayah_provinsi', function (Blueprint $table) {
                $table->string('id',2)->primary('id');
                $table->string('nama',30);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wilayah_provinsi');
    }
}
