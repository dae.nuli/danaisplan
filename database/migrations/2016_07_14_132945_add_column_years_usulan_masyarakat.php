<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnYearsUsulanMasyarakat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('usulan_masyarakat', 'years')) {
            Schema::table('usulan_masyarakat', function (Blueprint $table) {
                $table->string('years',4)->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('usulan_masyarakat', 'years')) {
            Schema::table('usulan_masyarakat', function (Blueprint $table) {
                $table->dropColumn('years',4)->nullable();
            });
        }
    }
}
