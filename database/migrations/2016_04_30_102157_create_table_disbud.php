<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDisbud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('disbud')){
            Schema::create('disbud', function (Blueprint $table) {
                $table->string('id',32)->primary();
                $table->string('name',30);
                $table->string('email',30)->unique();
                $table->string('password',100);
                $table->string('address');
                $table->string('phone',20);
                $table->enum('is_active', ['0', '1'])->default(1);
                $table->integer('level')->nullable();
                $table->string('avatar',30)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('disbud');
    }
}
