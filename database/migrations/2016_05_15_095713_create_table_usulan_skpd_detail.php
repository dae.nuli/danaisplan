<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsulanSkpdDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('usulan_skpd_detail')){
            Schema::create('usulan_skpd_detail', function (Blueprint $table) {
                $table->string('id',32)->primary();
                $table->string('id_usulan',32);
                $table->string('kabupaten_id',4);
                $table->string('kecamatan_id',7);
                $table->string('desa_id',10);
                $table->string('latitude')->nullable();
                $table->string('longitude')->nullable();
                // $table->string('keluaran');
                $table->string('tolak_ukur_kinerja');
                $table->string('jumlah_target_kinerja');
                $table->string('satuan_target_kinerja');
                $table->integer('anggaran');
                $table->integer('prioritas');
                // $table->string('tor')->nullable();
                // $table->string('rab')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usulan_skpd_detail');
    }
}
