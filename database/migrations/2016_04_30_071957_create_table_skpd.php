<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSkpd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('skpd')){
            Schema::create('skpd', function (Blueprint $table) {
                $table->string('id',32)->primary();
                $table->string('code',50);
                $table->string('instansi_name');
                $table->string('email',30)->unique();
                $table->string('email_staff',30)->nullable();
                $table->string('username',50)->nullable();
                $table->string('phone',30)->nullable();
                $table->string('address')->nullable();
                $table->string('personil_name',50)->nullable();
                $table->string('password')->nullable();
                $table->enum('is_active', ['0', '1'])->default(0);
                $table->enum('is_complete', ['0', '1', '2'])->default(0);
                $table->string('activation')->nullable();
                $table->string('avatar',30)->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('skpd');
    }
}
