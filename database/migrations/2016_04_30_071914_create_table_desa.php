<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDesa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('wilayah_desa')){
            Schema::create('wilayah_desa', function (Blueprint $table) {
                $table->string('id',10)->primary('id');
                $table->string('kecamatan_id',7);
                $table->string('nama',40);
                $table->string('latitude',20);
                $table->string('longitude',20);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wilayah_desa');
    }
}
