<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsulanSkpd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('usulan_skpd')){
            Schema::create('usulan_skpd', function (Blueprint $table) {
                $table->string('id',32)->primary();
                $table->string('id_skpd',32)->nullable();
                $table->integer('id_program');
                $table->integer('id_kegiatan');
                $table->string('years',4);
                $table->string('tor',30)->nullable();
                $table->string('rab',30)->nullable();
                $table->integer('perdais');
                // $table->integer('kabupaten_id');
                // $table->integer('kecamatan_id');
                // $table->integer('desa_id');
                // $table->string('keluaran');
                // $table->string('jumlah');
                // $table->string('satuan');
                // $table->string('anggaran');
                // $table->string('tor');
                // $table->string('rab');
                $table->text('note')->nullable();
                $table->text('revision_note')->nullable();
                $table->enum('is_approved', ['0', '1', '2', '3', '4'])->default(0); // 0. Pending, 1. Approved, 2. Revision, 3. Denied, 4. Repair Revision
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usulan_skpd');
    }
}
