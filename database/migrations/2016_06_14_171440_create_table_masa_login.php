<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMasaLogin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('masa_login')){
            Schema::create('masa_login', function (Blueprint $table) {
                $table->increments('id');
                $table->date('start_date');
                $table->date('end_date');
                // $table->enum('status', ['0', '1'])->default(1); // 0. Tidak aktif, 1. Aktif
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('masa_login');
    }
}
