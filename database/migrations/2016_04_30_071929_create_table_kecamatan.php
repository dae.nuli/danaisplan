<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKecamatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('wilayah_kecamatan')){
            Schema::create('wilayah_kecamatan', function (Blueprint $table) {
                $table->string('id',7)->primary('id');
                $table->string('kabupaten_id',4);
                $table->string('nama',30);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wilayah_kecamatan');
    }
}
