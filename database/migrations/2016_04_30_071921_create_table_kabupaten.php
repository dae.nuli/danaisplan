<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKabupaten extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('wilayah_kabupaten')){
            Schema::create('wilayah_kabupaten', function (Blueprint $table) {
                $table->string('id',4)->primary('id');
                $table->string('provinsi_id',2);
                $table->string('nama',30);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wilayah_kabupaten');
    }
}
