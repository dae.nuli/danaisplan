<?php

namespace App\Http\Middleware;

use Closure;

class LoginSkpd
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(empty($request->session()->get('loginskpd'))){
            return redirect('/')->with('skpd_false','Username and password did not match');
        }
        return $next($request);
    }
}
