<?php

namespace App\Http\Middleware;

use Closure;

class LoginDisbud
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(empty($request->session()->get('loginDisbud'))){
            return redirect('/login')->with('disbud_false','Username and password did not match');
        }
        return $next($request);
    }
}
