<?php

namespace App\Http\Middleware;

use Closure;
use App\Models as M;

class ProfileComplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!empty($request->session()->get('loginskpd'))){
            $var = M\Skpd::findOrFail(session('loginskpd')['id']);
            if($var->is_complete==0 || $var->is_complete==1){
                return redirect('/skpd/profile')->with('skpd_false','Silahkan lengkapi profile anda');
            }
        }
        return $next($request);
    }
}
