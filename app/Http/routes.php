<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/tes',function(){
// return PDF::loadFile('https://www.google.co.id/maps/@-7.8208499,110.3845018,15z')->inline('github.pdf');
    foreach(DB::table('skpd')->get() as $row){
    	$random = str_random(5);
    	$admin = App\Models\Skpd::find($row->id);
    	$admin->avatar = $random.'.png';
	    Avatar::create(ucwords($row->instansi_name))->save(public_path('storage/avatar/skpd/'.$random.'.png'));
    	$admin->save();
    }

    foreach(DB::table('disbud')->get() as $row){
    	$random = str_random(5);
    	$var = App\Models\Disbud::find($row->id);
    	$var->avatar = $random.'.png';
	    Avatar::create(ucwords($row->name))->save(public_path('storage/avatar/disbud/'.$random.'.png'));
    	$var->save();
    }
});

Route::get('/', 'HomeController@index');
Route::get('/usulanMasyarakat', 'HomeController@indexUsulanMasyarakat');
Route::post('/usulanMasyarakat', 'HomeController@postUsulanMasyarakat');
Route::post('/kecamatan', 'HomeController@kecamatan');
Route::post('/desa', 'HomeController@desa');
Route::get('/login', 'HomeController@login');
Route::get('/confirmation/{uri}/{title}/{message}/{value}', 'HomeController@confirmation');
// ($uri,$title,$message,$value)
// Route::group(['middleware' => ['web']], function(){
	Route::post('/skpdAuth', 'HomeController@skpdAuth');
	Route::post('/login', 'HomeController@postLogin');

	Route::group(['middleware' => 'skpdlogin'],function(){
			
			Route::get('/mapmodal/{index}/{latlng?}', 'Skpd\Usulan@mapmodal');

			Route::group(['prefix' => 'skpd'], function () {
				Route::controller('/profile', 'Skpd\Profile');
				
				Route::group(['middleware' => 'profileComplete'],function(){
					Route::controller('/home', 'Skpd\Dashboard');
					
					Route::controller('/usulan', 'Skpd\Usulan');
					// Route::post('/program', 'Skpd\Usulan@postProgram');

					Route::controller('/masyarakat', 'Skpd\Masyarakat');
					
				});
				Route::get('/logout', 'HomeController@logout');
			});
	});

	Route::group(['middleware' => 'disbudlogin'],function(){
		Route::get('/mapmodal/{index}/{latlng?}', 'Disbud\Usulan@mapmodal');
		Route::group(['prefix' => 'disbud'], function () {
			Route::controller('/home', 'Disbud\Dashboard');
			Route::controller('/profile', 'Disbud\Profile');
			Route::controller('/usulan', 'Disbud\Usulan');
			Route::controller('/masyarakat', 'Disbud\Masyarakat');
			Route::controller('/prioritas', 'Disbud\Prioritas');
			Route::controller('/perdais', 'Disbud\Perdais');
			Route::controller('/skpd', 'Disbud\Skpd');
			Route::controller('/program', 'Disbud\Program');
			Route::controller('/kegiatan', 'Disbud\Kegiatan');
			Route::controller('/desa', 'Disbud\Desa');
			Route::controller('/users', 'Disbud\Users');
			Route::controller('/pengumuman', 'Disbud\Pengumuman');
			Route::controller('/report', 'Disbud\Report');
			Route::controller('/reportMasyarakat', 'Disbud\ReportMasyarakat');
			Route::get('/logout', 'HomeController@logout');
		});
	});
// });