<?php

namespace App\Http\Controllers\Disbud;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M, DB, Helper,PDF;

class Dashboard extends Controller
{
	protected $folder   = 'disbud.dashboard';
	protected $title    = 'Dashboard';
	protected $uri      = 'disbud/home';
	protected $template = 'disbud.layout.content';
	protected $limit    = 20;

    public function getIndex(Request $request)
    {
		$data['title']         = $this->title;
		$data['template']      = $this->template;
		$data['uriMasyarakat'] = url('disbud/masyarakat/detail');
		$data['index']         = M\UsulanMasyarakat::orderBy('created_at','desc')->paginate(10);
		$data['uri']           = url('disbud/masyarakat/');
		$data['uriSkpd']       = url('disbud/usulan/');
		$data['action']        = url($this->uri);

        $tahunSekarang = date('Y')+1;
        $data['tahunSekarang'] = $tahunSekarang;

        $qP      = M\Program::orderBy('id','desc');
        $qK      = M\Kegiatan::orderBy('id','desc');
        if(!empty($request->syears)){
        	$qP = $qP->where('years',$request->syears);
        }else{
        	$qP = $qP->where('years',$tahunSekarang);
        }




		// SKPD
		if(!empty($request->program)){
			$qK = $qK->where('id_program',$request->program)->get();
		}

		$qS = M\UsulanSkpd::orderBy('created_at','desc');
		if(!empty($request->syears) && empty($request->program) && empty($request->kegiatan)){
			// return 'f';
			$qS = $qS->where('years',$request->syears)->get();
		}elseif (!empty($request->syears) && !empty($request->program)){
			$qS = $qS->where('years',$request->syears)
					->where('id_program',$request->program)->get();
		}elseif (!empty($request->syears) && !empty($request->program) && !empty($request->kegiatan)) {
			$qS = $qS->where('years',$request->syears)
					->where('id_program',$request->program)
					->where('id_kegiatan',$request->kegiatan)->get();
		}else{
			// return 'f';
			$qS = $qS->where('years',$tahunSekarang)->get();
		}
		// dd($qS);

		$data['program']    = $qP->get();
		$data['idProgram']  = !empty($request->program)?$request->program:NULL;

		$map =  array();
		foreach ($qS as $key => $value) {
			$map[$key] = Helper::usulanSkpdMapFilter($value->id,$request->skabupaten,$request->skecamatan,$request->sdesa,$value->is_approved,$value->created_at); //Tambahkan filter Tahun
			// $map[$key] = Helper::usulanSkpdMapFilter($value->id,$request->skabupaten,$request->skecamatan,$request->sdesa,$request->anggaran,$value->is_approved,$value->created_at); //Tambahkan filter Tahun
		}
		$usulanMap = array_collapse($map);
		$data['usulanMap'] = $usulanMap;
		
		$data['skabupaten'] = M\KabupatenModel::where('provinsi_id',34)->orderBy('id','desc')->get();
		
		$qrSKec = M\KecamatanModel::orderBy('id','desc');
		$qrSDes = M\DesaModel::orderBy('id','desc');

		if(isset($request->skabupaten)){
			$qrSKec = $qrSKec->where('kabupaten_id',$request->skabupaten)->get();
		}
		
		if(isset($request->skecamatan)){
			$qrSDes = $qrSDes->where('kecamatan_id',$request->skecamatan)->get();
		}

		$data['skecamatan'] = $qrSKec;
		$data['sdesa']      = $qrSDes;


		$data['sIdKab'] 	= (!empty($request->skabupaten))?$request->skabupaten:NULL;
		$data['sIdKec'] 	= (!empty($request->skecamatan))?$request->skecamatan:NULL;
		$data['sIdDes'] 	= (!empty($request->sdesa))?$request->sdesa:NULL;


		$data['kegiatan']   = $qK;
		$data['idKegiatan'] = (!empty($request->kegiatan))?$request->kegiatan:NULL;
		
		// if(!empty($request->anggaran)){
		// 	$exp = explode(';', $request->anggaran);
		// 	$data['from'] = $exp[0];
		// 	$data['to']   = $exp[1];
		// }
		// return;
		$data['skpd']       = $qS;

		// Masyarakat

		$qM    = M\UsulanMasyarakat::orderBy('created_at','desc');

		$qNew  = M\UsulanMasyarakat::where('status',0);
		$qApp  = M\UsulanMasyarakat::where('status',1);
		$qDen  = M\UsulanMasyarakat::where('status',2);
		if(isset($request->years)){

			// $qM = $qM->whereRaw('year(created_at) = '.$request->years);
			// $qNew = $qNew->whereRaw('year(created_at) = '.$request->years);
			// $qApp = $qApp->whereRaw('year(created_at) = '.$request->years);
			// $qDen = $qDen->whereRaw('year(created_at) = '.$request->years);


			$qM = $qM->where('years',$request->years);
			$qNew = $qNew->where('years',$request->years);
			$qApp = $qApp->where('years',$request->years);
			$qDen = $qDen->where('years',$request->years);
		}else{

			// $qM = $qM->whereRaw('year(created_at) = '.$tahunSekarang);
			// $qNew = $qNew->whereRaw('year(created_at) = '.$tahunSekarang);
			// $qApp = $qApp->whereRaw('year(created_at) = '.$tahunSekarang);
			// $qDen = $qDen->whereRaw('year(created_at) = '.$tahunSekarang);

			$qM = $qM->where('years',$tahunSekarang);
			$qNew = $qNew->where('years',$tahunSekarang);
			$qApp = $qApp->where('years',$tahunSekarang);
			$qDen = $qDen->where('years',$tahunSekarang);
		}


		$data['mkabupaten'] = M\KabupatenModel::where('provinsi_id',34)->orderBy('id','desc')->get();
		
		$qrKec = M\KecamatanModel::orderBy('id','desc');
		$qrDes = M\DesaModel::orderBy('id','desc');

		if(!empty($request->mkabupaten)){
			$qrKec = $qrKec->where('kabupaten_id',$request->mkabupaten)->get();
			$qM = $qM->where('id_kabupaten',$request->mkabupaten);

			$qNew = $qNew->where('id_kabupaten',$request->mkabupaten);
			$qApp = $qApp->where('id_kabupaten',$request->mkabupaten);
			$qDen = $qDen->where('id_kabupaten',$request->mkabupaten);
		}
		
		if(!empty($request->mkecamatan)){
			$qrDes = $qrDes->where('kecamatan_id',$request->mkecamatan)->get();
			$qM = $qM->where('id_kecamatan',$request->mkecamatan);

			$qNew = $qNew->where('id_kecamatan',$request->mkecamatan);
			$qApp = $qApp->where('id_kecamatan',$request->mkecamatan);
			$qDen = $qDen->where('id_kecamatan',$request->mkecamatan);
		}

		if(!empty($request->mdesa)){
			$qM = $qM->where('id_desa',$request->mdesa);

			$qNew = $qNew->where('id_desa',$request->mdesa);
			$qApp = $qApp->where('id_desa',$request->mdesa);
			$qDen = $qDen->where('id_desa',$request->mdesa);
		}

		$data['new']        = $qNew->count();
		$data['app']        = $qApp->count();
		$data['den']        = $qDen->count();

		$data['masyarakat'] = $qM->get();
		$data['mkecamatan'] = $qrKec;
		$data['mdesa']      = $qrDes;


		$data['mIdKab'] 	= (!empty($request->mkabupaten))?$request->mkabupaten:NULL;
		$data['mIdKec'] 	= (!empty($request->mkecamatan))?$request->mkecamatan:NULL;
		$data['mIdDes'] 	= (!empty($request->mdesa))?$request->mdesa:NULL;


		$data['yearRangeSKPD']       = range(2014,date('Y')+2);
		$data['yearRangeMasyarakat'] = range(2014,date('Y')+2);
		$data['years']               = isset($request->years)?$request->years:NULL;
		$data['syears']              = isset($request->syears)?$request->syears:NULL;

        if(!empty($request->years)){
			$data['printMasyarakat']     = url($this->uri.'?years='.$request->years.'&mkabupaten='.$request->mkabupaten.'&mkecamatan='.$request->mkecamatan.'&mdesa='.$request->mkecamatan.'&action=printmasyarakat');
        }else{
			$data['printMasyarakat']     = url($this->uri.'?years='.$tahunSekarang.'&mkabupaten='.$request->mkabupaten.'&mkecamatan='.$request->mkecamatan.'&mdesa='.$request->mkecamatan.'&action=printmasyarakat');
        }

        if(!empty($request->syears)){
			$data['printSkpd']     = url($this->uri.'?syears='.$request->syears.'&skabupaten='.$request->skabupaten.'&skecamatan='.$request->skecamatan.'&sdesa='.$request->sdesa.'&action=printskpd');
        }else{
			$data['printSkpd']     = url($this->uri.'?syears='.$tahunSekarang.'&skabupaten='.$request->skabupaten.'&skecamatan='.$request->skecamatan.'&sdesa='.$request->sdesa.'&action=printskpd');
        }
		if($request->action=='printmasyarakat') {
			if(count($data['masyarakat'])>0){
				$pdf = PDF::loadView($this->folder.'.print_masyarakat',['masyarakat'=>$qM->get()])->setOrientation('landscape');
				return $pdf->inline('usulan_masyarakat.pdf');
			}else{
				return 'data kosong';
			}
		}elseif($request->action=='printskpd'){
			if(count($usulanMap)>0){
				$pdf = PDF::loadView($this->folder.'.print_skpd',['skpd'=>$usulanMap]);
				return $pdf->inline('usulan_skpd.pdf');
			}else{
				return 'data kosong';
			}
		}else{
	        return view($this->folder.'.index',$data);
		}
    }

    public function getPrint(Request $request)
    {
    	$tahunSekarang = date('Y')+1;
		$qM    = M\UsulanMasyarakat::orderBy('created_at','desc');
		if(isset($request->years)){
			$qM = $qM->where('years',$request->years);
			// $qM = $qM->whereRaw('year(created_at) = '.$request->years);
		}else{
			$qM = $qM->where('years',$tahunSekarang);
			// $qM = $qM->whereRaw('year(created_at) = '.$tahunSekarang);
		}

		if(!empty($request->mkabupaten)){
			$qM = $qM->where('id_kabupaten',$request->mkabupaten);
		}
		
		if(!empty($request->mkecamatan)){
			$qM = $qM->where('id_kecamatan',$request->mkecamatan);
		}

		if(!empty($request->mdesa)){
			$qM = $qM->where('id_desa',$request->mdesa);
		}

		$data['masyarakat'] = $qM->get();
    }

    public function postProgram(Request $request)
    {
        if($request->ajax()){
            if(!empty($request->id_program)){

                $kegiatan = M\Kegiatan::orderBy('id','desc')->where('id_program',$request->id_program)->get();
                $data[] = '<option value="">- Pilih Kegiatan -</option>';
                foreach ($kegiatan as $key => $value) {
                    $data[] = '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
                return $data;
            }else{
                $data[] ='<option value="">- Pilih Kegiatan -</option>';
                return $data;
            }
        }
    }

    public function postTahun(Request $request)
    {
        if($request->ajax()){
            if(!empty($request->tahun)){

                $tahun = M\Program::orderBy('id','desc')->where('years',$request->tahun)->get();
                $data[] = '<option value="">- Pilih Program -</option>';
                foreach ($tahun as $key => $value) {
                    $data[] = '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
                return $data;
            }
        }
    }
}
