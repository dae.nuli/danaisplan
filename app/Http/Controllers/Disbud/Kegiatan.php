<?php

namespace App\Http\Controllers\Disbud;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M;

class Kegiatan extends Controller
{
    protected $folder   = 'disbud.kegiatan';
    protected $title    = 'Kegiatan';
    protected $uri      = 'disbud/kegiatan';
    protected $template = 'disbud.layout.content';
    protected $limit    = 10;

    public function index(Request $request)
    {
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['limit']    = $this->limit;
        $data['pages']    = $request->page;
        $data['index']    = M\Kegiatan::orderBy('created_at','desc')->paginate($this->limit);
        $data['uri']      = $this->uri;
        return view($this->folder.'.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['uri']      = $this->uri;
        return view($this->folder.'.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'program'       => 'required',
			'kode_kegiatan' => 'required',
			'nama'          => 'required',
        ]);

        // if ($validator->fails()) {
        //     return redirect()
        //         ->back()
        //         ->withInput()
        //         ->withErrors($validator);
        // }

        $var = new M\Kegiatan;
		$var->id            = Uuid::uuid4()->getHex();
		$var->id_program    = $request->program;
		$var->code_kegiatan = $request->kode_kegiatan;
		$var->name          = $request->nama;
		$var->description   = $request->keterangan;
        $var->save();

        return redirect($this->uri)->with('success','');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return M\Kegiatan::findOrFail(10);
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['template'] = $this->template;
        $data['index']    = M\Kegiatan::findOrFail($id);
        $data['uri']      = $this->uri;
        return view($this->folder.'.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $validator = Validator::make($request->all(), [
        //     'program'      => 'required',
        //     'kegiatan'     => 'required',
        //     'sub_kegiatan' => 'required',
        //     'anggaran'     => 'required',
        //     'catatan'      => 'required',
        // ]);

        // if ($validator->fails()) {
        //     return redirect()
        //         ->back()
        //         ->withInput()
        //         ->withErrors($validator);
        // }

        $this->validate($request, [
			'program'       => 'required',
			'kode_kegiatan' => 'required',
			'nama'          => 'required',
        ]);

        $var = M\Kegiatan::findOrFail($id);
		$var->id_program    = $request->program;
		$var->code_kegiatan = $request->kode_kegiatan;
		$var->name          = $request->nama;
		$var->description   = $request->keterangan;
        $var->save();

        return redirect($this->uri);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // M\UsulanSkpd::findOrFail($id);
    }
}
