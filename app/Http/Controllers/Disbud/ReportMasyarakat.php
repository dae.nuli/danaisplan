<?php

namespace App\Http\Controllers\Disbud;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M, Excel;
use Ramsey\Uuid\Uuid;

class ReportMasyarakat extends Controller
{
	protected $folder      = 'disbud.report_masyarakat';
	protected $title       = 'Laporan Masyarakat';
	protected $template    = 'disbud.layout.content';
	protected $uri         = 'disbud/reportMasyarakat';
	protected $limit       = 10;
	protected $created_msg = 'Created';
	protected $updated_msg = 'Updated';
	protected $deleted_msg = 'Deleted';

    public function getIndex(Request $request)
    {
        $firstYear = M\UsulanMasyarakat::select(\DB::raw('year(created_at) as year'))
                         ->orderBy('created_at','asc')
                         ->take(1)->first();
        $data['tahun']    = range(isset($firstYear->year)?$firstYear->year:date('Y'),date('Y')+2);
        $year             = (session('myears'))?session('myears'):date('Y');



        $data['year']      = $year;
        $data['template']  = $this->template;
        $data['title']     = $this->title;
        $data['limit']     = $this->limit;
        $data['pages']     = $request->page;
        
        $data['kabupaten'] = M\KabupatenModel::orderBy('id','desc')->get();

        $kabSelected         = (session('kabupaten'))?session('kabupaten'):'';
        $data['kabSelected'] = $kabSelected;

        if(!empty($kabSelected)){
            $data['kecamatan'] = M\KecamatanModel::where('kabupaten_id',$kabSelected)->orderBy('id','desc')->get();
        }

        $kecSelected         = (session('kecamatan'))?session('kecamatan'):'';
        $data['kecSelected'] = $kecSelected;

        if(!empty($kecSelected)){
            $data['desa'] = M\DesaModel::where('kecamatan_id',$kecSelected)->orderBy('id','desc')->get();
        }

        $desSelected         = (session('desa'))?session('desa'):'';
        $data['desSelected'] = $desSelected;

        $index    = M\UsulanMasyarakat::orderBy('created_at','desc');
        if(!empty($kabSelected)){
            $index = $index->where('id_kabupaten',$kabSelected);
        }
        if(!empty($kecSelected)){
            $index = $index->where('id_kecamatan',$kecSelected);
        }
        if(!empty($desSelected)){
            $index = $index->where('id_desa',$desSelected);
        }
        $data['index']    = $index->whereRaw('year(created_at) = '.$year)->paginate($this->limit);

        $data['uri']             = $this->uri;
        $data['action']          = $this->uri.'/filter';
        $data['actionKabupaten'] = $this->uri.'/filter-kabupaten';
        $data['actionKecamatan'] = $this->uri.'/filter-kecamatan';
        $data['actionDesa']      = $this->uri.'/filter-desa';
        return view($this->folder.'.index',$data);
    }

    public function getDetail($id)
    {
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $detail           = M\UsulanMasyarakat::find($id);
        $data['detail']   = $detail;
        return view($this->folder.'.detail',$data);
    }

    public function postFilter(Request $request)
    {
        $year = $request->year;
        if(!empty($year)){
            session()->put('myears',$year);
        }
        return redirect()->back();
    }
    public function postFilterKabupaten(Request $request)
    {
        $kabupaten = $request->kabupaten;
        if(!empty($kabupaten)){
            session()->put('kabupaten',$kabupaten);
        }else{
            session()->forget('kabupaten');
            session()->forget('kecamatan');
            session()->forget('desa');
        }
        return redirect()->back();
    }
    public function postFilterKecamatan(Request $request)
    {
        $kecamatan = $request->kecamatan;
        if(!empty($kecamatan)){
            session()->put('kecamatan',$kecamatan);
        }else{
            session()->forget('kecamatan');
            session()->forget('desa');
        }
        return redirect()->back();
    }
    public function postFilterDesa(Request $request)
    {
        $desa = $request->desa;
        if(!empty($desa)){
            session()->put('desa',$desa);
        }else{
            session()->forget('desa');
        }
        return redirect()->back();
    }

    public function getSummary()
    {
        Excel::create('Download-Summary-Usulan_Masyarakat', function($excel) {
            $excel->sheet('summary', function($sheet) {
            $year        = (session('myears'))?session('myears'):date('Y');
            
            $kabSelected = (session('kabupaten'))?session('kabupaten'):'';
            $kecSelected = (session('kecamatan'))?session('kecamatan'):'';
            $desSelected = (session('desa'))?session('desa'):'';

            $index    = M\UsulanMasyarakat::orderBy('created_at','desc');
            if(!empty($kabSelected)){
                $index = $index->where('id_kabupaten',$kabSelected);
            }
            if(!empty($kecSelected)){
                $index = $index->where('id_kecamatan',$kecSelected);
            }
            if(!empty($desSelected)){
                $index = $index->where('id_desa',$desSelected);
            }
            $data['index']    = $index->whereRaw('year(created_at) = '.$year)->get();

            // $data['index']    = M\UsulanMasyarakat::orderBy('created_at','desc')->whereRaw('year(created_at) = '.$year)->get();
                $sheet->setOrientation('landscape');
                $sheet->loadView($this->folder.'.download_summary',$data);
            });
        })->download('xlsx');

    }

    public function getFull()
    {
        Excel::create('Download-Full-Usulan_Masyarakat', function($excel) {
            $excel->sheet('full', function($sheet) {
                $sheet->setOrientation('landscape');
                $sheet->mergeCells('B1:F1');
                $sheet->mergeCells('G1:I1');
                $sheet->mergeCells('J1:J2');
                $sheet->mergeCells('A1:A2');
                $sheet->mergeCells('K1:K2');
                $sheet->mergeCells('L1:L2');
            
                $year             = (session('myears'))?session('myears'):date('Y');
                // $data['index']    = M\UsulanMasyarakat::orderBy('created_at','desc')->whereRaw('year(created_at) = '.$year)->get();
                $kabSelected = (session('kabupaten'))?session('kabupaten'):'';
                $kecSelected = (session('kecamatan'))?session('kecamatan'):'';
                $desSelected = (session('desa'))?session('desa'):'';
                $index    = M\UsulanMasyarakat::orderBy('created_at','desc');
                if(!empty($kabSelected)){
                    $index = $index->where('id_kabupaten',$kabSelected);
                }
                if(!empty($kecSelected)){
                    $index = $index->where('id_kecamatan',$kecSelected);
                }
                if(!empty($desSelected)){
                    $index = $index->where('id_desa',$desSelected);
                }
                $data['index']    = $index->whereRaw('year(created_at) = '.$year)->get();
                
                $sheet->setOrientation('landscape');
                $sheet->loadView($this->folder.'.download_full',$data);
            });
        })->download('xlsx');

    }

}
