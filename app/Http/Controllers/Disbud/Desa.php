<?php

namespace App\Http\Controllers\Disbud;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M;

class Desa extends Controller
{
    protected $folder      = 'disbud.desa';
    protected $title       = 'Desa';
    protected $uri         = 'disbud/desa';
    protected $template    = 'disbud.layout.content';
    protected $limit       = 20;
    protected $created_msg = 'Created';
    protected $updated_msg = 'Updated';
    protected $deleted_msg = 'Deleted';

    public function getIndex(Request $request)
    {
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['limit']    = $this->limit;
        $data['pages']    = $request->page;
        $data['index']    = M\DesaModel::orderBy('id','desc')->paginate($this->limit);
        $data['uri']      = $this->uri;
        return view($this->folder.'.index',$data);
    }

    public function getEdit(Request $request, $id)
    {
        $data['index']    = M\DesaModel::findOrFail($id);
        $data['pages']    = $request->page;
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['uri']      = $this->uri;
        $data['action']   = url($this->uri.'/update/'.$id.'?page='.$request->page);
        return view($this->folder.'.create',$data);
    }

    public function postUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'lat'    => 'required',
            'lon'    => 'required'
        ]);

        // return $request->page;

        $prog = M\DesaModel::findOrFail($id);
        $prog->latitude  = $request->lat;
        $prog->longitude = $request->lon;
        $prog->save();

        if($request->page){
            return redirect($this->uri.'?page='.$request->page)->with('success',$this->updated_msg);
        }else{
            return redirect($this->uri)->with('success',$this->updated_msg);
        }
    }
}
