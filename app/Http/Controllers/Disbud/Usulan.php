<?php

namespace App\Http\Controllers\Disbud;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M, Mail;

class Usulan extends Controller
{
    protected $folder   = 'disbud.usulan';
    protected $title    = 'Usulan SKPD';
    protected $uri      = 'disbud/usulan';
    protected $template = 'disbud.layout.content';
    protected $limit    = 10;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['limit']    = $this->limit;
        $data['pages']    = $request->page;
        $data['index']    = M\UsulanSkpd::orderBy('created_at','desc')->paginate($this->limit);
        $data['uri']      = $this->uri;
        return view($this->folder.'.index',$data);
    }

    public function getDetail($id)
    {
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $usulan           = M\UsulanSkpd::findOrFail($id);
        $data['usulan']   = $usulan;
        $data['detail']   = M\UsulanSkpdDetail::where('id_usulan',$id)->get();
        $data['program']  = M\Program::all();
        $data['kegiatan'] = M\Kegiatan::all();
        $data['uri']      = $this->uri;
        $data['action']   = url($this->uri.'/detail/'.$id);
        $data['tor']      = '/storage/tor/'.$usulan->tor;
        $data['rab']      = '/storage/rab/'.$usulan->rab;
        return view($this->folder.'.detail',$data);
    }

    // public function show($id)
    // {
    //     $data['usulan']   = M\UsulanSkpd::findOrFail($id);
    //     $data['detail']   = M\UsulanSkpdDetail::where('id_usulan',$id)->get();
    //     $data['template'] = $this->template;
    //     $data['title']    = $this->title;
    //     $data['uri']      = $this->uri;
    //     return view($this->folder.'.detail', $data);
    // }

    public function postDetail(Request $request, $id)
    {

        
        if($request->type=='approve'){

	        $usul = M\UsulanSkpd::findOrFail($id);
			$usul->revision_note = $request->catatan_revisi;
			$usul->is_approved   = 1;
	        $usul->save();

            $data['pesan'] = 'Usulan anda telah diterima';
            Mail::queue('welcome', $data, function ($message) use($usul) {
                $message->to($usul->email)->subject('Informasi Usulan Dana Istimewa');
            });

        }elseif($request->type=='denied'){

	        $usul = M\UsulanSkpd::findOrFail($id);
			$usul->revision_note = $request->catatan_revisi;
			$usul->is_approved   = 3;
	        $usul->save();

            $data['pesan'] = 'Usulan anda ditolak';
            Mail::queue('welcome', $data, function ($message) use($usul) {
                $message->to($usul->email)->subject('Informasi Usulan Dana Istimewa');
            });

        }elseif($request->type=='revision'){
	        
	        $this->validate($request, [
	            'catatan_revisi'      => 'required',
	        ]);

	        $usul = M\UsulanSkpd::findOrFail($id);
			$usul->revision_note = $request->catatan_revisi;
			$usul->is_approved   = 2;
	        $usul->save();

            $data['pesan'] = 'Usulan anda direvisi dengan pesan : '.$$request->catatan_revisi;
            Mail::queue('welcome', $data, function ($message) use($usul) {
                $message->to($usul->email)->subject('Informasi Usulan Dana Istimewa');
            });
	        
        }

        return redirect($this->uri);
    }

    public function mapmodal($index,$latlng=null)
    {
        $data['index']  = $index;
        $data['latlng'] = ($latlng)?explode(',', $latlng):NULL;
        $data['ll']     = ($latlng)?$latlng:NULL;
        $data['uri']    = $this->uri;
        return view($this->folder.'.modal_map',$data);
    }
}
