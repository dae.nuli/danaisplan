<?php

namespace App\Http\Controllers\Disbud;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M, File, Storage, Session, Avatar;

class Profile extends Controller
{
	protected $folder      = 'disbud.profile';
	protected $title       = 'Profil';
	protected $uri         = 'disbud/profile';
	protected $home        = 'disbud/home';
	protected $template    = 'disbud.layout.content';
	protected $limit       = 20;
	protected $created_msg = 'Silahkan cek email untuk melakukan aktivasi akun';
	protected $updated_msg = 'Updated';
	protected $deleted_msg = 'Deleted';
	protected $activation  = 'Akun anda telah aktif, silahkan login';
    protected $users;
    
    public function __construct()
    {
        $this->users = session('loginDisbud')['id'];
    }
    public function getIndex()
    {
		$data['template'] = $this->template;
		$data['title']    = $this->title;
		$data['action']   = url($this->uri);
		$data['uri']      = $this->uri;
		$data['index']    = M\Disbud::findOrFail($this->users);
        return view($this->folder.'.index',$data);
    }

    public function postIndex(Request $request)
    {
        $this->validate($request, [
			'name'    => 'required',
			// 'email'   => 'required|email|unique:disbud,email,'.$this->users,
			'phone'   => 'required|numeric',
			'address' => 'required',
        ]);

        $var = M\Disbud::findOrFail($this->users);
		// $var->email         = $request->email;
		$var->name    = $request->name;
		$var->address = $request->address;
		$var->phone   = $request->phone;
		// if($var->name!=$request->name){
			// $random      = str_random(20);

			// $var->avatar = $random.'.png';
		// }
        if(!empty($request->password)){
	        $var->password      = bcrypt($request->password);
        }
        $var->save();
	    Avatar::create(strtoupper($var->name))->save('storage/avatar/disbud/'.$var->avatar);

        return redirect($this->uri)->with('success',$this->updated_msg);
    }
}
