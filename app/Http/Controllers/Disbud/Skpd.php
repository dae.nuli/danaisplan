<?php

namespace App\Http\Controllers\Disbud;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M, Mail, Avatar;
use Ramsey\Uuid\Uuid;

class Skpd extends Controller
{
    protected $folder         = 'disbud.skpd';
    protected $title          = 'SKPD';
    protected $uri            = 'disbud/skpd';
    protected $template       = 'disbud.layout.content';
    protected $limit          = 10;
    protected $created_msg    = 'Created';
    protected $updated_msg    = 'Updated';
    protected $deleted_msg    = 'Deleted';
    protected $reset_msg      = 'Reseted';
    protected $noactive_msg   = 'SKPD harus diaktifkan terlebih dahulu';
    protected $activation_msg = 'Data akun SKPD telah dikirim';
    protected $disable_msg    = 'SKDP telah di-non aktifkan';
    protected $enable_msg     = 'SKDP telah di-aktif aktifkan';



    public function getIndex(Request $request)
    {
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['limit']    = $this->limit;
        $data['pages']    = $request->page;
        $data['index']    = M\Skpd::orderBy('created_at','desc')->paginate($this->limit);
        $data['uri']      = $this->uri;
        return view($this->folder.'.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['uri']      = $this->uri;
        $data['action']   = url($this->uri.'/store');
        return view($this->folder.'.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        $this->validate($request, [
            'kode_skpd'     => 'required|unique:skpd,code',
            'nama_instansi' => 'required',
            'email'         => 'required|email|unique:skpd,email',
            'telefon'       => 'numeric'
        ]);

        $random = str_random(20);
        $user   = explode('@', $request->email);
        $var = new M\Skpd;
        $var->id            = Uuid::uuid4()->getHex();
        $var->code          = $request->kode_skpd;
        $var->instansi_name = $request->nama_instansi;
        $var->email         = $request->email;
        $var->email_staff   = ($request->email_staff)?$request->email_staff:NULL;
        $var->username      = $user[0];
        $var->phone         = ($request->telefon)?$request->telefon:NULL;
        $var->address       = ($request->alamat)?$request->alamat:NULL;
        $var->personil_name = ($request->nama_penanggung_jawab)?$request->nama_penanggung_jawab:NULL;
        $var->password      = ($request->password)?bcrypt($request->password):NULL;
        $var->avatar        = $random.'.png';
        $var->save();
        Avatar::create(strtoupper($var->instansi_name))->save('storage/avatar/skpd/'.$var->avatar);

        return redirect($this->uri)->with('success',$this->created_msg);
    }

    public function getShow($id)
    {
        $data['title']    = $this->title;
        $data['template'] = $this->template;
        $data['show'] = M\Skpd::findOrFail($id);
        $data['uri']      = $this->uri;
        return view($this->folder.'.show',$data);
    }

    public function getEdit($id)
    {
        $data['template'] = $this->template;
        $data['index']    = M\Skpd::findOrFail($id);
        $data['title']    = $this->title;
        $data['uri']      = $this->uri;
        $data['action']   = url($this->uri.'/update/'.$id);
        return view($this->folder.'.create',$data);
    }

    public function postUpdate(Request $request, $id)
    {

        $this->validate($request, [
            'kode_skpd'     => 'required|unique:skpd,code,'.$id,
            'email'         => 'required|email|unique:skpd,email,'.$id,
            'email_staff'   => 'email|unique:skpd,email_staff,'.$id,
            'nama_instansi' => 'required',
            'telefon'       => 'numeric'
        ]);

        $user   = explode('@', $request->email);
        $var = M\Skpd::findOrFail($id);
        $var->code          = $request->kode_skpd;
        $var->instansi_name = $request->nama_instansi;
        $var->email         = $request->email;
        $var->email_staff   = ($request->email_staff)?$request->email_staff:NULL;
        $var->username      = $user[0];
        $var->phone         = ($request->telefon)?$request->telefon:NULL;
        $var->address       = ($request->alamat)?$request->alamat:NULL;
        $var->personil_name = ($request->nama_penanggung_jawab)?$request->nama_penanggung_jawab:NULL;
        $var->password      = ($request->password)?bcrypt($request->password):NULL;
        $var->save();

        Avatar::create(strtoupper($var->instansi_name))->save('storage/avatar/skpd/'.$var->avatar);

        return redirect($this->uri)->with('success',$this->updated_msg);
    }

    public function getFirstActivation($id)
    {
        if(!empty($id)){
            $var = M\Skpd::findOrFail($id);
            if(!empty($var)){
                $random = str_random(7);
                // $pass   = sha1($random.'sha1');

                $var->password  = bcrypt($random);
                $var->is_active = 1;
                $var->save();

                Mail::queue($this->folder.'.first_activation',$data = array('username' => $var->username, 'password' => $random),function($message) use ($var){
                    $message->to($var->email,$var->instansi_name)->subject('Akun SKPD');
                });
                return redirect()->back()->with('success',$this->activation_msg);
            }
        }
    }

    public function getReset($id)
    {
        if(!empty($id)){
            $var = M\Skpd::findOrFail($id);
            if(!empty($var) ){
                if($var->is_active){
                    $user   = explode('@', $var->email);
                    $random = str_random(7);
                    
                    $var->password      = bcrypt($random);
                    $var->save();

                    Mail::queue($this->folder.'.message_reset',$data = array('username' => $user[0], 'password' => $random),function($message) use ($var){
                        $message->to($var->email,$var->instansi_name)->subject('Reset Password');
                    });
                    return redirect()->back()->with('success',$this->reset_msg);
                }else{
                    return redirect()->back()->with('warning',$this->noactive_msg);
                }
            }
        }
    }

    public function getMasaLogin()
    {
        $data['action'] = url($this->uri.'/masa-login');
        $data['index']  = M\MasaLogin::findOrFail(1);
        return view($this->folder.'.modal_masa_login',$data);
    }

    public function postMasaLogin(Request $request)
    {
        $this->validate($request, [
            'start' => 'required',
            'end'   => 'required'
        ]);

        $var = M\MasaLogin::findOrFail(1);
        $var->start_date = ($request->start)?$request->start:NULL;
        $var->end_date   = ($request->end)?$request->end:NULL;
        $var->save();

        return redirect($this->uri)->with('success',$this->updated_msg);
    }

    public function getDisable($id)
    {
        if(!empty($id)){
            $var = M\Skpd::findOrFail($id);
            if(count($var)>0){
                $var->is_active = 0;
                $var->save();
                return redirect()->back()->with('success',$this->disable_msg);
            }
        }
    }

    public function getEnable($id)
    {
        if(!empty($id)){
            $var = M\Skpd::findOrFail($id);
            if(count($var)>0){
                $var->is_active = 1;
                $var->save();
                return redirect()->back()->with('success',$this->enable_msg);
            }
        }
    }
    public function getDestroy($id)
    {
        $var = M\Skpd::findOrFail($id);
        if(!empty($var->avatar)){
            Storage::delete('avatar/skpd/'.$var->avatar);
        }
        $var->delete();
        return redirect()->back()->with('success',$this->deleted_msg);
    }
}
