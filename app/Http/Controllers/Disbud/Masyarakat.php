<?php

namespace App\Http\Controllers\Disbud;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M, Mail, Storage;

class Masyarakat extends Controller
{
    protected $folder       = 'disbud.masyarakat';
    protected $title        = 'Usulan Masyarakat';
    protected $uri          = 'disbud/masyarakat';
    protected $template     = 'disbud.layout.content';
    protected $limit        = 10;
    protected $approved_msg = 'Approved';
    protected $denied_msg   = 'Denied';
    protected $deleted_msg  = 'Deleted';

    public function getIndex(Request $request)
    {
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['limit']    = $this->limit;
        $data['pages']    = $request->page;
        $data['index']    = M\UsulanMasyarakat::orderBy('created_at','desc')->paginate($this->limit);
        $data['uri']      = $this->uri;

        return view($this->folder.'.index',$data);
    }

    public function getDetail($id)
    {
        $data['action']   = url($this->uri.'/detail/'.$id);
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['skpd']     = M\Skpd::orderBy('instansi_name','asc')->get();
        $detail           = M\UsulanMasyarakat::find($id);
        $data['detail']   = $detail;
        $data['uri']      = $this->uri;
        // return Storage::size('public/masyarakat/'.$detail->document);
        $data['pdf']      = '/storage/masyarakat/'.$detail->document;
        $data['size']     = $this->bytesToHuman(Storage::size('masyarakat/'.$detail->document));
        return view($this->folder.'.detail',$data);
    }

    public function getDownload($doc)
    {
        return response()->download(storage_path('app/public/masyarakat/'.$doc));
    }

    public function bytesToHuman($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public function postDetail(Request $request, $id)
    {
        // dd($request->all());
        // return;

        if($request->type=='approve'){
            $this->validate($request, [
                'skpd'      => 'required',
            ]);

            $usul = M\UsulanMasyarakat::findOrFail($id);
            $usul->status  = 1;
            $usul->id_skpd = $request->skpd;
            $usul->assign  = 1;
            $usul->save();
            
            $data['pesan'] = 'Usulan anda telah diterima dan akan dialihkan ke skdp <u>'.$usul->skpd->instansi_name.'</u>';
            Mail::queue('welcome', $data, function ($message) use($usul) {
                $message->to($usul->email)->subject('Informasi Usulan');
            });
            return redirect($this->uri)->with('success',$this->approved_msg);
        }elseif($request->type=='denied'){

            $usul = M\UsulanMasyarakat::findOrFail($id);
            $usul->status  = 2;
            $usul->id_skpd = $request->skpd;
            $usul->assign  = 1;
            $usul->save();

            $data['pesan'] = 'Usulan anda ditolak';
            Mail::queue('welcome', $data, function ($message) use($usul) {
                $message->to($usul->email)->subject('Informasi Usulan');
            });
            return redirect($this->uri)->with('success',$this->denied_msg);
        }

    }

    public function getDestroy($id)
    {
        $um = M\UsulanMasyarakat::findOrFail($id);
        Storage::delete('masyarakat/'.$um->document);
        $um->delete();
        return redirect($this->uri)->with('success',$this->deleted_msg);
    }
}
