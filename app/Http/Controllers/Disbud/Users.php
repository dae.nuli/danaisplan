<?php

namespace App\Http\Controllers\Disbud;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M, Avatar;
use Ramsey\Uuid\Uuid;

class Users extends Controller
{
    protected $folder      = 'disbud.users';
    protected $title       = 'Users';
    protected $uri         = 'disbud/users';
    protected $template    = 'disbud.layout.content';
    protected $limit       = 10;
    protected $created_msg = 'Created';
    protected $updated_msg = 'Updated';
    protected $deleted_msg = 'Deleted';
    protected $disable_msg = 'Disabled';
    protected $enable_msg  = 'Enabled';


    public function __construct()
    {
        $this->users = session('loginDisbud')['id'];
    }

    public function getIndex(Request $request)
    {
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['limit']    = $this->limit;
        $data['pages']    = $request->page;
        $data['index']    = M\Disbud::orderBy('created_at','desc')->paginate($this->limit);
        $data['uri']      = $this->uri;
        return view($this->folder.'.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(Request $request)
    {
        $data['template']  = $this->template;
        $data['title']     = $this->title;
        $data['uri']       = $this->uri;
        $data['action']    = url($this->uri.'/store');
        return view($this->folder.'.create', $data);
    }

	public function postStore(Request $request)
    {
        $this->validate($request, [
			'name'     => 'required',
			'email'    => 'required|email|unique:disbud,email',
            'password' => 'required',
			'address' => 'required',
			'phone'    => 'required'
        ]);

		$random      = str_random(20);
        $var = new M\Disbud;
		$var->id       = Uuid::uuid4()->getHex();
		$var->name     = $request->name;
		$var->email    = $request->email;
		$var->phone    = $request->phone;
		$var->password = bcrypt($request->password);
		$var->address  = ($request->address)?$request->address:NULL;
		$var->avatar   = $random.'.png';
        $var->save();
	    Avatar::create(strtoupper($var->name))->save('storage/avatar/disbud/'.$var->avatar);

        return redirect($this->uri)->with('success',$this->created_msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getShow($id)
    {
        $data['detail']   = M\Disbud::findOrFail($id);
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['uri']      = $this->uri;
        return view($this->folder.'.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        $data['index']     = M\Disbud::findOrFail($id);
        $data['template']  = $this->template;
        $data['title']     = $this->title;
        $data['uri']       = $this->uri;
        $data['action']    = url($this->uri.'/update/'.$id);
        return view($this->folder.'.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request, $id)
    {
        $val['name']     = 'required';
        $val['email']    = 'required|email|unique:disbud,email,'.$id;
        $val['phone']    = 'required';
        $val['address']    = 'required';
        $this->validate($request, $val);

		// $random      = str_random(20);
        $var = M\Disbud::findOrFail($id);
		$var->name     = $request->name;
		$var->email    = $request->email;
		$var->phone    = $request->phone;
        if(!empty($request->password)){
            $var->password = bcrypt($request->password);
        }
		$var->address  = ($request->address)?$request->address:NULL;
		// $var->avatar   = $random.'.png';
        $var->save();

	    Avatar::create(strtoupper($var->name))->save('storage/avatar/disbud/'.$var->avatar);

        return redirect($this->uri)->with('success',$this->updated_msg);
    }

    public function getDisable($id)
    {
        if(!empty($id)){
            $var = M\Disbud::findOrFail($id);
            if(count($var)>0){
                $var->is_active = 0;
                $var->save();
                return redirect()->back()->with('success',$this->disable_msg);
            }
        }
    }

    public function getEnable($id)
    {
        if(!empty($id)){
            $var = M\Disbud::findOrFail($id);
            if(count($var)>0){
                $var->is_active = 1;
                $var->save();
                return redirect()->back()->with('success',$this->enable_msg);
            }
        }
    }

    public function getDestroy($id)
    {
        $var = M\Disbud::findOrFail($id);
        if(!empty($var->avatar)){
            Storage::delete('avatar/disbud/'.$var->avatar);
        }
        $var->delete();
        return redirect($this->uri)->with('success',$this->deleted_msg);
    }
}
