<?php

namespace App\Http\Controllers\Disbud;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M;

class Program extends Controller
{
    protected $folder      = 'disbud.program';
    protected $title       = 'Program';
    protected $uri         = 'disbud/program';
    protected $template    = 'disbud.layout.content';
    protected $limit       = 10;
    protected $created_msg = 'Created';
    protected $updated_msg = 'Updated';
    protected $deleted_msg = 'Deleted';

    public function getIndex(Request $request)
    {
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['limit']    = $this->limit;
        $data['pages']    = $request->page;
        $data['index']    = M\Program::orderBy('created_at','desc')->paginate($this->limit);
        $data['uri']      = $this->uri;
        return view($this->folder.'.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate(Request $request)
    {
        $data['template']  = $this->template;
        $data['title']     = $this->title;
        $data['uri']       = $this->uri;
        $data['action']    = url($this->uri.'/store');
        $data['yearRange'] = range(date('Y'),date('Y')+2);
        $data['import']    = url($this->uri.'/modal-import-program');

        if(!empty($request->program)){
            $data['program']  = M\Program::findOrFail($request->program);
            $data['kegiatan'] = M\Kegiatan::where('id_program',$request->program)->get();
            return view($this->folder.'.create_import', $data);
        }else{
            return view($this->folder.'.create', $data);
        }

    }

    public function getModalImportProgram()
    {
        $data['action']  = url($this->uri.'/modal-import-program');
        $data['program'] = M\Program::orderBy('id','asc')->get();
        return view($this->folder.'.modal_import',$data);
    }

    public function postModalImportProgram(Request $request)
    {
        $this->validate($request, [
            'program' => 'required'
        ]);
        $program = $request->program;
        return redirect($this->uri.'/create?program='.$program);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        // dd($request->all());
        // return;
        $this->validate($request, [
            'program_code'    => 'required|unique:program,code_program',
            'program_name'    => 'required',
            'program_years'   => 'required',
            'kegiatan_code.*' => 'required',
            'kegiatan_name.*' => 'required'
        ]);

        $prog = new M\Program;
		$prog->code_program = $request->program_code;
		$prog->name         = $request->program_name;
		$prog->years        = $request->program_years;
        $prog->save();

        // $kegiatanName = $request->kegiatan_name;
        // if(!empty($kegiatanName)){
        foreach ($request->kegiatan_name as $key => $value) {
            $km = new M\Kegiatan;
            $km->id_program    = $prog->id;
            $km->code_kegiatan = $request->kegiatan_code[$key];
            $km->name          = $value;
            $km->description   = ($request->description[$key]?$request->description[$key]:NULL);
            $km->save();
        }
        // }

        return redirect($this->uri)->with('success',$this->created_msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDetail($id)
    {
        $data['detail']   = M\Program::findOrFail($id);
        $data['item']     = M\Kegiatan::where('id_program',$id)->get();
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['uri']      = $this->uri;
        return view($this->folder.'.detail',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        $data['index']     = M\Program::findOrFail($id);
        $data['item']      = M\Kegiatan::where('id_program',$id)->get();
        $data['template']  = $this->template;
        $data['title']     = $this->title;
        $data['uri']       = $this->uri;
        $data['action']    = url($this->uri.'/update/'.$id);
        $data['yearRange'] = range(date('Y'),date('Y')+2);
        return view($this->folder.'.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request, $id)
    {
        $this->validate($request, [
            'program_code'    => 'required|unique:program,code_program,'.$id,
            'program_name'    => 'required',
            'program_years'   => 'required',
            'kegiatan_code.*' => 'required',
            'kegiatan_name.*' => 'required'
        ]);

        $prog = M\Program::findOrFail($id);
        $prog->code_program = $request->program_code;
        $prog->name         = $request->program_name;
        $prog->years        = $request->program_years;
        $prog->save();

        foreach ($request->kegiatan_name as $key => $value) {
            if(isset($request->item_id[$key])) {
                $km = M\Kegiatan::findOrFail($request->item_id[$key]);
            }else{
                $km = new M\Kegiatan;
                $km->id_program = $id;
            }

            $km->code_kegiatan = $request->kegiatan_code[$key];
            $km->name          = $request->kegiatan_name[$key];
            $km->description   = ($request->description[$key]?$request->description[$key]:NULL);
            $km->save();
        }
        return redirect($this->uri)->with('success',$this->updated_msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDeleteItem($id){
        M\Kegiatan::findOrFail($id)->delete();
        return redirect()->back()->with('success',$this->deleted_msg);
    }
    
    public function getDelete($id)
    {
        M\Program::findOrFail($id)->delete();
        M\Kegiatan::where('id_program',$id)->delete();
        return redirect($this->uri)->with('success',$this->deleted_msg);
    }
}
