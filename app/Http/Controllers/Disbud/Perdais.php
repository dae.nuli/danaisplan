<?php

namespace App\Http\Controllers\Disbud;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M;
use Ramsey\Uuid\Uuid;

class Perdais extends Controller
{
	protected $folder      = 'disbud.prioritas';
	protected $title       = 'Perdais';
	protected $template    = 'disbud.layout.content';
	protected $uri         = 'disbud/perdais';
	protected $limit       = 10;
	protected $created_msg = 'Created';
	protected $updated_msg = 'Updated';
	protected $deleted_msg = 'Deleted';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {
		$data['template'] = $this->template;
		$data['title']    = $this->title;
		$data['limit']    = $this->limit;
		$data['pages']    = $request->page;
		$data['index']    = M\Perdais::orderBy('created_at','desc')->paginate($this->limit);
		$data['uri']      = $this->uri;
        return view($this->folder.'.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
		$data['template'] = $this->template;
		$data['title']    = $this->title;
		$data['uri']      = $this->uri;
		$data['action']   = url($this->uri.'/store');
        return view($this->folder.'.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
 
        $sto = new M\Perdais;
		// $sto->slug = str_slug($request->name);
		$sto->name = $request->name;
		$sto->note = $request->note;
        $sto->save();

        return redirect($this->uri)->with('success',$this->created_msg);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
		$data['template'] = $this->template;
		$data['index']    = M\Perdais::findOrFail($id);
		$data['title']    = $this->title;
		$data['action']   = url($this->uri.'/edit/'.$id);
		$data['uri']      = $this->uri;
        return view($this->folder.'.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request, $id)
    {
        $this->validate($request, [
            'name'     => 'required'
        ]);

        $upd = M\Perdais::findOrFail($id);
		// $upd->slug = str_slug($request->name);
		$upd->name = $request->name;
		$upd->note = $request->note;
        $upd->save();

        return redirect($this->uri)->with('success',$this->updated_msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDestroy($id)
    {
        M\Perdais::findOrFail($id)->delete();
        return redirect($this->uri)->with('success',$this->deleted_msg);
    }

}
