<?php

namespace App\Http\Controllers\Disbud;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M;

class Pengumuman extends Controller
{
	protected $folder      = 'disbud.pengumuman';
	protected $title       = 'Pengumuman';
	protected $template    = 'disbud.layout.content';
	protected $uri         = 'disbud/pengumuman';
	protected $limit       = 10;
	protected $created_msg = 'Created';
	protected $updated_msg = 'Updated';
	protected $deleted_msg = 'Deleted';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {
		$data['template'] = $this->template;
		$data['title']    = $this->title;
		$data['limit']    = $this->limit;
		$data['pages']    = $request->page;
		$data['index']    = M\Pengumuman::orderBy('created_at','desc')->paginate($this->limit);
		$data['uri']      = $this->uri;
        return view($this->folder.'.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
		$data['template'] = $this->template;
		$data['title']    = $this->title;
		$data['uri']      = $this->uri;
		$data['action']   = url($this->uri.'/store');
        return view($this->folder.'.create', $data);
    }

    public function getDetail($id)
    {
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['uri']      = $this->uri;
        $data['index']    = M\Pengumuman::findOrFail($id);
        return view($this->folder.'.detail', $data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {
        $this->validate($request, [
            'content' => 'required',
            'end'     => 'required'
        ]);
 
        $sto = new M\Pengumuman;
        $sto->content  = $request->content;
        $sto->end_show = $request->end;
        $sto->save();

        return redirect($this->uri)->with('success',$this->created_msg);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
		$data['template'] = $this->template;
		$data['index']    = M\Pengumuman::findOrFail($id);
		$data['title']    = $this->title;
		$data['action']   = url($this->uri.'/edit/'.$id);
		$data['uri']      = $this->uri;
        return view($this->folder.'.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postEdit(Request $request, $id)
    {
        $this->validate($request, [
            'content' => 'required',
            'end'     => 'required'
        ]);

        $upd           = M\Pengumuman::findOrFail($id);
        $upd->content  = $request->content;
        $upd->end_show = $request->end;
        $upd->save();

        return redirect($this->uri)->with('success',$this->updated_msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDestroy($id)
    {
        M\Pengumuman::findOrFail($id)->delete();
        return redirect($this->uri)->with('success',$this->deleted_msg);
    }

}
