<?php

namespace App\Http\Controllers\Disbud;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M, Helper, Excel;
use Ramsey\Uuid\Uuid;

class Report extends Controller
{
	protected $folder      = 'disbud.report';
	protected $title       = 'Laporan SKPD';
	protected $template    = 'disbud.layout.content';
	protected $uri         = 'disbud/report';
	protected $limit       = 10;
	protected $created_msg = 'Created';
	protected $updated_msg = 'Updated';
	protected $deleted_msg = 'Deleted';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(Request $request)
    {
        $firstYear = M\UsulanSkpd::select(\DB::raw('year(created_at) as year'))
                         ->orderBy('created_at','asc')
                         ->take(1)->first();
        // $firstYear        = M\UsulanSkpd::orderBy('created_at','asc')->take(1)->first();
        $data['tahun']           = range(isset($firstYear->year)?$firstYear->year:date('Y'),date('Y')+2);
        $year                    = (session('years'))?session('years'):date('Y');
        $data['year']            = $year;
        $data['template']        = $this->template;
        $data['title']           = $this->title;
        $data['limit']           = $this->limit;
        $data['pages']           = $request->page;
        
        $data['program']         = M\Program::where('years',$year)->orderBy('id','desc')->get();
        $programSelected         = (session('program'))?session('program'):'';
        $data['programSelected'] = $programSelected;
        if(!empty($programSelected)){
            $data['kegiatan']        = M\Kegiatan::where('id_program',$programSelected)->orderBy('id','desc')->get();
        }
        $kegiatanSelected         = (session('kegiatan'))?session('kegiatan'):'';
        $data['kegiatanSelected'] = $kegiatanSelected;

        $data['skpd']         = M\Skpd::orderBy('id','desc')->get();
        $skpdSelected         = (session('skpd'))?session('skpd'):'';
        $data['skpdSelected'] = $skpdSelected;

        $index    = M\UsulanSkpd::orderBy('id_skpd','desc')
                            ->orderBy('id_program','desc')
                            ->orderBy('id_kegiatan','desc');
        if(!empty($programSelected)){
            $index = $index->where('id_program',$programSelected);
        }
        if(!empty($kegiatanSelected)){
            $index = $index->where('id_kegiatan',$kegiatanSelected);
        }
        if(!empty($skpdSelected)){
            $index = $index->where('id_skpd',$skpdSelected);
        }
        $data['index'] = $index->whereRaw('year(created_at) = '.$year)->paginate($this->limit);
        
        // $data['index']    = M\UsulanSkpd::orderBy('created_at','desc')->paginate($this->limit);
        // $vm = M\UsulanSkpd::whereRaw('year(created_at) = '.$year)->paginate($this->limit);        
        $data['uri']            = $this->uri;
        $data['action']         = $this->uri.'/filter';
        $data['actionProgram']  = $this->uri.'/filter-program';
        $data['actionKegiatan'] = $this->uri.'/filter-kegiatan';
        $data['actionSkpd']     = $this->uri.'/filter-skpd';
        return view($this->folder.'.index',$data);
    }

    public function postFilter(Request $request)
    {
        $year = $request->year;
        if(!empty($year)){
            session()->put('years',$year);
            session()->forget('program');
            session()->forget('kegiatan');
        }
        return redirect()->back();
    }

    public function postFilterProgram(Request $request)
    {
        $program = $request->program;
        if(!empty($program)){
            session()->put('program',$program);
        }else{
            session()->forget('program');
            session()->forget('kegiatan');
        }
        return redirect()->back();
    }

    public function postFilterKegiatan(Request $request)
    {
        $kegiatan = $request->kegiatan;
        if(!empty($kegiatan)){
            session()->put('kegiatan',$kegiatan);
        }else{
            session()->forget('kegiatan');
        }
        return redirect()->back();
    }

    public function postFilterSkpd(Request $request)
    {
        $skpd = $request->skpd;
        if(!empty($skpd)){
            session()->put('skpd',$skpd);
        }else{
            session()->forget('skpd');
        }
        return redirect()->back();
    }

    public function getDetail($id)
    {
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $usulan           = M\UsulanSkpd::findOrFail($id);
        $data['usulan']   = $usulan;
        $data['detail']   = M\UsulanSkpdDetail::where('id_usulan',$id)->get();
        $data['program']  = M\Program::all();
        $data['kegiatan'] = M\Kegiatan::all();
        $data['uri']      = $this->uri;
        $data['action']   = url($this->uri.'/detail/'.$id);
        $data['tor']      = '/storage/tor/'.$usulan->tor;
        $data['rab']      = '/storage/rab/'.$usulan->rab;
        return view($this->folder.'.detail_skpd',$data);
    }

    public function getSummary()
    {
        Excel::create('Download-Summary-Usulan_SKPD', function($excel) {
            $excel->sheet('summary', function($sheet) {
            $year             = (session('years'))?session('years'):date('Y');
            
            $programSelected  = (session('program'))?session('program'):'';
            $kegiatanSelected = (session('kegiatan'))?session('kegiatan'):'';
            $skpdSelected     = (session('skpd'))?session('skpd'):'';

            $index    = M\UsulanSkpd::orderBy('id_skpd','desc')
                                ->orderBy('id_program','desc')
                                ->orderBy('id_kegiatan','desc');

            if(!empty($programSelected)){
                $index = $index->where('id_program',$programSelected);
            }
            if(!empty($kegiatanSelected)){
                $index = $index->where('id_kegiatan',$kegiatanSelected);
            }
            if(!empty($skpdSelected)){
                $index = $index->where('id_skpd',$skpdSelected);
            }
            $data['index'] = $index->whereRaw('year(created_at) = '.$year)->get();

            // $data['index']    = M\UsulanSkpd::orderBy('created_at','desc')->whereRaw('year(created_at) = '.$year)->get();
                $sheet->setOrientation('landscape');
                if(!empty($skpdSelected)){
                    $sheet->loadView($this->folder.'.download_summary_skpd',$data);
                }else{
                    $sheet->loadView($this->folder.'.download_summary',$data);
                }
            });
        })->download('xlsx');
    }

    public function getFull()
    {
        Excel::create('Download-Full-Usulan_SKPD', function($excel) {
            $excel->sheet('full', function($sheet) {
                $sheet->setOrientation('landscape');
                $sheet->setMergeColumn(array(
                    'columns' => array('A','B','C','D','E','F','J'),
                    'rows' => array(
                        array(1,3)
                    )
                ));
                $sheet->setMergeColumn(array(
                    'columns' => array('G','K','L','M'),
                    'rows' => array(
                        array(2,3)
                    )
                ));
                $sheet->mergeCells('G1:I1');
                $sheet->mergeCells('H2:I2');
                $sheet->mergeCells('K1:M1');
                $sheet->mergeCells('N1:N3');
            
                $year             = (session('years'))?session('years'):date('Y');
                // $data['index']    = M\UsulanSkpd::orderBy('created_at','desc')->whereRaw('year(created_at) = '.$year)->get();
            $programSelected  = (session('program'))?session('program'):'';
            $kegiatanSelected = (session('kegiatan'))?session('kegiatan'):'';
            $skpdSelected     = (session('skpd'))?session('skpd'):'';

            $index    = M\UsulanSkpd::orderBy('id_skpd','desc')
                                ->orderBy('id_program','desc')
                                ->orderBy('id_kegiatan','desc');
            if(!empty($programSelected)){
                $index = $index->where('id_program',$programSelected);
            }
            if(!empty($kegiatanSelected)){
                $index = $index->where('id_kegiatan',$kegiatanSelected);
            }
            if(!empty($skpdSelected)){
                $index = $index->where('id_skpd',$skpdSelected);
            }
            $data['index'] = $index->whereRaw('year(created_at) = '.$year)->get();

                $sheet->setOrientation('landscape');
                
                if(!empty($skpdSelected)){
                    $sheet->loadView($this->folder.'.download_full_skpd',$data);
                }else{
                    $sheet->loadView($this->folder.'.download_full',$data);
                }
            });
        })->download('xlsx');

                // $year             = (session('years'))?session('years'):date('Y');
                // $data['index']    = M\UsulanSkpd::orderBy('created_at','desc')->whereRaw('year(created_at) = '.$year)->get();

                // return view($this->folder.'.download_full',$data);
    }
 
}
