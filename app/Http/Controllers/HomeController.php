<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models as M, DB,File;
use Ramsey\Uuid\Uuid;
use Hash, Helper, Mail;

class HomeController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
// Mail::raw('Laravel with Mailgun is easy!', function($message)
// {
//     $message->to('keren@mailinator.com');
// });

        return view('front.index');
    }
    public function login()
    {
        return view('disbud.login');
    }
    public function indexUsulanMasyarakat()
    {
        $data['kabupaten'] = M\KabupatenModel::where('provinsi_id',34)->get();
        $data['kecamatan'] = M\KecamatanModel::where('kabupaten_id',3404)->get();
        $data['desa']      = M\DesaModel::where('kecamatan_id',3401010)->get();
        $data['skpd']      = M\Skpd::get();
    	return view('front.usulan',$data);
    }

    public function postLogin(Request $request)
    {
        $validator = $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        // if ($validator->fails()) {
        //     return redirect('/')
        //         ->withInput()
        //         ->withErrors($validator);
        // }
        $email    = $request->email;
        $password = $request->password;
        $um = M\Disbud::where('email',$email)
                  // ->where('password',Hash::make($request->password))
                  ->where('deleted_at',null)
                  ->first();
        if(count($um) > 0)
        {
            if(Hash::check($password,$um->password)){
                if($um->is_active==1){
                    $data = array(
                        'id'         => $um->id,
                        'name'       => $um->name,
                        'avatar'     => $um->avatar,
                        'created_at' => $um->created_at
                        );
                    session()->put('loginDisbud',$data);
                    return redirect('disbud/home')->with('valid','Welcome to dashboard');
                }else{
                    return redirect('/login')->with('error_login','your account is not active');
                }
            }else{
                return redirect('/login')->with('error_login','your account is not valid');
            }
        }else{
            return redirect('/login')->with('error_login','your account is not valid');
        }

    }

    public function skpdAuth(Request $request)
    {
        $validator = $this->validate($request, [
            'email'    => 'required',
            'password' => 'required',
        ]);

        $email    = $request->email;
        $password = $request->password;
        $masa = M\MasaLogin::findOrFail(1);
        // return $masa->start_date.'<br>'.date('Y-m-d');
        // return strtotime($masa->start_date).'<br>'.strtotime(date('Y-m-d'));
        if(strtotime($masa->start_date) <= strtotime(date('Y-m-d')) && strtotime($masa->end_date) >= strtotime(date('Y-m-d H:i:s'))){

            $um = M\Skpd::where('username',$email)
                      // ->where('password',Hash::make($request->password))
                      ->where('deleted_at',null)
                      ->first();
            if(count($um) > 0)
            {
                if(Hash::check($password,$um->password)){
                    if($um->is_active==1){
                        $data = array(
                            'id'         => $um->id,
                            'name'       => $um->instansi_name,
                            'avatar'     => $um->avatar,
                            'created_at' => $um->created_at
                            );
                        $request->session()->put('loginskpd',$data);
                        return redirect('skpd/home')->with('valid','Welcome to dashboard');
                    }else{
                        return redirect('/')->with('error_login','your account is not active');
                    }
                }else{
                    return redirect('/')->with('error_login','your account is not valid');
                }
            }else{
                return redirect('/')->with('error_login','your account is not valid');
            }
        }else{
            return redirect('/')->with('error_login','masa login skpd belum dibuka');
        }

    }

    public function postUsulanMasyarakat(Request $request)
    {
        $this->validate($request, [
            // 'skpd'                   => 'required',
            'email'                  => 'required',
            'kabupaten'              => 'required',
            'kecamatan'              => 'required',
            'desa'                   => 'required',
            'nama_pengusul'          => 'required',
            'alamat_pengusul'        => 'required',
            // 'ringkasan_permasalahan' => 'required',
            'nomor_telfon'           => 'required',
            'usulan'                 => 'required',
            // 'lampiran'               => 'required|mimes:pdf',
        ]);

        // if ($validator->fails()) {
        //     return redirect('/')
        //         ->withInput()
        //         ->withErrors($validator);
        // }
        // $file = $request->file('document');
        // if (!$request->hasFile('document')) {
        //     return redirect('/');
        // }
 
        $lampiran = $this->getLampiran($request);

        $usul = new M\UsulanMasyarakat;
        $usul->id               = Uuid::uuid4()->getHex();
        $usul->id_skpd          = ($request->skpd)?$request->skpd:NULL;
        // $usul->id_program       = 1;
        // $usul->id_kegiatan      = 1;
        // $usul->id_provinsi      = 34;
        $usul->id_kabupaten     = $request->kabupaten;
        $usul->id_kecamatan     = $request->kecamatan;
        $usul->id_desa          = $request->desa;
        $usul->latitude         = $request->lat;
        $usul->longitude        = $request->lon;
        $usul->name             = $request->nama_pengusul;
        $usul->email            = $request->email;
        $usul->address          = $request->alamat_pengusul;
        // $usul->problems_summary = $request->ringkasan_permasalahan;
        $usul->phone            = $request->nomor_telfon;
        $usul->proposal         = $request->usulan;
        $usul->document         = ($lampiran)?$lampiran:NULL;
        $usul->years            = date('Y')+1;
        $usul->save();

        return redirect('/')->with('success','Usulan anda berhasil dikirim, cek selalu email anda untuk mendapatkan informasi');
    }

    public function kabupaten(Request $req)
    {
    	$data['kabupaten'] = M\KabupatenModel::orderBy('id','desc')->where('id_provinsi',$req->provinsi)->get();
    }

    public function kecamatan(Request $req)
    {
        if($req->ajax()){
            if(!empty($req->id_kabupaten)){

                $kecamatan = M\KecamatanModel::orderBy('id','desc')->where('kabupaten_id',$req->id_kabupaten)->get();
                // $data['kecamatan'] = M\KabupatenModel::where('id_kabupaten',$req->kabupaten)->get();
                $data[] = '<option value="">- Pilih Kecamatan -</option>';
                foreach ($kecamatan as $key => $value) {
                    $data[] = '<option value="'.$value->id.'">'.$value->nama.'</option>';
                }
                return $data;
            }else{
                $data[] ='<option value="">- Pilih Kecamatan -</option>';
                return $data;
            }
        }
    }

    public function desa(Request $req)
    {
        if($req->ajax()){
            if(!empty($req->id_kecamatan)){

                $desa = M\DesaModel::orderBy('id','desc')->where('kecamatan_id','=',$req->id_kecamatan)->get();
                $data[] = '<option value="">- Pilih Desa -</option>';
                foreach ($desa as $key => $val) {
                    $data[] = '<option value="'.$val->id.'" data-lat="'.$val->latitude.'" data-lng="'.$val->longitude.'">'.$val->nama.'</option>';
                }
                return $data;
            }else{
                $data[] ='<option value="">- Pilih Desa -</option>';
                return $data;
            }
        }
    }

    public function confirmation($uri,$title,$message,$value)
    {
        if(!empty($uri)&&!empty($title)&&!empty($message)&&!empty($value)){
            $data['uri']     = $uri;
            $data['title']   = $title;
            $data['message'] = $message;
            $data['value']   = $value;
            return view('modal_confirmation',$data);
        }
    }

    public function getLampiran($request)
    {
        $lampiran = $request->file('lampiran');
        if ($request->hasFile('lampiran')) {
            $ext  = $lampiran->getClientOriginalExtension();
            $lampiranName = str_random(20).'.'.$ext;
            $um = M\UsulanMasyarakat::where('document',$lampiranName)->count();

            while ($um) {
                $lampiranName = str_random(20).'.'.$ext;
                $um = M\UsulanMasyarakat::where('document',$lampiranName)->count();
            }
            $rootDir = base_path('public/storage');
            if(!File::isDirectory($rootDir)){
                File::makeDirectory($rootDir, 0775);    
            }
            
            $directory = base_path('public/storage/masyarakat');
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0775);    
            }

            $lampiran->move($directory, $lampiranName);
            return $lampiranName;
        }
    }
    public function logout()
    {
        if(session('loginskpd')){
            session()->forget('loginskpd');
        }else{
            session()->forget('loginDisbud');
        }
        return redirect('/');
    }
}
