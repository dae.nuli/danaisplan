<?php

namespace App\Http\Controllers\Skpd;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M, File, Storage, Session, Mail;
use Ramsey\Uuid\Uuid;

class Profile extends Controller
{
	protected $folder      = 'skpd.profile';
	protected $title       = 'Profil';
	protected $uri         = 'skpd/profile';
	protected $home        = 'skpd/home';
	protected $template    = 'layout.content';
	protected $limit       = 20;
	protected $created_msg = 'Silahkan cek email untuk melakukan aktivasi akun';
	protected $updated_msg = 'Updated';
	protected $deleted_msg = 'Deleted';
	protected $activation  = 'Akun anda telah aktif, silahkan login';
    protected $users;

    public function __construct()
    {
        $this->users = session('loginskpd')['id'];
    }

    public function getIndex()
    {
        $var = M\Skpd::findOrFail($this->users);

        if($var->is_complete==2){
        	return redirect($this->home);
        }
		$data['template'] = $this->template;
		$data['title']    = $this->title;
		$data['action']   = url($this->uri.'/update');
		$data['uri']      = $this->uri;
        if($var->is_complete==1){
	        return view($this->folder.'.index_2',$data);
        }else{
	        return view($this->folder.'.index',$data);
        }
    }

    public function postIndex(Request $request)
    {
		$var = M\Skpd::findOrFail($this->users);

        if($var->is_complete==2){
        	return redirect($this->home);
        }

        $this->validate($request, [
			'nama'   => 'required',
			'email'  => 'required|email|unique:skpd,email_staff',
			'phone'  => 'required|numeric',
			'alamat' => 'required'
        ]);
		
		$random =	str_random(20);
		$nm     =	M\Skpd::where('activation',$random)->count();
		while ($nm > 0) {
			$random =	str_random(20);
			$nm     =	M\Skpd::where('activation',$random)->count();
		}

        $var = M\Skpd::findOrFail($this->users);
		$var->email_staff   = $request->email;
		$var->phone         = $request->phone;
		$var->address       = $request->alamat;
		$var->personil_name = $request->nama;
		$var->is_complete   = 1;
		$var->activation    = $random;
		$var->password    	= bcrypt($request->password);
        $var->save();

		Mail::queue($this->folder.'.activation',$data = array('uri' => url($this->uri.'/activation/'.$random)),function($message) use ($var){
			$message->to($var->email_staff,$var->instansi_name)->subject('Aktivasi');
		});

        return redirect($this->uri)->with('success',$this->created_msg);
    }

	public function postUpdate(Request $request)
	{

		$var = M\Skpd::findOrFail($this->users);

        if($var->is_complete==2){
        	return redirect($this->home);
        }

        $this->validate($request, [
			'email' => 'required|email|unique:skpd,email_staff,'.$this->users
        ]);

		$random =	str_random(20);
		$nm     =	M\Skpd::where('activation',$random)->count();
		while ($nm > 0) {
			$random =	str_random(20);
			$nm     =	M\Skpd::where('activation',$random)->count();
		}
		
        $var = M\Skpd::findOrFail($this->users);
		$var->email_staff   = $request->email;
		$var->activation    = $random;
		$var->save();

		Mail::queue($this->folder.'.activation',$data = array('uri' => url($this->uri.'/activation/'.$random)),function($message) use ($var){
			$message->to($var->email_staff,$var->instansi_name)->subject('Aktivasi');
		});

        return redirect($this->uri)->with('success',$this->created_msg);
	}

	public function getActivation($code)
	{

		// $var = M\Skpd::findOrFail($this->users);

  //       if($var->is_complete==2){
  //       	return redirect($this->home);
  //       }

		if(!empty($code))
		{
			$var = M\Skpd::where('activation',$code)->first();
			if(count($var)>0){

				if($var->is_complete!=2){
					$var->is_complete = 2;
					$var->save();
	        		
	        		session()->forget('loginskpd');

	        		return redirect('/')->with('success',$this->activation);
				}else{
	        		return redirect('/');
				}
			}
		}
	}
    
    public function getData()
    {
		$data['template'] = $this->template;
		$data['title']    = $this->title;
		$data['action']   = url($this->uri.'/data');
		$data['uri']      = $this->uri;
		$data['index']    = M\Skpd::findOrFail($this->users);
        return view($this->folder.'.profile',$data);
    }

    public function postData(Request $request)
    {
        $this->validate($request, [
			// 'kode_skpd'             => 'required|unique:skpd,code,'.$this->users,
			// 'email'                 => 'required|email|unique:skpd,email,'.$this->users,
			// 'email_staff'           => 'email|unique:skpd,email_staff,'.$this->users,
			// 'nama_instansi'         => 'required',
			'alamat'                => 'required',
			'nama_penanggung_jawab' => 'required',
			'telefon'               => 'numeric'
        ]);

        $user   = explode('@', $request->email);
        $var = M\Skpd::findOrFail($this->users);
        // $var->instansi_name = $request->nama_instansi;
        // $var->email         = $request->email;
        // $var->email_staff   = ($request->email_staff)?$request->email_staff:NULL;
        // $var->username      = $user[0];
        $var->phone         = ($request->telefon)?$request->telefon:NULL;
        $var->address       = ($request->alamat)?$request->alamat:NULL;
        $var->personil_name = ($request->nama_penanggung_jawab)?$request->nama_penanggung_jawab:NULL;
        if(!empty($request->password)){
	        $var->password      = bcrypt($request->password);
        }
        $var->save();

        return redirect($this->uri.'/data')->with('success',$this->updated_msg);
    }
}
