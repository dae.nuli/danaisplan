<?php

namespace App\Http\Controllers\Skpd;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M, File, Storage, Session;
use Ramsey\Uuid\Uuid;

class Usulan extends Controller
{
    protected $folder           = 'skpd.usulan';
    protected $title            = 'Usulan Dana Istimewa';
    protected $uri              = 'skpd/usulan';
    protected $template         = 'layout.content';
    protected $limit            = 20;
    protected $created_msg      = 'Created';
    protected $updated_msg      = 'Updated';
    protected $deleted_msg      = 'Deleted';
    protected $deleted_item_msg = 'Item Deleted';
    protected $users;

    public function __construct()
    {
        $this->users = session('loginskpd')['id'];
    }
    public function getIndex(Request $request)
    {
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['limit']    = $this->limit;
        $data['pages']    = $request->page;
        $data['index']    = M\UsulanSkpd::where('id_skpd',$this->users)->orderBy('created_at','desc')->paginate($this->limit);
        $data['uri']      = $this->uri;
        return view($this->folder.'.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCreate()
    {
        $data['template']     = $this->template;
        $data['kabupaten']    = M\KabupatenModel::where('provinsi_id',34)->get();
        // $data['kecamatan'] = M\KecamatanModel::where('kabupaten_id',3404)->get();
        // $data['desa']      = M\DesaModel::where('kecamatan_id',3401010)->get();
        $data['title']        = $this->title;
        $data['prioritas']    = M\Prioritas::orderBy('id','desc')->get();
        $data['program']      = M\Program::where('years',date('Y'))->get();
        $data['kegiatan']     = M\Kegiatan::all();
        $data['perdais']      = M\Perdais::orderBy('id','desc')->get();
        $data['uri']          = $this->uri;
        $data['yearRange']    = range(date('Y'),date('Y')+2);
        return view($this->folder.'.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postStore(Request $request)
    {

        $this->validate($request, [
            'tahun'            => 'required',
            'program'          => 'required',
            'kegiatan'         => 'required',
            'perdais'          => 'required',
            'tor'              => 'mimes:pdf',
            'rab'              => 'mimes:pdf',
            'tolak_ukur.*'     => 'required',
            'jumlah_kinerja.*' => 'required',
            'satuan_kinerja.*' => 'required',
            'anggaran.*'       => 'required',
            
            'kabupaten.*'      => 'required',
            'kecamatan.*'      => 'required',
            'desa.*'           => 'required',
            // 'tor.*'       => 'mimes:pdf',
            // 'rab.*'       => 'mimes:pdf',
        ]);
 
        $torName = $this->getTor($request);
        $rabName = $this->getRab($request);

        $usul = new M\UsulanSkpd;
        $usul->id          = Uuid::uuid4()->getHex();
        $usul->id_skpd     = session('loginskpd')['id'];
        $usul->id_program  = $request->program;
        $usul->id_kegiatan = $request->kegiatan;
        $usul->years       = $request->tahun;
        $usul->tor         = ($torName)?$torName:NULL;
        $usul->rab         = ($rabName)?$rabName:NULL;
        $usul->perdais     = $request->perdais;
        $usul->save();

        // $torName = $this->getTor($request);
        // $rabName = $this->getRab($request);

        $filter = array('.',',');

        foreach ($request->tolak_ukur as $key => $value) {
            $latlon[$key] = ($request->latlng[$key])?explode(',', $request->latlng[$key]):NULL;

            $ud = new M\UsulanSkpdDetail;
            $ud->id                    = Uuid::uuid4()->getHex();
            $ud->id_usulan             = $usul->id;
            $ud->kabupaten_id          = $request->kabupaten[$key];
            $ud->kecamatan_id          = $request->kecamatan[$key];
            $ud->desa_id               = $request->desa[$key];
            $ud->latitude              = isset($latlon[$key][0])?$latlon[$key][0]:NULL;
            $ud->longitude             = isset($latlon[$key][1])?$latlon[$key][1]:NULL;
            $ud->tolak_ukur_kinerja    = $value;
            $ud->jumlah_target_kinerja = $request->jumlah_kinerja[$key];
            $ud->satuan_target_kinerja = $request->satuan_kinerja[$key];
            $ud->anggaran              = str_replace($filter, "", $request->anggaran[$key]);
            $ud->prioritas             = $request->prioritas[$key];
            // $ud->tor          = ($torName['tor'][$key])?$torName['tor'][$key]:NULL;
            // $ud->rab          = ($rabName['rab'][$key])?$rabName['rab'][$key]:NULL;
            $ud->save();
        }
        return redirect($this->uri)->with('success',$this->created_msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getShow($id)
    {
        $usulan           = M\UsulanSkpd::findOrFail($id);
        $data['usulan']   = $usulan;
        $data['detail']   = M\UsulanSkpdDetail::where('id_usulan',$id)->orderBy('created_at','desc')->get();
        $data['template'] = $this->template;
        $data['title']    = $this->title;
        $data['uri']      = $this->uri;
        $data['tor']      = '/storage/tor/'.$usulan->tor;
        $data['rab']      = '/storage/rab/'.$usulan->rab;
        return view($this->folder.'.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEdit($id)
    {
        $data['template']     = $this->template;
        $data['title']        = $this->title;
        $data['kabupaten']    = M\KabupatenModel::where('provinsi_id',34)->get();
        // $data['kecamatan'] = M\KecamatanModel::where('kabupaten_id',3404)->get();
        // $data['desa']      = M\DesaModel::where('kecamatan_id',3401010)->get();
        $usulanSkpd           = M\UsulanSkpd::findOrFail($id);
        $data['index']        = $usulanSkpd;
        $data['detail']       = M\UsulanSkpdDetail::where('id_usulan',$id)->orderBy('created_at','asc')->get();
        $data['program']      = M\Program::where('years',$usulanSkpd->years)->get();
        $data['kegiatan']     = M\Kegiatan::where('id_program',$usulanSkpd->id_program)->get();
        $data['prioritas']    = M\Prioritas::orderBy('id','desc')->get();
        $data['dais']         = M\Perdais::orderBy('id','desc')->get();
        $data['yearRange']    = range(date('Y'),date('Y')+2);
        $data['uri']          = $this->uri;
        $data['tor']          = '/storage/tor/'.$usulanSkpd->tor;
        $data['rab']          = '/storage/rab/'.$usulanSkpd->rab;
        return view($this->folder.'.edit',$data);
    }

    public function getDownloadTor($doc)
    {
        return response()->download(storage_path('app/public/tor/'.$doc));
    }

    public function getDownloadRab($doc)
    {
        return response()->download(storage_path('app/public/rab/'.$doc));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postUpdate(Request $request, $id)
    {
        $check = M\UsulanSkpd::findOrFail($id);
        if($check->is_approved==0||$check->is_approved==1||$check->is_approved==3||$check->is_approved==4) {
            return redirect()->back()->with('success','Mohon menunggu hingga usulan anda ditindak lanjut oleh admin');
        }
 
        $this->validate($request, [
            'tahun'            => 'required',
            'program'          => 'required',
            'kegiatan'         => 'required',
            'perdais'          => 'required',
            'tor'              => 'mimes:pdf',
            'rab'              => 'mimes:pdf',
            'tolak_ukur.*'     => 'required',
            'jumlah_kinerja.*' => 'required',
            'satuan_kinerja.*' => 'required',
            'anggaran.*'       => 'required',
            
            'kabupaten.*'      => 'required',
            'kecamatan.*'      => 'required',
            'desa.*'           => 'required',
            // 'tor.*'       => 'mimes:pdf',
            // 'rab.*'       => 'mimes:pdf',
        ]);

        $torName = $this->getTor($request);
        $rabName = $this->getRab($request);

        $usul = M\UsulanSkpd::findOrFail($id);
        $usul->id_program  = $request->program;
        $usul->id_kegiatan = $request->kegiatan;
        $usul->years       = $request->tahun;
        $usul->perdais     = $request->perdais;
        if($check->is_approved==2) {
            $usul->is_approved = 4;
        }

        if(!empty($torName)){
            Storage::delete('tor/'.$usul->tor);
        }
        if(!empty($rabName)){
            Storage::delete('rab/'.$usul->rab);
        }
        $usul->tor         = ($torName)?$torName:$usul->tor;
        $usul->rab         = ($rabName)?$rabName:$usul->rab;

        $usul->save();

        // $torName = $this->getTor($request);
        // $rabName = $this->getRab($request);

        $filter = array('.',',');
        foreach ($request->tolak_ukur as $key => $value) {
            

            $latlon[$key] = ($request->latlng[$key])?explode(',', $request->latlng[$key]):NULL;

            if(isset($request->id_detail[$key])){
                $ud = M\UsulanSkpdDetail::findOrFail($request->id_detail[$key]);
                
                // $tName[$key]   = $ud->tor;
                // $rName[$key]   = $ud->rab;

                // if(!empty($torName['tor'][$key])){
                //     (!empty($tName[$key]))?Storage::delete('tor/'.$tName[$key]):NULL;
                // }
                // if(!empty($rabName['rab'][$key])){
                //     (!empty($rName[$key]))?Storage::delete('rab/'.$rName[$key]):NULL;
                // }
            }else{
                $ud = new M\UsulanSkpdDetail;

                // $tName[$key]   = NULL;
                // $rName[$key]   = NULL;
                $ud->id           = Uuid::uuid4()->getHex();
                $ud->id_usulan    = $usul->id;
            }
            


            $ud->kabupaten_id          = $request->kabupaten[$key];
            $ud->kecamatan_id          = $request->kecamatan[$key];
            $ud->desa_id               = $request->desa[$key];
            $ud->latitude              = isset($latlon[$key][0])?$latlon[$key][0]:NULL;
            $ud->longitude             = isset($latlon[$key][1])?$latlon[$key][1]:NULL;
            $ud->tolak_ukur_kinerja    = $request->tolak_ukur[$key];
            $ud->jumlah_target_kinerja = $request->jumlah_kinerja[$key];
            $ud->satuan_target_kinerja = $request->satuan_kinerja[$key];
            $ud->anggaran              = str_replace($filter, "", $request->anggaran[$key]);
            $ud->prioritas             = $request->prioritas[$key];
            // $ud->tor          = ($torName['tor'][$key])?$torName['tor'][$key]:$tName[$key];
            // $ud->rab          = ($rabName['rab'][$key])?$rabName['rab'][$key]:$rName[$key];
            $ud->save();

        }

        return redirect($this->uri)->with('success',$this->updated_msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDestroy($id)
    {
        $var = M\UsulanSkpd::findOrFail($id);
        if(!empty($var->rab)){
            Storage::delete('rab/'.$var->rab);
        }
        if(!empty($var->tor)){
            Storage::delete('tor/'.$var->tor);
        }
        $var->delete();
        M\UsulanSkpdDetail::where('id_usulan',$id)->delete();
        return redirect($this->uri)->with('success',$this->deleted_msg);
    }

    public function mapmodal($index,$latlng=null)
    {
        $data['index']  = $index;
        $data['latlng'] = ($latlng)?explode(',', $latlng):NULL;
        $data['ll']     = ($latlng)?$latlng:NULL;
        $data['uri']    = $this->uri;
        return view($this->folder.'.modal_map',$data);
    }

    public function getTor($request)
    {
        $fileTor = $request->file('tor');
        if ($request->hasFile('tor')) {
            $extTor  = $fileTor->getClientOriginalExtension();
            $torName = rand(1111111,9999999).'.'.$extTor;
            $um = M\UsulanSkpd::where('tor',$torName)->count();

            while ($um) {
                $torName = rand(1111111,9999999).'.'.$extTor;
                $um = M\UsulanSkpd::where('tor',$torName)->count();
            }
            $rootDir = base_path('public/storage');
            if(!File::isDirectory($rootDir)){
                File::makeDirectory($rootDir, 0775);    
            }

            $directory = base_path('public/storage/tor');
            // $directory = storage_path('app/public/tor');
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0775);    
            }

            $fileTor->move($directory, $torName);
            return $torName;
        }
    }

    public function getRab($request)
    {
        $fileName = $request->file('rab');
        if ($request->hasFile('rab')) {
            $ext  = $fileName->getClientOriginalExtension();
            $name = rand(1111111,9999999).'.'.$ext;
            $um = M\UsulanSkpd::where('rab',$name)->count();

            while ($um) {
                $name = rand(1111111,9999999).'.'.$ext;
                $um = M\UsulanSkpd::where('rab',$name)->count();
            }

            $rootDir = base_path('public/storage');
            if(!File::isDirectory($rootDir)){
                File::makeDirectory($rootDir, 0775);    
            }

            $directory = base_path('public/storage/rab');
            // $directory = storage_path('app/public/rab');
            if(!File::isDirectory($directory)){
                File::makeDirectory($directory, 0775);    
            }

            $fileName->move($directory, $name);
            return $name;
        }
    }
    // public function getTor(Request $request, $id=null)
    // {
    //     foreach ($request->file('tor') as $key => $value) {
    //         $fileTor = $value;
    //         if ($value) {

    //             // TOR
    //             $extTor  = $fileTor->getClientOriginalExtension();
    //             $torName[$key] = rand(1111111,9999999).'.'.$extTor;
    //             $um = M\UsulanSkpdDetail::where('tor',$torName[$key])->count();

    //             while ($um) {
    //                 $torName[$key] = rand(1111111,9999999).'.'.$extTor;
    //                 $um = M\UsulanSkpdDetail::where('tor',$torName[$key])->count();
    //             }
    //             $directory = storage_path('app/public/tor');
    //             if(!File::isDirectory($directory)){
    //                 File::makeDirectory($directory, 0775);    
    //             }

    //             $fileTor->move($directory, $torName[$key]);
 
    //         }
    //             $data['tor'][$key] = isset($torName[$key])?$torName[$key]:NULL;
    //     }
    //             return $data;
    // }
    // public function getRab(Request $request, $id=null)
    // {
    //     foreach ($request->file('rab') as $key => $value) {
    //         $fileRab = $value;

    //         if ($value) {
 
    //             //RAB
    //             $extRab  = $fileRab->getClientOriginalExtension();
    //             $rabName[$key] = rand(0000000,9999999).'.'.$extRab;
    //             $us      = M\UsulanSkpdDetail::where('rab',$rabName[$key])->count();

    //             while ($us) {
    //                 $rabName[$key] = rand(0000000,9999999).'.'.$extRab;
    //                 $us      = M\UsulanSkpdDetail::where('rab',$rabName[$key])->count();
    //             }
    //             $directoryRab = storage_path('app/public/rab');
    //             if(!File::isDirectory($directoryRab)){
    //                 File::makeDirectory($directoryRab, 0775);    
    //             }

    //             $fileRab->move($directoryRab, $rabName[$key]);

    //         }
    //             $data['rab'][$key] = isset($rabName[$key])?$rabName[$key]:NULL;
    //     }
    //             return $data;
    // }

    public function postProgram(Request $request)
    {
        if($request->ajax()){
            if(!empty($request->id_program)){

                $kegiatan = M\Kegiatan::where('id_program',$request->id_program)->get();
                $data[] = '<option value="">- Pilih Kegiatan -</option>';
                foreach ($kegiatan as $key => $value) {
                    $data[] = '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
                return $data;
            }else{
                $data[] ='<option value="">- Pilih Kegiatan -</option>';
                return $data;
            }
        }
    }

    public function postTahun(Request $request)
    {
        if($request->ajax()){
            if(!empty($request->tahun)){

                $tahun = M\Program::where('years',$request->tahun)->get();
                $data[] = '<option value="">- Pilih Program -</option>';
                foreach ($tahun as $key => $value) {
                    $data[] = '<option value="'.$value->id.'">'.$value->name.'</option>';
                }
                return $data;
            }
        }
    }
    public function getDelete($id)
    {
        $det = M\UsulanSkpdDetail::findOrFail($id);
        $check = M\UsulanSkpd::findOrFail($det->id_usulan);
        if($check->is_approved==0||$check->is_approved==1||$check->is_approved==3||$check->is_approved==4) {
            return redirect()->back()->with('success','Mohon menunggu hingga usulan anda ditindak lanjut oleh admin');
        }

        $det->delete();
        return redirect()->back()->with('success',$this->deleted_item_msg);
    }
}
