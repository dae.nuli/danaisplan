<?php

namespace App\Http\Controllers\Skpd;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M, Helper;

class Dashboard extends Controller
{
	protected $folder   = 'skpd.dashboard';
	protected $title    = 'Dashboard';
	protected $uri      = 'skpd/masyarakat';
	protected $template = 'layout.content';
	protected $limit    = 20;
	protected $users;

	public function __construct()
	{
		$this->users = session('loginskpd')['id'];
	}
    public function getIndex()
    {
		$data['template'] = $this->template;
		$data['title']    = $this->title;
		$skpd             = M\UsulanSkpd::where('id_skpd',$this->users)->orderBy('created_at','desc')->get();
		$data['skpd']     = $skpd;
		// $data['pen']      = M\UsulanSkpd::where('is_approved',0)->count();
		// $data['app']      = M\UsulanSkpd::where('is_approved',1)->count();
		// $data['rev']      = M\UsulanSkpd::where('is_approved',2)->count();
		// $data['den']      = M\UsulanSkpd::where('is_approved',3)->count();
		// $data['dateNow']  = strtotime(date("Y-m-d"));
		$announce 		  = M\Pengumuman::orderBy('id','desc')->where('end_show','>=',date("Y-m-d"))->get();
		$dates = array();
		foreach ($announce as $i => $val) {
			$dates[$i]['end'] = strtotime($val->end_show);
			$dates[$i]['content'] = $val->content;
		}
		$data['dateShow'] = $dates;
		// return dd($dates);
		// return strtotime("2016-06-13 12:10:00").'<br>'.strtotime("2016-06-13 12:11:10");
		$map =  array();
		foreach ($skpd as $key => $value) {
			$map[$key] = Helper::usulanSkpdMap($value->id);
		}
		// dd($skpd);
		// dd(array_collapse($map));
		$data['usulanMap'] = array_collapse($map);
		$data['uri']       = url('skpd/usulan');
		$data['index']     = M\UsulanSkpd::where('id_skpd',$this->users)->orderBy('created_at','desc')->paginate(10);
		// echo "<pre>";
		// foreach ($data['usulanMap'] as $key => $value) {
		// 	print_r($value['desa']);
		// }
		// echo "</pre>";
        return view($this->folder.'.index',$data);
    }
}
