<?php

namespace App\Http\Controllers\Skpd;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models as M;

class Masyarakat extends Controller
{
    protected $folder   = 'skpd.masyarakat';
    protected $title    = 'Usulan Masyarakat';
    protected $uri      = 'skpd/masyarakat';
    protected $template = 'layout.content';
    protected $limit    = 20;

    public function getIndex(Request $request)
    {
        $data['title'] = $this->title;
        $data['limit'] = $this->limit;
        $data['pages'] = $request->page;
        $data['index'] = M\UsulanMasyarakat::where('id_skpd',session('loginskpd')['id'])
                        ->where('status',1)
        				->where('assign',1)
                        ->orderBy('created_at','desc')
        				->paginate($this->limit);
        $data['uri']   = $this->uri;

        return view($this->folder.'.index',$data);
    }

    public function getDetail($id)
    {
        $data['title'] = $this->title;
        $data['detail'] = M\UsulanMasyarakat::find($id);
        $data['uri']    = $this->uri;
        $data['action'] = url($this->uri.'/detail/'.$id);
        return view($this->folder.'.detail',$data);
    }

    public function postDetail(Request $request, $id)
    {

        
        if($request->type=='approve'){

            $usul = M\UsulanMasyarakat::findOrFail($id);
            $usul->status   = 1;
            $usul->save();

        }elseif($request->type=='denied'){

            $usul = M\UsulanMasyarakat::findOrFail($id);
            $usul->status   = 2;
            $usul->save();

        }

        return redirect($this->uri);
    }
    // public function postProcess($id)
    // {
    //     $um = M\UsulanMasyarakat::find($id);
    //     $um->status = 1;
    //     $um->save();
    //     return redirect($this->uri.'/detail/'.$id)->with('success','Usulan masyarakat telah diterima');
    // }

    // public function getDenied($id)
    // {
    //     $um = M\UsulanMasyarakat::find($id);
    //     $um->status = 2;
    //     $um->save();
    //     return redirect($this->uri.'/detail/'.$id)->with('danger','Usulan masyarakat telah ditolak');
    // }
}
