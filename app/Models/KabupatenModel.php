<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KabupatenModel extends Model
{
    protected $table = 'wilayah_kabupaten';
    protected $casts = ['id'=>'varchar'];

    public function masyarakat()
    {
    	return $this->hasMany('App\Models\UsulanMasyarakat');
    }

    public function provinsi()
    {
    	return $this->belongsTo('App\Models\ProvinsiModel');
    }
}
