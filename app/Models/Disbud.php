<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disbud extends Model
{
	protected $table = 'disbud';
	protected $casts = ['id'=>'varchar'];
}
