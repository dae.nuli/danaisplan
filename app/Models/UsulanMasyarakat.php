<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsulanMasyarakat extends Model
{
    protected $table = 'usulan_masyarakat';
    protected $casts = ['id'=>'varchar'];
    public $incrementing = false;
    public function kabupaten()
    {
    	return $this->belongsTo('App\Models\KabupatenModel','id_kabupaten','id');
    }

    public function kecamatan()
    {
    	return $this->belongsTo('App\Models\KecamatanModel','id_kecamatan');
    }

    public function desa()
    {
        return $this->belongsTo('App\Models\DesaModel','id_desa','id');
    }

    public function skpd()
    {
    	return $this->hasOne('App\Models\Skpd','id','id_skpd');
    }
}
