<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KecamatanModel extends Model
{
    protected $table = 'wilayah_kecamatan';
    protected $casts = ['id'=>'varchar'];

    public function desa()
    {
    	return $this->hasMany('App\Models\DesaModel');
    }

    public function kabupaten()
    {
    	return $this->belongsTo('App\Models\KabupatenModel');
    }
}
