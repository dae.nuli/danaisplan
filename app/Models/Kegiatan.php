<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    protected $table = 'kegiatan';
    // protected $casts = ['id'=>'varchar'];
    // public $incrementing = false;
    public function program()
    {
    	return $this->belongsTo('App\Models\Program','id_program');
    }

}
