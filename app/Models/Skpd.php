<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skpd extends Model
{
	use SoftDeletes;
	protected $table     = 'skpd';
	protected $casts     = ['id'=>'varchar'];
	public $incrementing = false;
	protected $dates     = ['deleted_at'];
}
