<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = 'program';
    // protected $casts = ['id'=>'varchar'];
    // public $incrementing = false;
    public function kegiatan()
    {
    	return $this->hasMany('App\Models\Kegiatan');
    }
}
