<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsulanSkpd extends Model
{
    protected $table = 'usulan_skpd';
    protected $casts = ['id'=>'varchar'];
    public $incrementing = false;

    public function kegiatan()
    {
    	return $this->hasOne('App\Models\Kegiatan','id','id_kegiatan');
    }

    public function dais()
    {
        return $this->hasOne('App\Models\Perdais','id','perdais');
    }

    public function program()
    {
    	return $this->hasOne('App\Models\Program','id','id_program');
    }

    public function skpd()
    {
        return $this->hasOne('App\Models\Skpd','id','id_skpd');
    }
}
