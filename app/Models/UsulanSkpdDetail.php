<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsulanSkpdDetail extends Model
{
    protected $table = 'usulan_skpd_detail';
    protected $casts = ['id'=>'varchar'];
    public $incrementing = false;

    public function kegiatan()
    {
    	return $this->hasOne('App\Models\Kegiatan','id','id_kegiatan');
    }

    public function program()
    {
    	return $this->hasOne('App\Models\Program','id','id_program');
    }

    public function desa()
    {
        return $this->belongsTo('App\Models\DesaModel','desa_id');
    }

    public function kabupaten()
    {
        return $this->belongsTo('App\Models\KabupatenModel','kabupaten_id');
    }

    public function kecamatan()
    {
        return $this->belongsTo('App\Models\KecamatanModel','kecamatan_id');
    }

    public function priority()
    {
        return $this->belongsTo('App\Models\Prioritas','prioritas');
    }
}
