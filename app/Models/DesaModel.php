<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DesaModel extends Model
{
    protected $table = 'wilayah_desa';

	protected $casts   = ['id'=>'varchar'];
	public $timestamps = false;
    public function kecamatan()
    {
    	return $this->belongsTo('App\Models\KecamatanModel');
    }

}
