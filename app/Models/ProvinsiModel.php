<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProvinsiModel extends Model
{
    protected $table = 'wilayah_provinsi';

    public function kabupaten()
    {
    	return $this->hasMany('App\Models\KabupatenModel');
    }
}
