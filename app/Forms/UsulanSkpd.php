<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class UsulanSkpd extends Form
{
    public function buildForm()
    {
    	$this
    		->add('program','select')
    		->add('kegiatan','select')
    		->add('sub kegiatan','text');
    }
}
