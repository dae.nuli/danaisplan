<?php
namespace App\Helpers;
use App\Models as M, Request, DB;
class Helper{
	public static function url_1()
	{
		return Request::segment(1);
	}

	public static function url_2()
	{
		return Request::segment(2);
	}

	public static function url_3()
	{
		return Request::segment(3);
	}

	public static function url_4()
	{
		return Request::segment(4);
	}

	public static function count_usulan_masyarakat()
	{
		return M\UsulanMasyarakat::where('status',0)->count();
	}

	public static function usulan_masyarakat_for_skpd($id_skpd)
	{
		return M\UsulanMasyarakat::where('id_skpd',$id_skpd)->where('status',1)->where('assign',1)->count();
	}

	public static function count_usulan_skpd()
	{
		return M\UsulanSkpd::where('is_approved',0)->count();
	}
	public static function kecamatan($id)
	{
		return M\KecamatanModel::where('kabupaten_id',$id)->get();
	}

	public static function desa($id)
	{
		return DB::table('wilayah_desa')->where('kecamatan_id',$id)->get();
	}

	// public static function getLatLngSkpd($id)
	// {
	// 	return M\UsulanSkpdDetail::where('id_usulan',$id)->first();
	// }

	public static function usulanSkpdMap($id)
	{
		$usulan = M\UsulanSkpd::findOrFail($id);
		$detail = M\UsulanSkpdDetail::where('id_usulan',$id)->get();
		foreach ($detail as $key => $value) {
			$data[$key]['latitude']  = ($value->latitude)?$value->latitude:NULL;
			$data[$key]['longitude'] = ($value->longitude)?$value->longitude:NULL;
			$data[$key]['kabupaten'] = ($value->kabupaten->nama)?$value->kabupaten->nama:NULL;
			$data[$key]['kecamatan'] = ($value->kecamatan->nama)?$value->kecamatan->nama:NULL;
			$data[$key]['desa']      = ($value->desa->nama)?$value->desa->nama:NULL;
			
			$data[$key]['tolak']     = ($value->tolak_ukur_kinerja)?$value->tolak_ukur_kinerja:NULL;
			$data[$key]['anggaran']  = ($value->anggaran)?number_format($value->anggaran,0,",","."):NULL;
			
			$data[$key]['program']   = ($usulan->program->name)?$usulan->program->name:NULL;
			$data[$key]['kegiatan']  = ($usulan->kegiatan->name)?$usulan->kegiatan->name:NULL;

			$data[$key]['status']  = ($usulan->is_approved)?$usulan->is_approved:NULL;
		}
		return $data;
		// return array('latitude'=>($data->latitude)?$data->latitude:NULL,'longitude'=>($data->longitude)?$data->longitude:NULL);
	}

	public static function usulanSkpdMapFilter($id,$kab=NULL,$kec=NULL,$des=NULL,$status,$date)
	// public static function usulanSkpdMapFilter($id,$kab=NULL,$kec=NULL,$des=NULL,$anggaran=NULL,$status,$date)
	{
		$det = M\UsulanSkpdDetail::where('id_usulan',$id);
		if(!empty($kab)){
			$det = $det->where('kabupaten_id',$kab);
		}
		if(!empty($kec)){
			$det = $det->where('kecamatan_id',$kec);
		}
		if(!empty($des)){
			$det = $det->where('desa_id',$des);
		}
		// if(!empty($anggaran)){
		// 	$exp = explode(';', $anggaran);
		// 	$det = $det->whereBetween('anggaran',array($exp[0],$exp[1]));
		// }
		$det = $det->get();
		$data = array();
		foreach ($det as $key => $value) {
			$data[$key]['id']         = ($value->id_usulan)?$value->id_usulan:NULL;
			$data[$key]['name']       = ($value->id_usulan)?self::findName($value->id_usulan):NULL;
			$data[$key]['latitude']   = ($value->latitude)?$value->latitude:NULL;
			$data[$key]['longitude']  = ($value->longitude)?$value->longitude:NULL;
			$data[$key]['kabupaten']  = ($value->kabupaten->nama)?$value->kabupaten->nama:NULL;
			$data[$key]['kecamatan']  = ($value->kecamatan->nama)?$value->kecamatan->nama:NULL;
			$data[$key]['desa']       = ($value->desa->nama)?$value->desa->nama:NULL;
			$data[$key]['tolak_ukur'] = ($value->tolak_ukur_kinerja)?$value->tolak_ukur_kinerja:NULL;
			$data[$key]['jumlah']     = ($value->jumlah_target_kinerja)?$value->jumlah_target_kinerja:NULL;
			$data[$key]['satuan']     = ($value->satuan_target_kinerja)?$value->satuan_target_kinerja:NULL;
			$data[$key]['status']     = ($status)?$status:NULL;
			$data[$key]['created_at'] = ($date)?$date:NULL;
			$data[$key]['anggaran']   = ($value->anggaran)?number_format($value->anggaran,0,",","."):NULL;
		}
		return $data;
	}

	public static function findName($usulan){
		$usulan = M\UsulanSkpd::findOrFail($usulan);
		return $usulan->skpd->instansi_name;
	}

	public static function anggaranCount($id){
		$usulan = M\UsulanSkpdDetail::where('id_usulan',$id)->get();
		foreach ($usulan as $key => $value) {
			$data[] = $value->anggaran;
		}
		return array_sum($data);
	}

	public static function detailSKPD($id){
		return M\UsulanSkpdDetail::where('id_usulan',$id)->get();
	}	

	public static function detailItem($id){
		return M\UsulanSkpdDetail::where('id_skpd',$id)->get();
	}

	public static function statusUsulanMasyarakat($status)
	{
		if ($status==0) {
			return 'New';
		}elseif ($status==1) {
			return 'Approved';
		}elseif ($status==2) {
			return 'Denied';
		}
	}

	public static function statusUsulanSkpd($status)
	{
		if ($status==0) {
			return 'New';
		}elseif ($status==1) {
			return 'Approved';
		}elseif ($status==2) {
			return 'Denied';
		}
	}

	public static function totalKegiatan($id)
	{
		return M\Kegiatan::where('id_program',$id)->count();
	}
}